pub mod rpg_characters;
pub mod rpg_entries;
pub mod rpg_sessions;
pub mod rpg_sessions_rpg_characters;
pub mod tokens;
pub mod users;

use diesel::prelude::*;
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use dotenv::dotenv;

use crate::models::*;

use std::env;

pub type DbPool = Pool<ConnectionManager<PgConnection>>;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub trait FindRecord {
    fn id(&self) -> i32;
}

pub trait Pagination {
    fn page(&self) -> i64;
    fn limit(&self) -> i64;
}

pub trait ORMSubset {
    fn ids(&self) -> Vec<i32>;
}

pub trait RpgSessionsRelated {
    fn rpg_session_ids(&self) -> Vec<i32>;
}

pub trait UsersRelated {
    fn user_ids(&self) -> Vec<i32>;
}

impl Pagination for UsersListForm {
    fn page(&self) -> i64 {
        i64::from(self.page)
    }

    fn limit(&self) -> i64 {
        i64::from(self.limit)
    }
}

impl Pagination for PaginationForm {
    fn page(&self) -> i64 {
        i64::from(self.page)
    }

    fn limit(&self) -> i64 {
        i64::from(self.limit)
    }
}

impl Pagination for RpgSessionsRelatedPaginationForm {
    fn page(&self) -> i64 {
        i64::from(self.page)
    }

    fn limit(&self) -> i64 {
        i64::from(self.limit)
    }
}

impl Pagination for UsersRelatedRpgCharactersForm {
    fn page(&self) -> i64 {
        i64::from(self.page)
    }

    fn limit(&self) -> i64 {
        i64::from(self.limit)
    }
}

impl ORMSubset for UsersListForm {
    fn ids(&self) -> Vec<i32> {
        self.ids.clone()
    }
}

impl RpgSessionsRelated for RpgSessionsRelatedPaginationForm {
    fn rpg_session_ids(&self) -> Vec<i32> {
        self.rpg_session_ids.clone()
    }
}

impl FindRecord for SingleRpgSessionForm {
    fn id(&self) -> i32 {
        self.id
    }
}

impl UsersRelated for UsersRelatedRpgCharactersForm {
    fn user_ids(&self) -> Vec<i32> {
        self.user_ids.clone()
    }
}
