use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;
use uuid::Uuid;

use crate::db::{ORMSubset, Pagination};
use crate::models::{ResponseContext, User, UserForm, UserResult, UsersResult};

pub fn find_by_email(email: String, conn: &PgConnection) -> UserResult {
    use crate::schema::users;
    use std::result::Result as R;

    let query = users::dsl::users
        .filter(users::dsl::email.eq(email))
        .limit(1);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<User>(conn) {
        Ok(v) => match v.get(0) {
            Some(u) => R::Ok(u.clone()),
            _ => R::Err(ResponseContext::UserNotFound),
        },
        _ => R::Err(ResponseContext::UserNotFound),
    }
}

pub fn find_by_nickname(nickname: String, conn: &PgConnection) -> UserResult {
    use crate::schema::users;
    use std::result::Result as R;

    let query = users::dsl::users
        .filter(users::dsl::nickname.eq(nickname))
        .limit(1);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<User>(conn) {
        Ok(v) => match v.get(0) {
            Some(u) => R::Ok(u.clone()),
            _ => R::Err(ResponseContext::UserNotFound),
        },
        _ => R::Err(ResponseContext::UserNotFound),
    }
}

pub fn find_by_access_token(uuid: &Uuid, conn: &PgConnection) -> UserResult {
    use crate::schema::tokens;
    use crate::schema::users;
    use std::result::Result as R;

    let query = users::table.into_boxed().inner_join(tokens::table);
    let query = query.filter(tokens::dsl::access_token.eq(uuid));
    let query = query.select(users::dsl::users.default_selection());
    let query = query.limit(1);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<User>(conn) {
        Ok(v) => match v.get(0) {
            Some(u) => R::Ok(u.clone()),
            _ => R::Err(ResponseContext::UserNotFound),
        },
        _ => R::Err(ResponseContext::UserNotFound),
    }
}

pub fn is_email_registered(email: String, conn: &PgConnection) -> bool {
    find_by_email(email, conn).is_ok()
}

pub fn register_email(email: String, conn: &PgConnection) -> UserResult {
    use crate::schema::users;

    let user = UserForm {
        email: email.clone(),
        nickname: "User".to_string(),
        updated_at: Utc::now().naive_utc(),
    };

    let query = diesel::insert_into(users::table).values(&user);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    query
        .get_result::<User>(conn)
        .or(Err(ResponseContext::SaveEmailFailure))?;

    find_by_email(email, &conn)
}

pub fn update_nickname(user: &User, given_nickname: String, conn: &PgConnection) -> UserResult {
    use crate::schema::users;
    use std::result::Result as R;

    let target = users::dsl::users.filter(users::dsl::id.eq(user.id));
    let query = diesel::update(target).set(users::dsl::nickname.eq(given_nickname));

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<User>(conn) {
        Ok(v) => match v.get(0) {
            Some(u) => R::Ok(u.clone()),
            _ => R::Err(ResponseContext::UserNotFound),
        },
        _ => R::Err(ResponseContext::UserNotFound),
    }
}

pub fn load_users<T: Pagination + ORMSubset>(conn: &PgConnection, form: &T) -> UsersResult {
    use crate::schema::users;
    use std::result::Result as R;

    let query = users::table
        .into_boxed()
        .distinct()
        .filter(users::dsl::id.eq_any(form.ids()))
        .limit(form.limit())
        .offset(form.page())
        .order(users::dsl::id.desc());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<User>(conn) {
        Ok(v) => Ok(v),
        _ => R::Err(ResponseContext::NoUsersFound),
    }
}
