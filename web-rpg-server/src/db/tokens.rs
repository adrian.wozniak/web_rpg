use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;
use uuid::Uuid;

use crate::models::{ResponseContext, Token, TokenForm, TokenResult, User};

pub fn find_by_uuid(uuid: Uuid, conn: &PgConnection) -> TokenResult {
    use crate::schema::tokens;
    use diesel;

    let query = tokens::dsl::tokens
        .filter(tokens::dsl::access_token.eq(uuid))
        .limit(1);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<Token>(conn) {
        Ok(v) => match v.get(0) {
            Some(t) => Ok(t.clone()),
            _ => Err(ResponseContext::TokenNotFound),
        },
        _ => Err(ResponseContext::TokenNotFound),
    }
}

pub fn create_token(user: &User, conn: &PgConnection) -> TokenResult {
    use crate::schema::tokens;
    use diesel;

    let token = TokenForm {
        access_token: uuid::Uuid::new_v4(),
        user_id: user.id,
        created_at: Utc::now().naive_utc(),
        updated_at: Utc::now().naive_utc(),
    };

    let query = diesel::insert_into(tokens::table).values(token);

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    query
        .get_result::<Token>(conn)
        .or(Err(ResponseContext::CreateTokenFailure))
}
