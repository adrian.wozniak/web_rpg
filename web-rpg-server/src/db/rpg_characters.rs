use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;

use crate::db::{Pagination, RpgSessionsRelated, UsersRelated};
use crate::models::{ResponseContext, RpgCharacter, RpgCharactersResult};

pub fn load_rpg_characters<T: Pagination + RpgSessionsRelated>(
    conn: &PgConnection,
    form: &T,
) -> RpgCharactersResult {
    use crate::schema::rpg_characters;
    use crate::schema::rpg_sessions_rpg_characters;
    use diesel;

    let query = rpg_characters::table
        .into_boxed()
        .distinct()
        .inner_join(rpg_sessions_rpg_characters::table)
        .filter(rpg_sessions_rpg_characters::dsl::rpg_session_id.eq_any(form.rpg_session_ids()))
        .select(rpg_characters::dsl::rpg_characters.default_selection())
        .order(rpg_characters::dsl::created_at.desc())
        .limit(form.limit())
        .offset(form.page());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgCharacter>(conn) {
        Ok(vec) => Ok(vec),
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}

pub fn load_users_rpg_characters<T: Pagination + UsersRelated>(
    conn: &PgConnection,
    form: &T,
) -> RpgCharactersResult {
    use crate::schema::rpg_characters;
    use diesel;

    let query = rpg_characters::table
        .into_boxed()
        .distinct()
        .filter(rpg_characters::dsl::user_id.eq_any(form.user_ids()))
        .select(rpg_characters::dsl::rpg_characters.default_selection())
        .order(rpg_characters::dsl::created_at.desc())
        .limit(form.limit())
        .offset(form.page());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgCharacter>(conn) {
        Ok(vec) => Ok(vec),
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}
