use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;

use crate::db::{Pagination, RpgSessionsRelated};
use crate::models::{ResponseContext, RpgEntriesResult, RpgEntry};
use crate::web::web_socket_message::WebSocketMessage;

pub fn load_rpg_entries<T: Pagination + RpgSessionsRelated>(
    conn: &PgConnection,
    form: &T,
) -> RpgEntriesResult {
    use crate::schema::rpg_entries;
    use diesel;

    let query = rpg_entries::table
        .into_boxed()
        .distinct()
        .filter(rpg_entries::dsl::rpg_session_id.eq_any(form.rpg_session_ids()))
        .order(rpg_entries::dsl::created_at.desc())
        .limit(form.limit())
        .offset(form.page());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgEntry>(conn) {
        Ok(vec) => Ok(vec),
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}
