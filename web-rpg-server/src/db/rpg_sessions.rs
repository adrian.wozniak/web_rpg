use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;

use crate::db::{FindRecord, Pagination};
use crate::models::{ResponseContext, RpgSession, RpgSessionResult, RpgSessionsResult};

pub fn load_rpg_sessions(conn: &PgConnection, form: &dyn Pagination) -> RpgSessionsResult {
    use crate::schema::rpg_sessions;
    use diesel;

    let query = rpg_sessions::table
        .into_boxed()
        .distinct()
        .order(rpg_sessions::dsl::id.desc())
        .limit(form.limit())
        .offset(form.page());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgSession>(conn) {
        Ok(vec) => Ok(vec),
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}

pub fn find_by_id(conn: &PgConnection, form: &dyn FindRecord) -> RpgSessionResult {
    use crate::schema::rpg_sessions;
    use diesel;

    let query = rpg_sessions::table
        .into_boxed()
        .filter(rpg_sessions::dsl::id.eq(form.id()));

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgSession>(conn) {
        Ok(vec) => match vec.get(0) {
            Some(rec) => Ok(rec.clone()),
            _ => Err(ResponseContext::NoRpgSessionsFound),
        },
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}
