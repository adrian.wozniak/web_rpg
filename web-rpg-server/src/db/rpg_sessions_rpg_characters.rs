use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::debug_query;
use diesel::pg::Pg;
use diesel::prelude::*;

use crate::db::{Pagination, RpgSessionsRelated};
use crate::models::{ResponseContext, RpgSessionsRpgCharacter, RpgSessionsRpgCharactersResult};

pub fn load_rpg_entries<T: Pagination + RpgSessionsRelated>(
    conn: &PgConnection,
    form: &T,
) -> RpgSessionsRpgCharactersResult {
    use crate::schema::rpg_sessions_rpg_characters;
    use diesel;

    let query = rpg_sessions_rpg_characters::table
        .into_boxed()
        .distinct()
        .filter(rpg_sessions_rpg_characters::dsl::rpg_session_id.eq_any(form.rpg_session_ids()))
        .order(rpg_sessions_rpg_characters::dsl::created_at.desc())
        .limit(form.limit())
        .offset(form.page());

    println!("Exec:\n{}", debug_query::<Pg, _>(&query).to_string());

    match query.load::<RpgSessionsRpgCharacter>(conn) {
        Ok(vec) => Ok(vec),
        _ => Err(ResponseContext::NoRpgSessionsFound),
    }
}
