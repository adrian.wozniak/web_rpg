#[derive(Debug, Clone, PartialEq)]
pub enum DrainError {
    NotEnoughBytes,
    MissingLength,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Drained<T>
where
    T: Clone,
{
    pub drained: T,
    pub rest: Vec<i32>,
}

impl<T> Drained<T>
where
    T: Clone,
{
    pub fn new(drained: T, rest: Vec<i32>) -> Self {
        Self { drained, rest }
    }
}

impl Drained<String> {
    pub fn from_str(s: &'static str) -> Self {
        Drained::new(s.to_string(), vec![])
    }
}

pub type DrainedResult<T> = Result<Drained<T>, DrainError>;

pub struct Drainer<T>
where
    T: Clone + Default,
{
    target: T,
    vec: Vec<i32>,
}

impl<T: Clone + Default> Drainer<T> {
    pub fn new(vec: Vec<i32>) -> Self {
        Self {
            vec,
            target: T::default(),
        }
    }

    pub fn collect(&self) -> T {
        self.target.clone()
    }

    pub fn and_then<A: Clone>(
        &mut self,
        drain: fn(&[i32]) -> DrainedResult<A>,
        success: fn(target: &mut T, val: A),
        failure: fn(target: &mut T) -> (),
    ) -> &mut Self {
        match drain(&self.vec.as_slice()) {
            Ok(d) => {
                success(&mut self.target, d.drained.clone());
                self.vec = d.rest.clone();
            }
            _ => {
                failure(&mut self.target);
                self.vec = vec![];
            }
        };
        self
    }
}

pub fn drain_string(vec: &[i32]) -> DrainedResult<String> {
    match vec.get(0) {
        Some(len) => match (len + 1) as usize {
            l if l <= vec.len() => Ok(Drained::new(decode_string(&vec[1..l]), vec[l..].to_vec())),
            _ => Err(DrainError::NotEnoughBytes),
        },
        None => Err(DrainError::MissingLength),
    }
}

pub fn drain_array(vec: &[i32]) -> DrainedResult<Vec<i32>> {
    match vec.get(0) {
        Some(len) => match (len + 1) as usize {
            l if l <= vec.len() => Ok(Drained::new(vec[1..l].to_vec(), vec[l..].to_vec())),
            _ => Err(DrainError::NotEnoughBytes),
        },
        None => Err(DrainError::MissingLength),
    }
}

pub fn drain_number(vec: &[i32]) -> DrainedResult<i32> {
    match vec.len() {
        0 => Err(DrainError::NotEnoughBytes),
        1 => Ok(Drained::new(vec[0], vec![])),
        _ => Ok(Drained::new(vec[0], vec[1..].to_vec())),
    }
}

pub fn decode_string(vec: &[i32]) -> String {
    let mut buffer = String::new();
    for c in vec {
        let n = (*c) as u8;
        buffer.push(n as char);
    }
    buffer
}

#[cfg(test)]
mod test {
    #[cfg(test)]
    mod drainer {
        use crate::stream::*;

        #[derive(Default, Clone, Debug, PartialEq)]
        struct Foo {
            pub id: i32,
        }

        #[derive(Default, Clone, Debug, PartialEq)]
        struct Bar {
            pub id: i32,
            pub name: String,
        }

        #[derive(Default, Clone, Debug, PartialEq)]
        struct Baz {
            pub id: i32,
            pub name: String,
            pub ids: Vec<i32>,
        }

        #[test]
        fn drain_foo() {
            let id = 34;
            let stream = vec![id.clone()];
            let target: Foo = Drainer::new(stream)
                .and_then(
                    drain_number,
                    |target: &mut Foo, val| target.id = val,
                    |_| (),
                )
                .collect();
            let expected = Foo { id };
            assert_eq!(target, expected);
        }

        #[test]
        fn drain_bar() {
            let id = 34;
            let name = "bar".to_string();
            let stream = vec![id.clone(), 3, 98, 97, 114];
            let target: Bar = Drainer::new(stream)
                .and_then(
                    drain_number,
                    |target: &mut Bar, val| target.id = val,
                    |_| (),
                )
                .and_then(
                    drain_string,
                    |target: &mut Bar, val| target.name = val,
                    |_| (),
                )
                .collect();
            let expected = Bar { id, name };
            assert_eq!(target, expected);
        }

        #[test]
        fn drain_baz() {
            let id = 34;
            let name = "bar".to_string();
            let ids: Vec<i32> = vec![5, 2, 9, 0, 11];
            let stream = vec![id.clone(), 3, 98, 97, 114, 5, 5, 2, 9, 0, 11];
            let target: Baz = Drainer::new(stream)
                .and_then(
                    drain_number,
                    |target: &mut Baz, val| target.id = val,
                    |_| (),
                )
                .and_then(
                    drain_string,
                    |target: &mut Baz, val| target.name = val,
                    |_| (),
                )
                .and_then(
                    drain_array,
                    |target: &mut Baz, val| target.ids = val,
                    |_| (),
                )
                .collect();
            let expected = Baz { id, name, ids };
            assert_eq!(target, expected);
        }
    }

    #[cfg(test)]
    mod drain_string {
        use crate::stream::*;

        #[test]
        fn empty_vector() {
            let vec: [i32; 0] = [];
            let res = drain_string(&vec);
            assert_eq!(res, Err(DrainError::MissingLength));
        }

        #[test]
        fn vector_with_only_len() {
            let vec: [i32; 1] = [3];
            let res = drain_string(&vec);
            assert_eq!(res, Err(DrainError::NotEnoughBytes));
        }

        #[test]
        fn vector_only_with_string() {
            let vec: [i32; 4] = [3, 98, 97, 114];
            let expected = Ok(Drained::new("bar".to_string(), vec![]));
            let res = drain_string(&vec);
            assert_eq!(res, expected);
        }

        #[test]
        fn vector_with_string_and_other_data() {
            let vec: [i32; 7] = [3, 98, 97, 114, 2, 987, 64];
            let expected = Ok(Drained::new("bar".to_string(), vec![2, 987, 64]));
            let res = drain_string(&vec);
            assert_eq!(res, expected);
        }
    }

    #[cfg(test)]
    mod drain_array {
        use crate::stream::*;

        #[test]
        fn empty_vector() {
            let vec: [i32; 0] = [];
            let res = drain_array(&vec);
            assert_eq!(res, Err(DrainError::MissingLength));
        }

        #[test]
        fn vector_with_only_len() {
            let vec: [i32; 1] = [3];
            let res = drain_array(&vec);
            assert_eq!(res, Err(DrainError::NotEnoughBytes));
        }

        #[test]
        fn vector_only_with_array() {
            let vec: [i32; 4] = [3, 98, 97, 114];
            let expected = Ok(Drained::new(vec![98, 97, 114], vec![]));
            let res = drain_array(&vec);
            assert_eq!(res, expected);
        }

        #[test]
        fn vector_with_array_and_other_data() {
            let vec: [i32; 7] = [3, 98, 97, 114, 2, 987, 64];
            let expected = Ok(Drained::new(vec![98, 97, 114], vec![2, 987, 64]));
            let res = drain_array(&vec);
            assert_eq!(res, expected);
        }
    }

    #[cfg(test)]
    mod drain_number {
        use crate::stream::*;

        #[test]
        fn empty_vector() {
            let vec: [i32; 0] = [];
            let res = drain_number(&vec);
            assert_eq!(res, Err(DrainError::NotEnoughBytes));
        }

        #[test]
        fn vector_only_with_number() {
            let vec: [i32; 1] = [98];
            let expected = Ok(Drained::new(98, vec![]));
            let res = drain_number(&vec);
            assert_eq!(res, expected);
        }

        #[test]
        fn vector_with_number_and_other_data() {
            let vec: [i32; 4] = [98, 2, 987, 64];
            let expected = Ok(Drained::new(98, vec![2, 987, 64]));
            let res = drain_number(&vec);
            assert_eq!(res, expected);
        }
    }
}
