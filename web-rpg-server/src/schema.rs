table! {
    rpg_character_images (id) {
        id -> Int4,
        image_url -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    rpg_characters (id) {
        id -> Int4,
        name -> Text,
        user_id -> Int4,
        statistics -> Array<Text>,
        stat_type -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        image_url -> Text,
    }
}

table! {
    rpg_entries (id) {
        id -> Int4,
        user_id -> Int4,
        rpg_character_id -> Int4,
        rpg_session_id -> Int4,
        input -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    rpg_sessions (id) {
        id -> Int4,
        name -> Text,
        user_id -> Int4,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    rpg_sessions_rpg_characters (id) {
        id -> Int4,
        rpg_session_id -> Int4,
        rpg_character_id -> Int4,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    tokens (id) {
        id -> Int4,
        access_token -> Uuid,
        user_id -> Int4,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Int4,
        email -> Text,
        nickname -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(rpg_characters -> users (user_id));
joinable!(rpg_entries -> rpg_characters (rpg_character_id));
joinable!(rpg_entries -> rpg_sessions (rpg_session_id));
joinable!(rpg_entries -> users (user_id));
joinable!(rpg_sessions -> users (user_id));
joinable!(rpg_sessions_rpg_characters -> rpg_characters (rpg_character_id));
joinable!(rpg_sessions_rpg_characters -> rpg_sessions (rpg_session_id));
joinable!(tokens -> users (user_id));

allow_tables_to_appear_in_same_query!(
    rpg_character_images,
    rpg_characters,
    rpg_entries,
    rpg_sessions,
    rpg_sessions_rpg_characters,
    tokens,
    users,
);
