use diesel::prelude::*;
use std::cell::Cell;
use std::sync::Arc;
use uuid::Uuid;

use crate::db::tokens;
use crate::db::users;
use crate::db::DbPool;
use crate::mailing::account_handlers as mailers;
use crate::models::{ResponseContext, Token, TokenResult, User, UserResult};
use crate::web::ws::*;

pub fn check_token(token: &str, conn: &PgConnection, ws: &mut RpgWebSocket) -> TokenResult {
    let uuid: Uuid = match Uuid::parse_str(token) {
        Ok(uuid) => uuid,
        _ => return Err(ResponseContext::InvalidUuidFormat),
    };
    let token = tokens::find_by_uuid(uuid, conn)?;
    let user = users::find_by_access_token(&token.access_token, conn)?;

    ws.current_user_mut().replace(Some(user.clone()));

    Ok(token)
}

pub fn find_current_user(token: &Uuid, conn: &PgConnection, ws: &mut RpgWebSocket) -> UserResult {
    let user = users::find_by_access_token(token, conn)?;

    ws.current_user_mut().replace(Some(user.clone()));

    Ok(user)
}

pub fn sign_in(email: String, conn: &PgConnection, ws: &mut RpgWebSocket) -> TokenResult {
    let user: User = if !users::is_email_registered(email.clone(), conn) {
        crate::db::users::register_email(email.clone(), conn)?
    } else {
        users::find_by_email(email.clone(), conn)?
    };

    let token: Token = crate::db::tokens::create_token(&user, conn)?;
    mailers::send_sign_in_mail(email, &token, &user, ws)?;

    ws.current_user_mut().replace(Some(user.clone()));

    Ok(token)
}

pub fn save_email(email: String, conn: &PgConnection, ws: &mut RpgWebSocket) -> TokenResult {
    let user: User = if !users::is_email_registered(email.clone(), conn) {
        crate::db::users::register_email(email.clone(), conn)?
    } else {
        return Err(ResponseContext::EmailAlreadyTaken);
    };

    let token: Token = tokens::create_token(&user, conn)?;
    let user = mailers::send_sign_up_mail(email, &token, &user, ws)?;

    ws.current_user_mut().replace(Some(user.clone()));

    Ok(token)
}

pub fn update_nickname(nickname: String, conn: &PgConnection, ws: &mut RpgWebSocket) -> UserResult {
    if users::find_by_nickname(nickname.clone(), conn).is_ok() {
        return Err(ResponseContext::NicknameAlreadyTaken);
    }

    let arc = ws.current_user().clone();
    let option: Option<User> = arc.take();

    let user = match option {
        Some(user) => users::update_nickname(&user, nickname, conn)?,
        None => return Err(ResponseContext::UserNotFound),
    };

    ws.current_user_mut().replace(Some(user.clone()));

    Ok(user)
}
