use chrono::NaiveDateTime;
use uuid::Uuid;

use crate::models::*;
use crate::web::binary::i32_to_u8;
use crate::web::web_socket_message::WebSocketMessage;

use web_rpg_shared::op_codes::*;

pub trait WebSocketValue {
    fn ws_value(&self) -> Vec<u8>;
}

impl<T, E> WebSocketValue for Result<T, E>
where
    T: WebSocketValue,
    E: WebSocketValue,
{
    fn ws_value(&self) -> Vec<u8> {
        match self {
            Ok(v) => v.ws_value(),
            Err(e) => e.ws_value(),
        }
    }
}

impl<T> WebSocketValue for Vec<T>
where
    T: WebSocketValue,
{
    fn ws_value(&self) -> Vec<u8> {
        let mut v: Vec<u8> = vec![];
        v.append(&mut bincode::serialize(&self.len()).unwrap());
        for item in self.iter() {
            v.append(&mut item.ws_value());
        }
        v
    }
}

impl WebSocketValue for ResponseContext {
    fn ws_value(&self) -> Vec<u8> {
        use bincode;

        match self {
            &ResponseContext::DeliveryFailure => bincode::serialize(&DELIVERY_FAILURE).unwrap(),
            &ResponseContext::SaveEmailFailure => bincode::serialize(&SAVE_EMAIL_FAILURE).unwrap(),
            &ResponseContext::UserNotFound => bincode::serialize(&USER_NOT_FOUND).unwrap(),
            &ResponseContext::EmailAlreadyTaken => {
                bincode::serialize(&EMAIL_ALREADY_TAKEN).unwrap()
            }
            &ResponseContext::CreateTokenFailure => {
                bincode::serialize(&CREATE_TOKEN_FAILURE).unwrap()
            }
            &ResponseContext::TokenNotFound => bincode::serialize(&TOKEN_NOT_FOUND).unwrap(),
            &ResponseContext::ParseTemplateFailure => {
                bincode::serialize(&PARSE_TEMPLATE_FAILURE).unwrap()
            }
            &ResponseContext::InvalidUuidFormat => {
                bincode::serialize(&INVALID_UUID_FORMAT).unwrap()
            }
            &ResponseContext::NicknameAlreadyTaken => {
                bincode::serialize(&NICKNAME_ALREADY_TAKEN).unwrap()
            }
            &ResponseContext::NoRpgSessionsFound => {
                bincode::serialize(&NO_RPG_SESSIONS_FOUND).unwrap()
            }
            &ResponseContext::NoUsersFound => bincode::serialize(&NO_USERS_FOUND).unwrap(),
            &ResponseContext::DatabaseUnavailable => {
                bincode::serialize(&OP_DATABASE_UNAVAILABLE).unwrap()
            }

            &ResponseContext::CurrentUser(ref user) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_CURRENT_USER).unwrap());
                v.append(&mut bincode::serialize(&user).unwrap());
                println!(
                    "\n\n{:?}\n{:?}\n\n",
                    v,
                    bincode::serialize(&(OP_CURRENT_USER.clone(), user.clone())).unwrap()
                );
                v
            }
            &ResponseContext::ValidToken(ref token) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_VALID_TOKEN).unwrap());
                v.append(&mut bincode::serialize(&token).unwrap());
                v
            }
            &ResponseContext::SingleRpgSession(ref record) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_RPG_SESSION).unwrap());
                v.append(&mut bincode::serialize(&record).unwrap());
                v
            }
            &ResponseContext::RpgSessionsList(ref vec) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_RPG_SESSIONS_LIST).unwrap());
                v.append(&mut bincode::serialize(&vec.len()).unwrap());
                for t in vec {
                    v.append(&mut bincode::serialize(&t).unwrap());
                }
                v
            }
            &ResponseContext::UsersList(ref vec) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_USERS_LIST).unwrap());
                v.append(&mut bincode::serialize(&vec.len()).unwrap());
                for t in vec {
                    v.append(&mut bincode::serialize(&t).unwrap());
                }
                v
            }
            &ResponseContext::RpgCharactersList(ref vec) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_RPG_CHARACTERS_LIST).unwrap());
                v.append(&mut bincode::serialize(&vec.len()).unwrap());
                for t in vec {
                    v.append(&mut bincode::serialize(&t).unwrap());
                }
                v
            }
            &ResponseContext::RpgEntriesList(ref vec) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_RPG_ENTRIES_LIST).unwrap());
                v.append(&mut bincode::serialize(&vec.len()).unwrap());
                for t in vec {
                    v.append(&mut bincode::serialize(&t).unwrap());
                }
                v
            }
            &ResponseContext::RpgSessionsRpgCharacters(ref vec) => {
                let mut v: Vec<u8> = vec![];
                v.append(&mut bincode::serialize(&OP_RPG_SESSIONS_RPG_CHARACTERS).unwrap());
                v.append(&mut bincode::serialize(&vec.len()).unwrap());
                for t in vec {
                    v.append(&mut bincode::serialize(&t).unwrap());
                }
                v
            }
        }
    }
}
