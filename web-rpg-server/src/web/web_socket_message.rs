use diesel::prelude::*;
use futures::future::*;
use futures::Async;

use crate::models::*;
use crate::stream::*;
use crate::web::account_handlers;
use crate::web::app_state::AppState;
use crate::web::web_socket_value;
use crate::web::ws::RpgWebSocket;
use crate::web::WebSocketValue;
use bincode;
use uuid::Uuid;
use web_rpg_shared::op_codes::*;

fn map<A, B>(u: &Vec<A>, f: &Fn(&A) -> B) -> Vec<B> {
    let mut res: Vec<B> = Vec::with_capacity(u.len());
    for x in u.iter() {
        res.push(f(x));
    }
    res
}

#[derive(Debug, Clone, PartialEq)]
pub enum WebSocketMessage {
    Malformed(&'static str),
    NoOp(Vec<i32>),
    Ping,
    SignIn(String),
    SignUp(String),
    Authorize(String),
    CurrentUser(Uuid),
    ChangeNickname(String),
    RpgSession(SingleRpgSessionForm),
    RpgSessionsList(PaginationForm),
    UsersList(UsersListForm),
    RpgCharactersList(RpgSessionsRelatedPaginationForm),
    RpgEntriesList(RpgSessionsRelatedPaginationForm),
    UsersRelatedRpgCharactersList(UsersRelatedRpgCharactersForm),
    RpgSessionsRpgCharactersList(RpgSessionsRelatedPaginationForm),
}

impl From<Vec<i32>> for PaginationForm {
    fn from(vec: Vec<i32>) -> Self {
        Drainer::new(vec)
            .and_then(drain_number, |t: &mut Self, val| t.page = val, |_| ())
            .and_then(
                drain_number,
                |t: &mut Self, val| t.limit = val,
                |t: &mut Self| t.limit = 100,
            )
            .collect()
    }
}

impl From<Vec<i32>> for UsersListForm {
    fn from(vec: Vec<i32>) -> Self {
        Drainer::new(vec)
            .and_then(drain_number, |t: &mut Self, v| t.page = v, |_| ())
            .and_then(
                drain_number,
                |t: &mut Self, v| t.limit = v,
                |t| t.limit = 100,
            )
            .and_then(drain_array, |t: &mut Self, v| t.ids = v, |_| ())
            .collect()
    }
}

impl From<Vec<i32>> for RpgSessionsRelatedPaginationForm {
    fn from(vec: Vec<i32>) -> Self {
        Drainer::new(vec)
            .and_then(drain_number, |t: &mut Self, val| t.page = val, |_| ())
            .and_then(
                drain_number,
                |t: &mut Self, val| t.limit = val,
                |t: &mut Self| t.limit = 100,
            )
            .and_then(
                drain_array,
                |t: &mut Self, val| t.rpg_session_ids = val,
                |_| (),
            )
            .collect()
    }
}

impl From<Vec<i32>> for UsersRelatedRpgCharactersForm {
    fn from(vec: Vec<i32>) -> Self {
        Drainer::new(vec)
            .and_then(drain_number, |t: &mut Self, v| t.page = v, |_| ())
            .and_then(
                drain_number,
                |t: &mut Self, v| t.limit = v,
                |t: &mut Self| t.limit = 100,
            )
            .and_then(drain_array, |t: &mut Self, v| t.user_ids = v, |_| ())
            .collect()
    }
}

impl From<Vec<i32>> for WebSocketMessage {
    fn from(vec: Vec<i32>) -> Self {
        use crate::stream;
        let mut rest: Vec<i32> = vec.as_slice().split_at(1).1.into();
        let op: Option<i32> = vec.get(0).cloned();

        let message = match op.or(Some(NO_OP)).unwrap() {
            NO_OP => WebSocketMessage::NoOp(rest),
            PING => WebSocketMessage::Ping,
            SIGN_IN => {
                let email: String = match stream::drain_string(&rest.as_slice()) {
                    Ok(res) => res.drained.clone(),
                    _ => String::new(),
                };
                WebSocketMessage::SignIn(email)
            }
            SIGN_UP => {
                let email: String = match stream::drain_string(&rest.as_slice()) {
                    Ok(res) => res.drained.clone(),
                    _ => String::new(),
                };
                WebSocketMessage::SignUp(email)
            }
            CHANGE_NICKNAME => {
                let nickname: String = match stream::drain_string(&rest.as_slice()) {
                    Ok(res) => res.drained.clone(),
                    _ => String::new(),
                };
                WebSocketMessage::ChangeNickname(nickname)
            }
            AUTHORIZE => {
                let token: String = match stream::drain_string(&rest.as_slice()) {
                    Ok(res) => res.drained.clone(),
                    _ => String::new(),
                };
                WebSocketMessage::Authorize(token)
            }
            CURRENT_USER => {
                //                let token: String = match stream::drain_string(&rest.as_slice()) {
                //                    Ok(res) => res.drained.clone(),
                //                    _ => String::new(),
                //                };

                let token = unsafe {
                    println!("rest {:?}", rest);
                    let mut tmp: Vec<u32> = map(&rest, &|n| *n as u32);
                    println!("tmp {:?}", tmp);
                    let length = tmp.len() * 4;
                    let capacity = tmp.capacity() * 4;
                    let ptr = tmp.as_mut_ptr() as *mut u8;
                    std::mem::forget(tmp);
                    let vec8: Vec<u8> = Vec::from_raw_parts(ptr, length, capacity);
                    println!("vec8 {:?}", vec8);
                    let s: Uuid = bincode::deserialize(vec8.as_slice()).unwrap();
                    println!("s {:?}", s);
                    s
                };
                WebSocketMessage::CurrentUser(token)
            }
            RPG_SESSIONS_LIST => WebSocketMessage::RpgSessionsList(rest.into()),
            USERS_LIST => WebSocketMessage::UsersList(rest.into()),
            RPG_CHARACTERS_LIST => WebSocketMessage::RpgCharactersList(rest.into()),
            RPG_ENTRIES_LIST => WebSocketMessage::RpgEntriesList(rest.into()),
            SINGLE_RPG_SESSION => {
                if rest.len() != 1 {
                    return WebSocketMessage::Malformed("RpgSession");
                }
                let form = SingleRpgSessionForm { id: rest[0] };
                WebSocketMessage::RpgSession(form)
            }
            USERS_RELATED_RPG_CHARACTERS_LIST => {
                WebSocketMessage::UsersRelatedRpgCharactersList(rest.into())
            }
            RPG_SESSIONS_RPG_CHARACTERS_LIST => {
                WebSocketMessage::RpgSessionsRpgCharactersList(rest.into())
            }
            _ => WebSocketMessage::NoOp(rest),
        };
        if message != WebSocketMessage::Ping {
            println!("Incoming message {:?}", message);
        }
        message
    }
}

impl WebSocketMessage {
    pub fn ws_handle(
        &self,
        conn: &PgConnection,
        ws: &mut RpgWebSocket,
    ) -> Result<Vec<u8>, ResponseContext> {
        let result = match self.clone() {
            WebSocketMessage::SignUp(email) => {
                let token = account_handlers::save_email(email.clone(), conn, ws)?;
                ResponseContext::ValidToken(token).ws_value()
            }
            WebSocketMessage::SignIn(email) => {
                let token = account_handlers::sign_in(email.clone(), conn, ws)?;
                ResponseContext::ValidToken(token).ws_value()
            }
            WebSocketMessage::CurrentUser(token) => {
                let user = account_handlers::find_current_user(&token, conn, ws)?;
                ResponseContext::CurrentUser(user).ws_value()
            }
            WebSocketMessage::ChangeNickname(nickname) => {
                let user = account_handlers::update_nickname(nickname, conn, ws)?;
                ResponseContext::CurrentUser(user).ws_value()
            }
            WebSocketMessage::Authorize(incoming) => {
                let s = incoming.as_str();
                let token = account_handlers::check_token(s, conn, ws)?;
                ResponseContext::ValidToken(token).ws_value()
            }
            WebSocketMessage::RpgSessionsList(form) => {
                use crate::db::rpg_sessions;
                let rpg_sessions = rpg_sessions::load_rpg_sessions(conn, &form)?;
                ResponseContext::RpgSessionsList(rpg_sessions).ws_value()
            }
            WebSocketMessage::RpgSession(form) => {
                use crate::db::rpg_sessions;
                let rpg_session = rpg_sessions::find_by_id(conn, &form)?;
                ResponseContext::SingleRpgSession(rpg_session).ws_value()
            }
            WebSocketMessage::UsersList(form) => {
                use crate::db::users;
                let users = users::load_users(conn, &form)?;
                ResponseContext::UsersList(users).ws_value()
            }
            WebSocketMessage::RpgCharactersList(form) => {
                use crate::db::rpg_characters;
                let rpg_characters = rpg_characters::load_rpg_characters(conn, &form)?;
                ResponseContext::RpgCharactersList(rpg_characters).ws_value()
            }
            WebSocketMessage::UsersRelatedRpgCharactersList(form) => {
                use crate::db::rpg_characters;
                let rpg_characters = rpg_characters::load_users_rpg_characters(conn, &form)?;
                ResponseContext::RpgCharactersList(rpg_characters).ws_value()
            }
            WebSocketMessage::RpgEntriesList(form) => {
                use crate::db::rpg_entries;
                let rpg_entries = rpg_entries::load_rpg_entries(conn, &form)?;
                ResponseContext::RpgEntriesList(rpg_entries).ws_value()
            }
            WebSocketMessage::RpgSessionsRpgCharactersList(form) => {
                use crate::db::rpg_sessions_rpg_characters;
                let vec = rpg_sessions_rpg_characters::load_rpg_entries(conn, &form)?;
                ResponseContext::RpgSessionsRpgCharacters(vec).ws_value()
            }
            _ => {
                println!("println! {:?} (unrecognized)", self);
                bincode::serialize(&UNRECOGNIZED_WEB_SOCKET_MSG).unwrap()
            }
        };
        println!("exec vec res {:?}", result);
        Ok(result)
    }
}
