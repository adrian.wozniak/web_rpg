use bytes::Bytes;

pub fn transform_bytes(bytes: &Bytes) -> Vec<i32> {
    let mut result: Vec<i32> = Vec::new();
    let mut n = 0;
    loop {
        if n >= (bytes.len() / 4) {
            break;
        }
        let mu1: i32 = i32::from(bytes[(n * 4)]);
        let mu2: i32 = i32::from(bytes[(n * 4) + 1]) << 8;
        let mu3: i32 = i32::from(bytes[(n * 4) + 2]) << 16;
        let mu4: i32 = i32::from(bytes[(n * 4) + 3]) << 24;

        result.push(mu1 + mu2 + mu3 + mu4);
        n += 1;
    }
    result
}

pub fn i32_to_u8(value: i32) -> Vec<u8> {
    unsafe {
        std::slice::from_raw_parts([value].as_ptr() as *const u8, std::mem::size_of::<i32>())
            .to_vec()
    }
}

#[cfg(test)]
mod test {
    use crate::web::binary::*;

    #[test]
    fn cast_16777216_from_i32_to_u8() {
        let res: Vec<u8> = i32_to_u8(16777216);
        assert_eq!(res, vec![0, 0, 0, 1]);
    }

    #[test]
    fn cast_65536_from_i32_to_u8() {
        let res: Vec<u8> = i32_to_u8(65536);
        assert_eq!(res, vec![0, 0, 1, 0]);
    }

    #[test]
    fn cast_256_from_i32_to_u8() {
        let res: Vec<u8> = i32_to_u8(256);
        assert_eq!(res, vec![0, 1, 0, 0]);
    }

    #[test]
    fn cast_1_from_i32_to_u8() {
        let res: Vec<u8> = i32_to_u8(1);
        assert_eq!(res, vec![1, 0, 0, 0]);
    }
}
