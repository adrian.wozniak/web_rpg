use std::cell::Cell;
use std::sync::{Arc, Mutex};

use actix::Recipient;

use crate::db::DbPool;
use crate::mailing::client::MailSender;
use crate::models::*;
use crate::web::ws::Join;

/// Define http actor
#[derive(Clone)]
pub struct AppState {
    clients: Arc<Mutex<Vec<Recipient<Join>>>>,
    pool: DbPool,
    current_user: Arc<Cell<Option<User>>>,
    mail_sender: MailSender,
}

impl AppState {
    pub fn new(pool: DbPool, mail_sender: MailSender) -> Self {
        Self {
            clients: Arc::new(Mutex::new(vec![])),
            pool: pool.clone(),
            current_user: Arc::new(Cell::new(None)),
            mail_sender,
        }
    }

    pub fn clients(&self) -> &Arc<Mutex<Vec<Recipient<Join>>>> {
        &self.clients
    }

    pub fn clients_mut(&mut self) -> &mut Arc<Mutex<Vec<Recipient<Join>>>> {
        &mut self.clients
    }

    pub fn pool(&self) -> &DbPool {
        &self.pool
    }

    pub fn pool_mut(&mut self) -> &mut DbPool {
        &mut self.pool
    }

    pub fn current_user(&self) -> &Arc<Cell<Option<User>>> {
        &self.current_user
    }

    pub fn current_user_mut(&mut self) -> &mut Arc<Cell<Option<User>>> {
        &mut self.current_user
    }

    pub fn mail_sender(&self) -> &MailSender {
        &self.mail_sender
    }

    pub fn mail_sender_mut(&mut self) -> &mut MailSender {
        &mut self.mail_sender
    }
}
