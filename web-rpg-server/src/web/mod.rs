use std::cell::Cell;
use std::env;
use std::ops::Deref;
use std::sync::{Arc, RwLock};

use actix::prelude::*;
use actix_broker::BrokerSubscribe;
use actix_files as fs;
use actix_web::http::{header, StatusCode};
use actix_web::middleware::cors::Cors;
use actix_web::*;
use bytes::Bytes;
use diesel::prelude::*;
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use futures::Async;
use sendgrid::SGClient;

use crate::db::DbPool;
use crate::models::*;
use crate::web::app_state::*;
use crate::web::binary::transform_bytes;
use crate::web::web_socket_message::*;
use crate::web::web_socket_value::*;
use crate::web::ws::*;

pub mod account_handlers;
pub mod app_state;
pub mod binary;
pub mod web_socket_message;
pub mod web_socket_value;
pub mod ws;

fn ws_index(
    r: HttpRequest,
    stream: web::Payload,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, Error> {
    println!("{:?}", r);
    let app = RpgWebSocket::new(app_state.get_ref().clone());
    let res = actix_web_actors::ws::start(app, &r, stream);
    println!("{:?}", res.as_ref().unwrap());
    res
}

pub fn run() {
    let sys = ::actix::System::new("WebRpg");
    let addr = env::var("SERVER_ADDRESS")
        .unwrap_or_else(|_| panic!("Missing SERVER_ADDRESS env variable"));
    println!("Server address will be: {:?}", addr);
    let manager = ConnectionManager::<PgConnection>::new(
        env::var("DATABASE_URL").expect("DATABASE_URL must be set"),
    );
    let pool = Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");
    let mail_client = crate::mailing::client::MailSender::build()
        .unwrap_or_else(|e| panic!("Failed to build mail sender: {:?}", e));

    println!("Listen: {}", addr);
    HttpServer::new(move || {
        App::new()
            .data(AppState::new(pool.clone(), mail_client.clone()))
            .wrap(middleware::Logger::new("%T %s %r %{User-Agent}i"))
            .wrap(
                Cors::new()
                    .send_wildcard()
                    .allowed_methods(vec!["GET", "POST"])
                    .allowed_headers(vec![
                        header::AUTHORIZATION,
                        header::ACCEPT,
                        header::CONTENT_TYPE,
                    ])
                    .max_age(3600),
            )
            .service(web::resource("/ws/").route(web::get().to(ws_index)))
            .service(fs::Files::new("/assets", "public"))
            .service(fs::Files::new("/", "public").index_file("index.html"))
    })
    .bind(addr.clone())
    .unwrap_or_else(|_| panic!("Can not bind to {:?}", addr.to_string()))
    .start();

    println!("sys.run()...");
    sys.run()
        .unwrap_or_else(|e| panic!("Failed to run system {:?}", e));
    println!("sys.run() done");
}
