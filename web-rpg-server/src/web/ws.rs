use std::cell::Cell;
use std::env;
use std::ops::Deref;
use std::sync::{Arc, Mutex};

use actix::prelude::*;
use actix_broker::BrokerSubscribe;
use actix_web::http::{header, StatusCode};
use actix_web::middleware::cors::Cors;
use actix_web::*;
use actix_web_actors::ws;
use bytes::Bytes;
use diesel::prelude::*;
use diesel::sql_types::Binary;
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use futures::future::*;
use futures::Async;
use web_rpg_shared::op_codes::*;

use crate::mailing::client::MailSender;
use crate::models::{ResponseContext, User};
use crate::web::app_state::*;
use crate::web::binary::transform_bytes;
use crate::web::web_socket_message::WebSocketMessage;
use crate::web::web_socket_value::WebSocketValue;

pub struct RpgWebSocket {
    app_state: AppState,
}

impl RpgWebSocket {
    pub fn new(app_state: AppState) -> Self {
        Self { app_state }
    }

    pub fn db(&mut self) -> &mut Pool<ConnectionManager<PgConnection>> {
        self.app_state.pool_mut()
    }

    pub fn current_user_mut(&mut self) -> &mut Arc<Cell<Option<User>>> {
        self.app_state.current_user_mut()
    }

    pub fn current_user(&mut self) -> &Arc<Cell<Option<User>>> {
        &self.app_state.current_user()
    }

    pub fn clients_mut(&mut self) -> &mut Arc<Mutex<Vec<Recipient<Join>>>> {
        self.app_state.clients_mut()
    }

    pub fn mail_sender_mut(&mut self) -> &mut MailSender {
        self.app_state.mail_sender_mut()
    }

    pub fn mail_sender(&self) -> &MailSender {
        self.app_state.mail_sender()
    }
}

pub type WsContext = ws::WebsocketContext<RpgWebSocket>;

impl Actor for RpgWebSocket {
    type Context = WsContext;

    fn started(&mut self, ctx: &mut Self::Context) {
        let recipient = ctx.address().recipient();
        let mut guard = self.clients_mut().lock().unwrap();
        (*guard).push(recipient.clone());
    }

    fn stopped(&mut self, ctx: &mut Self::Context) {
        let recipient = ctx.address().recipient();
        let mut guard = self.clients_mut().lock().unwrap();

        for (index, el) in (*guard).iter().enumerate() {
            if *el == recipient {
                (*guard).remove(index);
                break;
            }
        }
    }
}

#[derive(Clone, Message)]
pub struct WsMessage(pub Bytes);

#[derive(Clone, Message)]
#[rtype(result = "Vec<u8>")]
pub struct Join(pub Recipient<WsMessage>);

impl Handler<Join> for RpgWebSocket {
    type Result = MessageResult<Join>;

    fn handle(&mut self, _msg: Join, _ctx: &mut Self::Context) -> Self::Result {
        MessageResult(vec![0, 0, 0, 0])
    }
}

/// Handler for ws::Message message
#[allow(unused_must_use)]
impl StreamHandler<ws::Message, ws::ProtocolError> for RpgWebSocket {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        env::set_var("RUST_LOG", "actix_web=info");
        env::set_var("RUST_BACKTRACE", "1");

        match msg {
            ws::Message::Ping(msg) => ctx.pong(&msg),

            ws::Message::Text(text) => ctx.text(text),

            ws::Message::Binary(bin) => {
                let msg: WebSocketMessage = transform_bytes(&bin).into();

                match handle_ws_message(msg, self) {
                    Ok(res) => ctx.binary(Bytes::from(res)),
                    Err(res) => ctx.binary(Bytes::from(res)),
                };
            }
            _ => (),
        }
    }

    fn finished(&mut self, _ctx: &mut Self::Context) {
        println!("finished");
    }
}

fn handle_ws_message(msg: WebSocketMessage, ws: &mut RpgWebSocket) -> Result<Vec<u8>, Vec<u8>> {
    use crate::web::web_socket_value::*;

    use diesel::backend::Backend;
    use diesel::connection::TransactionManager;
    use diesel::pg::Pg;
    use diesel::query_builder::*;

    if msg == WebSocketMessage::Ping {
        return Ok(bincode::serialize(&PONG).unwrap());
    }

    let pooled = ws
        .db()
        .get()
        .map_err(|_| ResponseContext::DatabaseUnavailable.ws_value())
        .unwrap();
    let conn: &PgConnection = pooled.deref();

    println!("handle_ws_message {:?}", msg);

    let mut query_builder = <Pg as Backend>::QueryBuilder::default();
    conn.build_transaction()
        .read_write()
        .to_sql(&mut query_builder)
        .unwrap_or(());
    let sql = query_builder.finish();
    let tm = conn.transaction_manager();

    tm.begin_transaction_sql(conn, &sql)
        .map_err(|_| ResponseContext::DatabaseUnavailable.ws_value())?;

    match msg.ws_handle(conn, ws) {
        Err(e) => {
            tm.rollback_transaction(conn)
                .map_err(|_| ResponseContext::DatabaseUnavailable.ws_value())?;
            Err(e.ws_value())
        }
        Ok(res) => {
            tm.commit_transaction(conn)
                .map_err(|_| ResponseContext::DatabaseUnavailable.ws_value())?;
            Ok(res)
        }
    }
}
