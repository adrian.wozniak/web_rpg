use diesel::pg::Pg;
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::VarChar;

#[derive(Debug, PartialEq, AsExpression, Clone, Serialize)]
#[sql_type = "VarChar"]
pub enum RpgSessionUserState {
    PendingRequest,
    Accepted,
    Rejected,
    HasLeft,
    Kicked,
}

impl ToSql<VarChar, Pg> for RpgSessionUserState {
    fn to_sql<W: std::io::Write>(&self, out: &mut Output<W, Pg>) -> ::diesel::serialize::Result {
        match self {
            &RpgSessionUserState::PendingRequest => {
                ToSql::<VarChar, Pg>::to_sql("pending_request", out)
            }
            &RpgSessionUserState::Accepted => ToSql::<VarChar, Pg>::to_sql("accepted", out),
            &RpgSessionUserState::Rejected => ToSql::<VarChar, Pg>::to_sql("rejected", out),
            &RpgSessionUserState::HasLeft => ToSql::<VarChar, Pg>::to_sql("has_left", out),
            &RpgSessionUserState::Kicked => ToSql::<VarChar, Pg>::to_sql("kicked", out),
        }
    }
}
