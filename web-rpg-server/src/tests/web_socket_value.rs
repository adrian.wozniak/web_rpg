extern crate chrono;
extern crate uuid;

use crate::web::web_socket_value::*;
use chrono::prelude::*;
use hamcrest::prelude::*;
use uuid::prelude::*;

#[test]
fn it_build_vec_from_int32_1() {
    let target: i32 = 1;
    let result = target.ws_value();
    let expected: Vec<u8> = vec![1, 0, 0, 0];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_int32_12345() {
    let target: i32 = 12345;
    let result = target.ws_value();
    let expected: Vec<u8> = vec![57, 48, 0, 0];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_usize_1() {
    let target: usize = 1;
    let result = target.ws_value();
    let expected: Vec<u8> = vec![1, 0, 0, 0];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_usize_12345() {
    let target: usize = 12345;
    let result = target.ws_value();
    let expected: Vec<u8> = vec![57, 48, 0, 0];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_string_bar() {
    let target = "bar".to_string();
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        3, 0, 0, 0, // len
        98, 0, 0, 0, // b
        97, 0, 0, 0, // a
        114, 0, 0, 0, // r
    ];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_string_foo() {
    let target = "foo".to_string();
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        3, 0, 0, 0, // len
        102, 0, 0, 0, // f
        111, 0, 0, 0, // o
        111, 0, 0, 0, // o
    ];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_multi_language_string() {
    let target = "Löwe 老虎 Léopard".to_string();
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        21, 0, 0, 0, // length
        76, 0, 0, 0, // L
        195, 0, 0, 0, // ö
        182, 0, 0, 0, //
        119, 0, 0, 0, // w
        101, 0, 0, 0, // e
        32, 0, 0, 0, // ' '
        232, 0, 0, 0, // 老
        128, 0, 0, 0, //
        129, 0, 0, 0, //
        232, 0, 0, 0, // 虎
        153, 0, 0, 0, //
        142, 0, 0, 0, //
        32, 0, 0, 0, // ' '
        76, 0, 0, 0, // L
        195, 0, 0, 0, // é
        169, 0, 0, 0, //
        111, 0, 0, 0, // o
        112, 0, 0, 0, // p
        97, 0, 0, 0, // a
        114, 0, 0, 0, // r
        100, 0, 0, 0, // d
    ];
    assert_that!(result, is(equal_to(expected)));
}

#[test]
fn it_build_vec_from_naive_date_time() {
    let target = NaiveDateTime::parse_from_str("2018-10-05 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        19, 0, 0, 0, // len
        50, 0, 0, 0, // year
        48, 0, 0, 0, // year
        49, 0, 0, 0, // year
        56, 0, 0, 0, // year
        45, 0, 0, 0, // -
        49, 0, 0, 0, // month
        48, 0, 0, 0, // month
        45, 0, 0, 0, // -
        48, 0, 0, 0, // day
        53, 0, 0, 0, // day
        32, 0, 0, 0, // ' '
        48, 0, 0, 0, // hour
        55, 0, 0, 0, // hour
        58, 0, 0, 0, // :
        51, 0, 0, 0, // minutes
        50, 0, 0, 0, // minutes
        58, 0, 0, 0, // :
        50, 0, 0, 0, // seconds
        49, 0, 0, 0, // seconds
    ];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_from_uuid() {
    let target = Uuid::parse_str("79290e5b-c832-43f4-8351-479c6feb54d1").unwrap();
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        121, 41, 14, 91, 200, 50, 67, 244, 131, 81, 71, 156, 111, 235, 84, 209,
    ];
    assert_eq!(result, expected);
}

#[test]
fn it_build_vec_u8_from_vec_i32() {
    let target: Vec<i32> = vec![1, 2, 3, 4];
    let result = target.ws_value();
    let expected: Vec<u8> = vec![
        4, 0, 0, 0, // len
        1, 0, 0, 0, 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0,
    ];
    assert_eq!(result, expected);
}

#[cfg(test)]
mod results {
    use crate::models::*;
    use crate::web::web_socket_message::WebSocketMessage;
    use crate::web::web_socket_value::*;
    use hamcrest::prelude::*;

    #[test]
    fn ok_string_result() {
        let target: Result<String, ResponseContext> = Ok("A".to_string());
        let result = target.ws_value();
        let expected: Vec<u8> = vec![
            // len
            1, 0, 0, 0, // A
            65, 0, 0, 0,
        ];
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn error_string_result() {
        let target: Result<String, ResponseContext> = Err(ResponseContext::DatabaseUnavailable);
        let result = target.ws_value();
        let expected: Vec<u8> = vec![30, 0, 0, 0];
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn ok_i32_result() {
        let target: Result<i32, ResponseContext> = Ok(32);
        let result = target.ws_value();
        let expected: Vec<u8> = vec![32, 0, 0, 0];
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn error_i32_result() {
        let target: Result<i32, ResponseContext> = Err(ResponseContext::DatabaseUnavailable);
        let result = target.ws_value();
        let expected: Vec<u8> = vec![30, 0, 0, 0];
        assert_that!(result, is(equal_to(expected)));
    }
}

#[cfg(test)]
mod web_socket_errors {
    use crate::models::*;
    use crate::web::web_socket_value::*;
    use web_rpg_shared::op_codes::*;

    #[test]
    fn delivery_failure() {
        let target = ResponseContext::DeliveryFailure;
        let result = target.ws_value();
        let expected = DELIVERY_FAILURE.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn save_email_failure() {
        let target = ResponseContext::SaveEmailFailure;
        let result = target.ws_value();
        let expected = SAVE_EMAIL_FAILURE.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn user_not_found() {
        let target = ResponseContext::UserNotFound;
        let result = target.ws_value();
        let expected = USER_NOT_FOUND.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn email_already_taken() {
        let target = ResponseContext::EmailAlreadyTaken;
        let result = target.ws_value();
        let expected = EMAIL_ALREADY_TAKEN.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn create_token_failure() {
        let target = ResponseContext::CreateTokenFailure;
        let result = target.ws_value();
        let expected = CREATE_TOKEN_FAILURE.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn token_not_found() {
        let target = ResponseContext::TokenNotFound;
        let result = target.ws_value();
        let expected = TOKEN_NOT_FOUND.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn parse_template_failure() {
        let target = ResponseContext::ParseTemplateFailure;
        let result = target.ws_value();
        let expected = PARSE_TEMPLATE_FAILURE.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn invalid_uuid_format() {
        let target = ResponseContext::InvalidUuidFormat;
        let result = target.ws_value();
        let expected = INVALID_UUID_FORMAT.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn nickname_already_taken() {
        let target = ResponseContext::NicknameAlreadyTaken;
        let result = target.ws_value();
        let expected = NICKNAME_ALREADY_TAKEN.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn no_rpg_sessions_found() {
        let target = ResponseContext::NoRpgSessionsFound;
        let result = target.ws_value();
        let expected = NO_RPG_SESSIONS_FOUND.ws_value();
        assert_eq!(result, expected);
    }

    #[test]
    fn no_users_found() {
        let target = ResponseContext::NoUsersFound;
        let result = target.ws_value();
        let expected = NO_USERS_FOUND.ws_value();
        assert_eq!(result, expected);
    }
}

#[cfg(test)]
mod models {
    use crate::models::*;
    use crate::tests::library;
    use crate::web::web_socket_value::*;
    use chrono::prelude::*;
    use hamcrest::prelude::*;
    use uuid::prelude::*;

    #[test]
    fn it_build_vec_from_user() {
        let target = library::user::get_user();
        let result = target.ws_value();
        let expected: Vec<u8> = library::user::USER.to_vec();
        assert_eq!(result, expected);
    }

    #[test]
    fn it_build_vec_from_token() {
        let target = library::token::get_token();
        let result = target.ws_value();
        let expected: Vec<u8> = library::token::TOKEN.to_vec();
        assert_eq!(result, expected);
    }

    #[test]
    fn it_build_vec_from_rpg_session() {
        let target = library::rpg_session::get_rpg_session();
        let result = target.ws_value();
        let expected: Vec<u8> = library::rpg_session::RPG_SESSION.to_vec();
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn it_build_vec_from_rpg_entry() {
        let target = library::rpg_entry::get_rpg_entry();
        let result = target.ws_value();
        let expected: Vec<u8> = library::rpg_entry::RPG_ENTRY.to_vec();
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn it_build_vec_from_rpg_character() {
        let target = library::rpg_character::get_rpg_character();
        let result = target.ws_value();
        let expected: Vec<u8> = library::rpg_character::RPG_CHARACTER.to_vec();
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn it_build_vec_from_rpg_sessions_rpg_character() {
        let target = library::rpg_sessions_rpg_characters::get_rpg_sessions_rpg_character();
        let result = target.ws_value();
        let expected: Vec<u8> =
            library::rpg_sessions_rpg_characters::RPG_SESSIONS_RPG_CHARACTER.to_vec();
        assert_that!(result, is(equal_to(expected)));
    }
}

#[cfg(test)]
mod response_context {
    use crate::models::*;
    use crate::tests::library;
    use crate::web::web_socket_value::*;
    use chrono::prelude::*;
    use hamcrest::prelude::*;
    use uuid::prelude::*;
    use web_rpg_shared::op_codes::*;

    #[test]
    fn current_user() {
        let user = library::user::get_user();
        let target = ResponseContext::CurrentUser(user.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_CURRENT_USER.ws_value());
        expected.append(&mut user.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn valid_token() {
        let token = library::token::get_token();
        let target = ResponseContext::ValidToken(token.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_VALID_TOKEN.ws_value());
        expected.append(&mut token.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn single_rpg_session() {
        let rpg_session = library::rpg_session::get_rpg_session();
        let target = ResponseContext::SingleRpgSession(rpg_session.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_RPG_SESSION.ws_value());
        expected.append(&mut rpg_session.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn rpg_sessions_list() {
        let rpg_sessions = vec![library::rpg_session::get_rpg_session()];
        let target = ResponseContext::RpgSessionsList(rpg_sessions.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_RPG_SESSIONS_LIST.ws_value());
        expected.append(&mut rpg_sessions.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn users_list() {
        let users = vec![library::user::get_user()];
        let target = ResponseContext::UsersList(users.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_USERS_LIST.ws_value());
        expected.append(&mut users.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn rpg_characters_list() {
        let rpg_characters = vec![library::rpg_character::get_rpg_character()];
        let target = ResponseContext::RpgCharactersList(rpg_characters.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_RPG_CHARACTERS_LIST.ws_value());
        expected.append(&mut rpg_characters.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn rpg_entries_list() {
        let rpg_entries = vec![library::rpg_entry::get_rpg_entry()];
        let target = ResponseContext::RpgEntriesList(rpg_entries.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_RPG_ENTRIES_LIST.ws_value());
        expected.append(&mut rpg_entries.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn rpg_sessions_rpg_characters() {
        let rpg_sessions_rpg_characters =
            vec![library::rpg_sessions_rpg_characters::get_rpg_sessions_rpg_character()];
        let target = ResponseContext::RpgSessionsRpgCharacters(rpg_sessions_rpg_characters.clone());
        let result = target.ws_value();
        let mut expected: Vec<u8> = Vec::new();
        expected.append(&mut OP_RPG_SESSIONS_RPG_CHARACTERS.ws_value());
        expected.append(&mut rpg_sessions_rpg_characters.ws_value());
        assert_that!(result, is(equal_to(expected)));
    }
}
