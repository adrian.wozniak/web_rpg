use crate::models::RpgSessionsRpgCharacter;
use chrono::prelude::*;

pub const RPG_SESSIONS_RPG_CHARACTER: [u8; 172] = [
    // id
    10, 0, 0, 0, // rpg_character_id
    23, 0, 0, 0, // rpg_session_id
    38, 0, 0, 0, // created at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    53, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
    // updated at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    55, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
];

pub fn get_rpg_sessions_rpg_character() -> RpgSessionsRpgCharacter {
    let created_at =
        NaiveDateTime::parse_from_str("2018-10-05 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let updated_at =
        NaiveDateTime::parse_from_str("2018-10-07 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let id = 10;
    let rpg_character_id = 23;
    let rpg_session_id = 38;

    RpgSessionsRpgCharacter {
        id,
        rpg_character_id,
        rpg_session_id,
        created_at,
        updated_at,
    }
}
