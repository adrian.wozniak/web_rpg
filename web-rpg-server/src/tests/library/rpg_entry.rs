use crate::models::RpgEntry;
use chrono::prelude::*;

pub const RPG_ENTRY: [u8; 240] = [
    // id
    10, 0, 0, 0, // user_id
    13, 0, 0, 0, // rpg_character_id
    18, 0, 0, 0, // rpg_session_id
    43, 0, 0, 0, // input
    15, 0, 0, 0, 69, 0, 0, 0, 120, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 112, 0, 0, 0, 108, 0, 0, 0,
    101, 0, 0, 0, 32, 0, 0, 0, 115, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 115, 0, 0, 0, 105, 0, 0,
    0, 111, 0, 0, 0, 110, 0, 0, 0, // created at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    53, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
    // updated at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    55, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
];

pub fn get_rpg_entry() -> RpgEntry {
    let created_at =
        NaiveDateTime::parse_from_str("2018-10-05 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let updated_at =
        NaiveDateTime::parse_from_str("2018-10-07 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let input = "Example session".to_string();
    let id = 10;
    let user_id = 13;
    let rpg_character_id = 18;
    let rpg_session_id = 43;

    RpgEntry {
        id,
        user_id,
        rpg_character_id,
        rpg_session_id,
        input,
        created_at,
        updated_at,
    }
}
