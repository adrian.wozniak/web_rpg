use crate::models::User;
use chrono::prelude::*;

pub const ADMIN_AT_EXAMPLE_COM: [i32; 18] = [
    17,  // length
    97,  // a
    100, // d
    109, // m
    105, // i
    110, // n
    64,  // @
    101, // e
    120, // x
    97,  // a
    109, // m
    112, // p
    108, // l
    101, // e
    46,  // .
    99,  // c
    111, // o
    109, // m
];

pub const USER: [u8; 260] = [
    // id
    10, 0, 0, 0, // email
    17, 0, 0, 0, // len
    97, 0, 0, 0, 100, 0, 0, 0, 109, 0, 0, 0, 105, 0, 0, 0, 110, 0, 0, 0, 64, 0, 0, 0, 101, 0, 0, 0,
    120, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 112, 0, 0, 0, 108, 0, 0, 0, 101, 0, 0, 0, 46, 0, 0, 0,
    99, 0, 0, 0, 111, 0, 0, 0, 109, 0, 0, 0, // nickname
    5, 0, 0, 0, 65, 0, 0, 0, 100, 0, 0, 0, 109, 0, 0, 0, 105, 0, 0, 0, 110, 0, 0, 0,
    // created at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    53, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
    // updated at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    55, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
];

pub fn get_user() -> User {
    let created_at =
        NaiveDateTime::parse_from_str("2018-10-05 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let updated_at =
        NaiveDateTime::parse_from_str("2018-10-07 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let id = 10;
    let email = "admin@example.com".to_string();
    let nickname = "Admin".to_string();

    User {
        id,
        email,
        nickname,
        created_at,
        updated_at,
    }
}
