use crate::models::Token;
use chrono::prelude::*;
use uuid::Uuid;

pub const TOKEN: [u8; 184] = [
    // id
    10, 0, 0, 0, // user_id
    13, 0, 0, 0, // access_token
    121, 41, 14, 91, 200, 50, 67, 244, 131, 81, 71, 156, 111, 235, 84, 209, // created at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    53, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
    // updated at
    19, 0, 0, 0, // len
    50, 0, 0, 0, // year
    48, 0, 0, 0, // year
    49, 0, 0, 0, // year
    56, 0, 0, 0, // year
    45, 0, 0, 0, // -
    49, 0, 0, 0, // month
    48, 0, 0, 0, // month
    45, 0, 0, 0, // -
    48, 0, 0, 0, // day
    55, 0, 0, 0, // day
    32, 0, 0, 0, // ' '
    48, 0, 0, 0, // hour
    55, 0, 0, 0, // hour
    58, 0, 0, 0, // :
    51, 0, 0, 0, // minutes
    50, 0, 0, 0, // minutes
    58, 0, 0, 0, // :
    50, 0, 0, 0, // seconds
    49, 0, 0, 0, // seconds
];

pub fn get_token() -> Token {
    let created_at =
        NaiveDateTime::parse_from_str("2018-10-05 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let updated_at =
        NaiveDateTime::parse_from_str("2018-10-07 7:32:21", "%Y-%m-%d %H:%M:%S").unwrap();
    let access_token = Uuid::parse_str("79290e5b-c832-43f4-8351-479c6feb54d1").unwrap();
    let id = 10;
    let user_id = 13;

    Token {
        id,
        user_id,
        access_token,
        created_at,
        updated_at,
    }
}
