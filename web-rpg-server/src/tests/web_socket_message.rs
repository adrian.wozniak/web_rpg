use hamcrest::prelude::*;

#[cfg(test)]
mod pagination_form {
    use crate::models::*;
    use crate::web::web_socket_value::*;

    #[test]
    fn it_create_from_empty_vec() {
        let vec: Vec<i32> = vec![];
        let form: PaginationForm = vec.into();
        assert_eq!(form.page, 0);
        assert_eq!(form.limit, 100);
    }

    #[test]
    fn it_create_from_1_entry_vec() {
        let vec: Vec<i32> = vec![22];
        let form: PaginationForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 100);
    }

    #[test]
    fn it_create_from_2_entry_vec() {
        let vec: Vec<i32> = vec![22, 74];
        let form: PaginationForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
    }

    #[test]
    fn it_create_from_3_entry_vec() {
        let vec: Vec<i32> = vec![22, 74, 456];
        let form: PaginationForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
    }
}

#[cfg(test)]
mod rpg_sessions_related_pagination_form {
    use crate::models::*;
    use crate::web::web_socket_value::*;

    #[test]
    fn created_from_empty_vec() {
        let vec: Vec<i32> = vec![];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 0,
            limit: 100,
            rpg_session_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_1_valid_int() {
        let vec: Vec<i32> = vec![22];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 22,
            limit: 100,
            rpg_session_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_2_valid_int() {
        let vec: Vec<i32> = vec![22, 74];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 22,
            limit: 74,
            rpg_session_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_4_int_only_len() {
        let vec: Vec<i32> = vec![22, 74, 456];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 22,
            limit: 74,
            rpg_session_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_4_int_wrong_length() {
        let vec: Vec<i32> = vec![22, 74, 893, 20];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 22,
            limit: 74,
            rpg_session_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_4_valid_int() {
        let vec: Vec<i32> = vec![22, 74, 1, 20];
        let form: RpgSessionsRelatedPaginationForm = vec.into();
        let expected = RpgSessionsRelatedPaginationForm {
            page: 22,
            limit: 74,
            rpg_session_ids: vec![20],
        };
        assert_eq!(form, expected);
    }
}

#[cfg(test)]
mod users_list_form {
    use crate::models::*;
    use crate::web::web_socket_value::*;

    #[test]
    fn it_create_from_empty_vec() {
        let vec: Vec<i32> = vec![];
        let ids: Vec<i32> = vec![];
        let form: UsersListForm = vec.into();
        assert_eq!(form.page, 0);
        assert_eq!(form.limit, 100);
        assert_eq!(form.ids, ids);
    }

    #[test]
    fn it_create_from_1_entry_vec() {
        let vec: Vec<i32> = vec![22];
        let ids: Vec<i32> = vec![];
        let form: UsersListForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 100);
        assert_eq!(form.ids, ids);
    }

    #[test]
    fn it_create_from_2_entry_vec() {
        let vec: Vec<i32> = vec![22, 74];
        let ids: Vec<i32> = vec![];
        let form: UsersListForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
        assert_eq!(form.ids, ids);
    }

    #[test]
    fn it_create_from_3_entry_vec() {
        let vec: Vec<i32> = vec![22, 74, 456];
        let ids: Vec<i32> = vec![];
        let form: UsersListForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
        assert_eq!(form.ids, ids);
    }

    #[test]
    fn it_create_from_4_entry_vec() {
        let vec: Vec<i32> = vec![22, 74, 456, 20];
        let form: UsersListForm = vec.into();
        let expected = UsersListForm {
            page: 22,
            limit: 74,
            ids: vec![],
        };
        assert_eq!(form, expected);
    }
}

#[cfg(test)]
mod users_related_rpg_characters_form {
    use crate::models::*;
    use crate::web::web_socket_value::*;
    use web_rpg_shared::op_codes::*;

    #[test]
    fn it_create_from_empty_vec() {
        let vec: Vec<i32> = vec![];
        let user_ids: Vec<i32> = vec![];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        assert_eq!(form.page, 0);
        assert_eq!(form.limit, 100);
        assert_eq!(form.user_ids, user_ids);
    }

    #[test]
    fn it_create_from_1_entry_vec() {
        let vec: Vec<i32> = vec![22];
        let user_ids: Vec<i32> = vec![];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 100);
        assert_eq!(form.user_ids, user_ids);
    }

    #[test]
    fn it_create_from_2_entry_vec() {
        let vec: Vec<i32> = vec![22, 74];
        let user_ids: Vec<i32> = vec![];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
        assert_eq!(form.user_ids, user_ids);
    }

    #[test]
    fn it_create_from_3_entry_vec() {
        let vec: Vec<i32> = vec![22, 74, 456];
        let user_ids: Vec<i32> = vec![];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        assert_eq!(form.page, 22);
        assert_eq!(form.limit, 74);
        assert_eq!(form.user_ids, user_ids);
    }

    #[test]
    fn created_from_4_int_invalid_len() {
        let vec: Vec<i32> = vec![22, 74, 456, 20];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        let expected = UsersRelatedRpgCharactersForm {
            page: 22,
            limit: 74,
            user_ids: vec![],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_4_int_valid_len() {
        let vec: Vec<i32> = vec![22, 74, 1, 20];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        let expected = UsersRelatedRpgCharactersForm {
            page: 22,
            limit: 74,
            user_ids: vec![20],
        };
        assert_eq!(form, expected);
    }

    #[test]
    fn created_from_5_int_valid_len() {
        let vec: Vec<i32> = vec![22, 74, 1, 20, 99];
        let form: UsersRelatedRpgCharactersForm = vec.into();
        let expected = UsersRelatedRpgCharactersForm {
            page: 22,
            limit: 74,
            user_ids: vec![20],
        };
        assert_eq!(form, expected);
    }
}

#[cfg(test)]
mod web_socket_messages {
    use crate::models::*;
    use crate::tests::library;
    use crate::web::web_socket_message::*;
    use crate::web::web_socket_value::*;
    use chrono::prelude::*;
    use hamcrest::prelude::*;
    use uuid::prelude::*;
    use web_rpg_shared::op_codes::*;

    #[test]
    fn ping_from_valid_vec() {
        let vec: Vec<i32> = vec![PING];
        let result: WebSocketMessage = WebSocketMessage::from(vec);
        let expected = WebSocketMessage::Ping;
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn sign_in_from_valid_vec() {
        let mut vec: Vec<i32> = vec![SIGN_IN];
        vec.append(&mut library::user::ADMIN_AT_EXAMPLE_COM.to_vec());
        let email = "admin@example.com".to_string();
        let result: WebSocketMessage = WebSocketMessage::from(vec);
        let expected = WebSocketMessage::SignIn(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn sign_in_from_invalid_vec() {
        let vec: Vec<i32> = vec![SIGN_IN];
        let email = "".to_string();
        let result: WebSocketMessage = WebSocketMessage::from(vec);
        let expected = WebSocketMessage::SignIn(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn sign_up_from_valid_vec() {
        let mut vec: Vec<i32> = vec![SIGN_UP];
        vec.append(&mut library::user::ADMIN_AT_EXAMPLE_COM.to_vec());
        let email = "admin@example.com".to_string();
        let result: WebSocketMessage = vec.into();
        let expected = WebSocketMessage::SignUp(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn sign_up_from_invalid_vec() {
        let vec: Vec<i32> = vec![SIGN_UP];
        let email = "".to_string();
        let result: WebSocketMessage = vec.into();
        let expected = WebSocketMessage::SignUp(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn change_nickname_from_vec() {
        let mut vec: Vec<i32> = vec![CHANGE_NICKNAME];
        vec.append(&mut library::user::ADMIN_AT_EXAMPLE_COM.to_vec());
        let email = "admin@example.com".to_string();
        let result: WebSocketMessage = vec.into();
        let expected = WebSocketMessage::ChangeNickname(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn authorize_from_vec() {
        let mut vec: Vec<i32> = vec![AUTHORIZE];
        vec.append(&mut library::user::ADMIN_AT_EXAMPLE_COM.to_vec());
        let email = "admin@example.com".to_string();
        let result: WebSocketMessage = vec.into();
        let expected = WebSocketMessage::Authorize(email);
        assert_that!(result, is(equal_to(expected)));
    }

    #[test]
    fn current_user_from_vec() {
        let mut vec: Vec<i32> = vec![CURRENT_USER];
        vec.append(&mut library::user::ADMIN_AT_EXAMPLE_COM.to_vec());
        let email = "admin@example.com".to_string();
        let result: WebSocketMessage = vec.into();
        let expected = WebSocketMessage::CurrentUser(email);
        assert_that!(result, is(equal_to(expected)));
    }
}
