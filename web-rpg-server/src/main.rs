#![allow(proc_macro_derive_resolution_fallback, unused_macros, unused_imports)]
#![feature(duration_float, result_map_or_else)]
#[cfg_attr(feature = "cargo-clippy", allow(needless_lifetimes))]
#[cfg(test)]
#[macro_use]
extern crate hamcrest;

extern crate console;
extern crate env_logger;
extern crate pretty_env_logger;
#[macro_use]
extern crate log;
extern crate actix;
extern crate actix_files;
extern crate actix_http;
extern crate actix_web;
extern crate actix_web_actors;
extern crate bincode;
extern crate chrono;
extern crate dotenv;
extern crate lettre;
extern crate lettre_email;
extern crate r2d2;
extern crate serde;
extern crate uuid;
extern crate web_rpg_shared;
#[macro_use]
extern crate futures;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_full_text_search;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate tera;
#[macro_use]
extern crate lazy_static;

use dotenv::dotenv;
use std::env;

pub mod custom_types;
pub mod db;
pub mod mailing;
pub mod models;
pub mod schema;
pub mod stream;
pub mod web;

#[cfg(test)]
pub mod tests;

fn main() {
    dotenv().ok();
    env::set_var("RUST_LOG", "actix_web=info");
    env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    web::run();
}
