pub mod account_handlers;
pub mod client;

use tera::Tera;

lazy_static! {
    pub static ref TERA: Tera = {
        let mut tera = compile_templates!("assets/templates/**/*");
        tera.autoescape_on(vec!["html", ".sql"]);
        tera
    };
}
