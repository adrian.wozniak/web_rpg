use std::cell::Cell;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::ops::Deref;
use std::sync::Arc;

use actix_http::error::ParseError::Header;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use lettre_email::EmailBuilder;
use sendgrid::v3::*;
use sendgrid::{Destination, Mail};
use tera::Tera;

use crate::db::tokens;
use crate::db::users;
use crate::db::DbPool;
use crate::mailing::client::MailSender;
use crate::mailing::{client, TERA};
use crate::models::{ResponseContext, Token, TokenResult, User, UserResult};
use crate::web::app_state::AppState;
use crate::web::ws::RpgWebSocket;

#[derive(Serialize)]
struct SignInReceiver {
    email: String,
    token: String,
}

#[derive(Serialize)]
struct SignUpReceiver {
    email: String,
    token: String,
}

pub fn send_sign_in_mail(
    email: String,
    token: &Token,
    user: &User,
    ws: &mut RpgWebSocket,
) -> UserResult {
    let receiver = SignInReceiver {
        email,
        token: token.access_token.to_string(),
    };

    let html = TERA
        .render("sign_in.html", &receiver)
        .or(Err(ResponseContext::ParseTemplateFailure))?;

    if !cfg!(release) {
        let mut file = std::fs::File::create("tmp/mail.html").unwrap();
        file.write_all(html.as_bytes()).unwrap();
    }

    let mail = EmailBuilder::new()
        .from(MailSender::from_email())
        .to(receiver.email.to_string())
        .subject("Welcome back!")
        .html(html.to_string())
        .build()
        .or_else(|_| Err(ResponseContext::DeliveryFailure))?;
    ws.mail_sender().send_lettre(mail).map_or_else(
        |_| Err(ResponseContext::DeliveryFailure),
        |_| Ok(user.clone()),
    )
}

pub fn send_sign_up_mail(
    email: String,
    token: &Token,
    user: &User,
    ws: &mut RpgWebSocket,
) -> UserResult {
    let receiver = SignUpReceiver {
        email,
        token: token.access_token.to_string(),
    };

    let html = TERA
        .render("sign_up.html", &receiver)
        .or(Err(ResponseContext::ParseTemplateFailure))?;

    let mail = EmailBuilder::new()
        .from(MailSender::from_email())
        .to(receiver.email.to_string())
        .subject("Welcome in Web RPG!")
        .html(html.to_string())
        .build()
        .or_else(|_| Err(ResponseContext::DeliveryFailure))?;
    ws.mail_sender().send_lettre(mail).map_or_else(
        |_| Err(ResponseContext::DeliveryFailure),
        |_| Ok(user.clone()),
    )
}
