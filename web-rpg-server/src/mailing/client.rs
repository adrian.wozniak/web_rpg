use std::time::Duration;

use lettre::smtp::authentication::*;
use lettre::smtp::client::net::DEFAULT_TLS_PROTOCOLS;
use lettre::smtp::extension::*;
use lettre::smtp::ConnectionReuseParameters;
use lettre::*;
use native_tls::{Protocol, TlsConnector};

use dotenv::dotenv;

#[derive(Clone)]
pub struct MailSender {
    inner: Box<SmtpClient>,
}

impl MailSender {
    pub fn build() -> Result<MailSender, String> {
        Ok(Self {
            inner: Box::new(Self::client()),
        })
    }

    pub fn send_lettre(&self, mail: lettre_email::Email) -> Result<(), String> {
        let builder = self.inner.clone();
        std::thread::spawn(move || {
            let res = builder.transport().send(mail.into());
            // builder.transport(Self::client())).send(mail)
            match res {
                Ok(_) => println!("Send!"),
                Err(e) => eprintln!("Failed to send email: {:?}", e),
            };
        });
        Ok(())
    }

    fn client() -> SmtpClient {
        dotenv().ok();

        let password = Self::password();
        let email = Self::email();
        let domain = Self::domain();
        let port = Self::port();

        println!(
            "Booting mail sender with: {:?}",
            (
                domain.clone(),
                port.clone(),
                email.clone(),
                password.clone()
            )
        );

        let mut tls_builder = TlsConnector::builder();
        tls_builder.min_protocol_version(Some(Protocol::Sslv3));

        let tls_parameters =
            ClientTlsParameters::new(domain.to_string(), tls_builder.build().unwrap());
        SmtpClient::new(
            (domain.as_str(), port),
            ClientSecurity::Wrapper(tls_parameters),
        )
        .unwrap_or_else(|_| panic!("Couldn't start mail client"))
        .hello_name(ClientId::Domain("poczta.ita-prog.pl".to_string()))
        .credentials(Credentials::new(email.to_string(), password.to_string()))
        .smtp_utf8(true)
        .authentication_mechanism(Mechanism::Plain)
        .connection_reuse(ConnectionReuseParameters::ReuseUnlimited)
    }

    pub fn password() -> String {
        ::std::env::var("MAIL_PASSWORD")
            .unwrap_or_else(|_| panic!("Environment MAIL_PASSWORD not found"))
    }

    pub fn email() -> String {
        ::std::env::var("MAIL_EMAIL").unwrap_or_else(|_| panic!("Environment MAIL_EMAIL not found"))
    }

    pub fn domain() -> String {
        ::std::env::var("MAIL_HOST").unwrap_or_else(|_| panic!("Environment MAIL_HOST not found"))
    }

    pub fn port() -> u16 {
        ::std::env::var("MAIL_PORT")
            .unwrap_or_else(|_| panic!("Environment MAIL_PORT not found"))
            .parse::<u16>()
            .unwrap_or_else(|_| panic!("Environment MAIL_PORT not found"))
    }

    pub fn from_email() -> String {
        ::std::env::var("MAIL_FROM").unwrap_or_else(|_| panic!("Environment MAIL_FROM not found"))
    }
}
