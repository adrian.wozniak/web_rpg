#![allow(proc_macro_derive_resolution_fallback)]

use std::io::prelude::*;

use chrono::naive::NaiveDateTime;
use uuid::Uuid;

use crate::schema::*;

/* Input types */

#[derive(Debug, PartialEq, Clone, Default)]
pub struct PaginationForm {
    pub page: i32,
    pub limit: i32,
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct RpgSessionsRelatedPaginationForm {
    pub page: i32,
    pub limit: i32,
    pub rpg_session_ids: Vec<i32>,
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct UsersListForm {
    pub page: i32,
    pub limit: i32,
    pub ids: Vec<i32>,
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct SingleRpgSessionForm {
    pub id: i32,
}

#[derive(Debug, PartialEq, Clone, Default)]
pub struct UsersRelatedRpgCharactersForm {
    pub page: i32,
    pub limit: i32,
    pub user_ids: Vec<i32>,
}

/* Output type */

#[derive(Debug, Clone)]
pub enum ResponseContext {
    // errors
    DeliveryFailure,
    SaveEmailFailure,
    UserNotFound,
    EmailAlreadyTaken,
    CreateTokenFailure,
    TokenNotFound,
    ParseTemplateFailure,
    InvalidUuidFormat,
    NicknameAlreadyTaken,
    NoRpgSessionsFound,
    NoUsersFound,
    DatabaseUnavailable,

    // success
    CurrentUser(User),
    ValidToken(Token),
    RpgSessionsList(Vec<RpgSession>),
    SingleRpgSession(RpgSession),
    UsersList(Vec<User>),
    RpgCharactersList(Vec<RpgCharacter>),
    RpgEntriesList(Vec<RpgEntry>),
    RpgSessionsRpgCharacters(Vec<RpgSessionsRpgCharacter>),
}

/* Database handlers */

impl From<ResponseContext> for diesel::result::Error {
    fn from(_context: ResponseContext) -> Self {
        use diesel::result::Error;
        //        match context {
        //            ResponseContext::DeliveryFailure => {},
        //            ResponseContext::SaveEmailFailure => {},
        //            ResponseContext::UserNotFound => {},
        //            ResponseContext::EmailAlreadyTaken => {},
        //            ResponseContext::CreateTokenFailure => {},
        //            ResponseContext::TokenNotFound => {},
        //            ResponseContext::ParseTemplateFailure => {},
        //            ResponseContext::InvalidUuidFormat => {},
        //            ResponseContext::NicknameAlreadyTaken => {},
        //            ResponseContext::NoRpgSessionsFound => {},
        //            ResponseContext::NoUsersFound => {},
        //            ResponseContext::DatabaseUnavailable => {},
        //            ResponseContext::CurrentUser(_) => {},
        //            ResponseContext::ValidToken(_) => {},
        //            ResponseContext::RpgSessionsList(_) => {},
        //            ResponseContext::SingleRpgSession(_) => {},
        //            ResponseContext::UsersList(_) => {},
        //            ResponseContext::RpgCharactersList(_) => {},
        //            ResponseContext::RpgEntriesList(_) => {},
        //            ResponseContext::RpgSessionsRpgCharacters(_) => {},
        //        }
        Error::RollbackTransaction
    }
}

/* Database ORM */

// select uuid_generate_v5(uuid_ns_url(), 'web-rpg.ita-prog.pl/' || (now()) :: TEXT);

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct Token {
    pub id: i32,
    pub access_token: Uuid,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "tokens"]
pub struct TokenForm {
    pub access_token: Uuid,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub nickname: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "users"]
pub struct UserForm {
    pub email: String,
    pub nickname: String,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct RpgSession {
    pub id: i32,
    pub name: String,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "rpg_sessions"]
pub struct RpgSessionForm {
    pub name: String,
    pub user_id: i32,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct RpgEntry {
    pub id: i32,
    pub user_id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub input: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "rpg_entries"]
pub struct RpgEntryForm {
    pub user_id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub input: String,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct RpgCharacter {
    pub id: i32,
    pub name: String,
    pub user_id: i32,
    pub statistics: Vec<String>,
    pub stat_type: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub image_url: String,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "rpg_characters"]
pub struct RpgCharacterForm {
    pub name: String,
    pub user_id: i32,
    pub statistics: Vec<String>,
    pub stat_type: String,
    pub updated_at: NaiveDateTime,
    pub image_url: String,
}

#[derive(Deserialize, Serialize, Queryable, Debug, Clone)]
pub struct RpgSessionsRpgCharacter {
    pub id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Insertable, Debug, Clone)]
#[table_name = "rpg_sessions_rpg_characters"]
pub struct RpgSessionsRpgCharacterForm {
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
}

/* Fetch database results */

pub type UserResult = Result<User, ResponseContext>;
pub type UsersResult = Result<Vec<User>, ResponseContext>;

pub type TokenResult = Result<Token, ResponseContext>;
pub type TokensResult = Result<Vec<Token>, ResponseContext>;

pub type RpgSessionResult = Result<RpgSession, ResponseContext>;
pub type RpgSessionsResult = Result<Vec<RpgSession>, ResponseContext>;

pub type RpgCharacterResult = Result<RpgCharacter, ResponseContext>;
pub type RpgCharactersResult = Result<Vec<RpgCharacter>, ResponseContext>;

pub type RpgEntryResult = Result<RpgEntry, ResponseContext>;
pub type RpgEntriesResult = Result<Vec<RpgEntry>, ResponseContext>;

pub type RpgSessionsRpgCharacterResult = Result<RpgSessionsRpgCharacter, ResponseContext>;
pub type RpgSessionsRpgCharactersResult = Result<Vec<RpgSessionsRpgCharacter>, ResponseContext>;
