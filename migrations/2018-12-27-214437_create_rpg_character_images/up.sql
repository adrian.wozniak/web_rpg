CREATE TABLE IF NOT EXISTS rpg_character_images
(
  id         SERIAL PRIMARY KEY UNIQUE NOT NULL,
  image_url  TEXT UNIQUE               NOT NULL,
  created_at TIMESTAMP                 NOT NULL DEFAULT now(),
  updated_at TIMESTAMP                 NOT NULL DEFAULT now()
);

ALTER TABLE rpg_characters ADD COLUMN image_url TEXT NOT NULL DEFAULT '';
