CREATE TABLE IF NOT EXISTS rpg_sessions_rpg_characters
(
  id               SERIAL UNIQUE PRIMARY KEY        NOT NULL,
  rpg_session_id   INT REFERENCES rpg_sessions (id) NOT NULL,
  rpg_character_id INT REFERENCES rpg_characters (id) NOT NULL,
  created_at       TIMESTAMP                        NOT NULL DEFAULT NOW(),
  updated_at       TIMESTAMP                        NOT NULL DEFAULT NOW()
);
