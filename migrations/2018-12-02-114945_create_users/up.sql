CREATE TABLE IF NOT EXISTS users
(
  id         SERIAL UNIQUE NOT NULL PRIMARY KEY,
  email      TEXT UNIQUE   NOT NULL,
  nickname   TEXT UNIQUE   NOT NULL,
  created_at TIMESTAMP     NOT NULL DEFAULT now(),
  updated_at TIMESTAMP     NOT NULL DEFAULT now()
);
