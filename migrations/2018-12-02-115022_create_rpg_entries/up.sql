CREATE TABLE IF NOT EXISTS rpg_entries
(
  id               serial unique                      NOT NULL PRIMARY KEY,
  user_id          INT REFERENCES users (id)          NOT NULL,
  rpg_character_id INT REFERENCES rpg_characters (id) NOT NULL,
  rpg_session_id   INT REFERENCES rpg_sessions (id)   NOT NULL,
  input            TEXT                               NOT NULL DEFAULT '',
  created_at       TIMESTAMP                          NOT NULL DEFAULT now(),
  updated_at       TIMESTAMP                          NOT NULL DEFAULT now()
);
