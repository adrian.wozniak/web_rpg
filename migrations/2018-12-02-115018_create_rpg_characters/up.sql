CREATE TABLE IF NOT EXISTS rpg_characters
(
  id         serial unique             not null primary key,
  name       TEXT unique               not null,
  user_id    INT REFERENCES users (id) not null,
  statistics TEXT []                   NOT NULL DEFAULT '{}',
  stat_type  TEXT                      not null,
  created_at TIMESTAMP                 NOT NULL DEFAULT now(),
  updated_at TIMESTAMP                 NOT NULL DEFAULT now()
);
