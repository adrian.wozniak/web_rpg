# Web RPG

## Run

Backend:

```bash
curl https://sh.rustup.rs -sSf | sh
rustup install nightly
rustup default nightly
cargo build
cargo run
```

Frontend:

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install 10.0.0
nvm use 10.0.0

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
cd client
yarn
yarn dev
```

```bash
firefox http://localhost:9000
```

## Communication layer

* Binary messages only
* Encode everything as int 32 bytes
* Data order depends on backend

## Testing

### Code coverage

### Install
```bash
RUSTFLAGS="--cfg procmacro2_semver_exempt" cargo install cargo-tarpaulin
cd client
yarn

```

```bash
export DATABASE_URL=postgres://localhost/web_rpg_test
diesel setup
diesel migration run
cargo test
cargo tarpaulin
cd client
yarn test
```
