/* input */
pub const NO_OP: i32 = -1;
pub const PING: i32 = 1;
pub const PONG: i32 = 2;
pub const SIGN_IN: i32 = 3;
pub const SIGN_UP: i32 = 4;
pub const AUTHORIZE: i32 = 5;
pub const CURRENT_USER: i32 = 6;
pub const CHANGE_NICKNAME: i32 = 7;
pub const RPG_SESSIONS_LIST: i32 = 8;
pub const USERS_LIST: i32 = 9;
pub const RPG_CHARACTERS_LIST: i32 = 10;
pub const RPG_ENTRIES_LIST: i32 = 11;
pub const SINGLE_RPG_SESSION: i32 = 12;
pub const USERS_RELATED_RPG_CHARACTERS_LIST: i32 = 13;
pub const RPG_SESSIONS_RPG_CHARACTERS_LIST: i32 = 14;

pub const DATABASE_UNAVAILABLE: i32 = 30;

/* errors */
pub const DELIVERY_FAILURE: i32 = 201;
pub const SAVE_EMAIL_FAILURE: i32 = 202;
pub const USER_NOT_FOUND: i32 = 203;
pub const EMAIL_ALREADY_TAKEN: i32 = 204;
pub const CREATE_TOKEN_FAILURE: i32 = 205;
pub const TOKEN_NOT_FOUND: i32 = 206;
pub const PARSE_TEMPLATE_FAILURE: i32 = 207;
pub const INVALID_UUID_FORMAT: i32 = 208;
pub const NICKNAME_ALREADY_TAKEN: i32 = 209;
pub const NO_RPG_SESSIONS_FOUND: i32 = 210;
pub const NO_USERS_FOUND: i32 = 211;

/* output */
pub const UNRECOGNIZED_WEB_SOCKET_MSG: i32 = 0;
pub const OP_USER: i32 = 400;
pub const OP_TOKEN: i32 = 401;
pub const OP_CURRENT_USER: i32 = 402;
pub const OP_VALID_TOKEN: i32 = 403;
pub const OP_RPG_SESSIONS_LIST: i32 = 404;
pub const OP_USERS_LIST: i32 = 405;
pub const OP_RPG_CHARACTERS_LIST: i32 = 406;
pub const OP_RPG_ENTRIES_LIST: i32 = 407;
pub const OP_RPG_SESSION: i32 = 408;
pub const OP_RPG_SESSIONS_RPG_CHARACTERS: i32 = 409;
pub const OP_DATABASE_UNAVAILABLE: i32 = 509;
