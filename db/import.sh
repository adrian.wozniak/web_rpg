#!/usr/bin/env bash

psql web_rpg -f ./users.psql
psql web_rpg -f ./tokens.psql
psql web_rpg -f ./rpg_sessions.psql
psql web_rpg -f ./rpg_characters.psql
psql web_rpg -f ./rpg_entries.psql
psql web_rpg -f ./create_rpg_character_images.psql
