#!/bin/bash

ROOT=$(pwd)

cargo build --verbose -p web-rpg-server && \
cargo build --release --target wasm32-unknown-unknown --verbose -p web-rpg-client && \
cd web-rpg-client && yarn && wasm-pack build && cp $ROOT/web-rpg-client/pkg/* $ROOT/web/src/native
