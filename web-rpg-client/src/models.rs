#![allow(proc_macro_derive_resolution_fallback)]

use std::io::prelude::*;

use chrono::naive::NaiveDateTime;
use uuid::Uuid;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Token {
    pub id: i32,
    pub access_token: Uuid,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct TokenForm {
    pub access_token: Uuid,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub nickname: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct UserForm {
    pub email: String,
    pub nickname: String,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgSession {
    pub id: i32,
    pub name: String,
    pub user_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgSessionForm {
    pub name: String,
    pub user_id: i32,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgEntry {
    pub id: i32,
    pub user_id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub input: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgEntryForm {
    pub user_id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub input: String,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgCharacter {
    pub id: i32,
    pub name: String,
    pub user_id: i32,
    pub statistics: Vec<String>,
    pub stat_type: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub image_url: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgCharacterForm {
    pub name: String,
    pub user_id: i32,
    pub statistics: Vec<String>,
    pub stat_type: String,
    pub updated_at: NaiveDateTime,
    pub image_url: String,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgSessionsRpgCharacter {
    pub id: i32,
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct RpgSessionsRpgCharacterForm {
    pub rpg_character_id: i32,
    pub rpg_session_id: i32,
}
