#![allow(unused_imports)]
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate bincode;
extern crate futures;
extern crate uuid;
#[macro_use]
extern crate web_sys;
#[macro_use]
extern crate js_sys;
#[macro_use]
extern crate wasm_bindgen;
extern crate web_rpg_shared;

use wasm_bindgen::prelude::*;

pub mod models;
pub mod requests;
mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("Hello, web-rpg-client!");
}
