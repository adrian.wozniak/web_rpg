use futures::future::Future;
use js_sys::*;
use serde::*;
use uuid;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys;
use web_sys::{BinaryType, Blob, ErrorEvent, MessageEvent, WebSocket, Window};

use web_rpg_shared;

macro_rules! console_log {
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen(start)]
pub fn establish_connection() -> Result<(), JsValue> {
    let window = match web_sys::window() {
        Some(window) => window,
        None => return Err("Window object not found!".into()),
    };
    let local_storage = match window.local_storage()? {
        Some(local_storage) => local_storage,
        None => return Err("LocalStorage object not found!".into()),
    };
    let access_token: String = match local_storage.get_item("access-token")? {
        Some(str) => str,
        None => return Err("Access token not found".into()),
    };
    console_log!("Access token is: {:?}", access_token);
    let parsed_access_token = match uuid::Uuid::parse_str(access_token.as_str()) {
        Ok(uuid) => uuid,
        Err(e) => return Err(format!("Invalid uuid: {:?}", e).into()),
    };
    console_log!("Access token is: {:?}", parsed_access_token);

    let server_address: &'static str = if cfg!(release) {
        "wss://web_rpg.ita-prog.pl/ws/"
    } else {
        "ws://0.0.0.0:3678/ws/"
    };

    let ws = match WebSocket::new(server_address) {
        Ok(ws) => ws,
        Err(err) => {
            console_log!("Failed to created web socket!!!");
            return Err(err);
        }
    };
    ws.set_binary_type(BinaryType::Blob);
    let onmessage_callback = Closure::wrap(Box::new(move |event: MessageEvent| {
        // handle message
        console_log!("message event, received event: {:?}", event);
        let data = event.data();
        console_log!("message event, received data: {:?}", data);
        let uint8 = Uint8Array::new(&data);
        console_log!("message event, received data: {:?}", uint8);
        let mut a: Vec<u8> = Vec::new();
        a.reserve(uint8.length() as usize);
        a.resize(uint8.length() as usize, 8u8);
        uint8.copy_to(a.as_mut_slice());

        console_log!("message event, received data: {:?}", a);
    }) as Box<dyn FnMut(MessageEvent)>);
    ws.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));
    onmessage_callback.forget();

    let onerror_callback = Closure::wrap(Box::new(move |e: ErrorEvent| {
        console_log!("error event: {:?}", e);
    }) as Box<dyn FnMut(ErrorEvent)>);
    ws.set_onerror(Some(onerror_callback.as_ref().unchecked_ref()));
    onerror_callback.forget();

    let cloned_ws = ws.clone();
    let onopen_callback = Closure::wrap(Box::new(move |_| {
        console_log!("socket opened");
        match cloned_ws.ready_state() {
            1 => {
                let mut payload: Vec<u8> =
                    bincode::serialize(&web_rpg_shared::op_codes::CURRENT_USER).unwrap();
                payload.append(&mut bincode::serialize(&parsed_access_token).unwrap());
                cloned_ws
                    .send_with_u8_array(&mut payload.as_mut_slice())
                    .unwrap_or_else(|e| console_log!("Failed to send request {:?}", e));
                console_log!("Buffer send {:?}", payload);
            }
            _ => console_log!("WebSocket is not ready"),
        };
    }) as Box<dyn FnMut(JsValue)>);
    ws.set_onopen(Some(onopen_callback.as_ref().unchecked_ref()));
    onopen_callback.forget();

    Ok(())
}
