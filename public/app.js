const socketUrl = () => {
    const protocol = location.protocol === 'https' ? 'wss' : 'ws';
    const { host } = location;
    return `${protocol}://${host}/ws/`;
};

const createSocket = () => new WebSocket(socketUrl());

document.addEventListener('DOMContentLoaded', () => {
    const app = Elm.Main.init({
        node: document.querySelector('#elm-port')
    });
    window.app = app;
    console.log(app);
    app.ports.cache.subscribe(function (data) {
        localStorage.setItem('cache', JSON.stringify(data));
    });

    // const ws = createSocket();
    // ws.addEventListener('open', (event) => {
    //     console.log(event);
    //     ws.send(JSON.stringify({ type: 'hello' }));
    //     const bin = new Int32Array(10);
    //     for (let i = 0; i < 10; i++) bin[i] = 20 + (i * i + i);
    //     ws.send(bin.buffer);
    //
    //     ws.addEventListener('message', (event) => {
    //         console.log(event);
    //     });
    //     ws.addEventListener('close', (event) => {
    //         console.log(event);
    //     });
    //     ws.addEventListener('error', (event) => {
    //         console.log(event);
    //     });
    // });
});
