import { combineReducers } from "redux";

import * as types from "./types";
import { unique } from "./utils";

const list = (state = [], { type, payload }) => {
    switch (type) {
        case types.RPG_ENTRIES_RECEIVED:
            return [
                ...state.filter(o => !payload.find(e => e.id === o.id)),
                ...payload.sort((a, b) => {
                    if (a.id > b.id) return 1;
                    else if (a.id < b.id) return -1;
                    return 0;
                })
            ];
        default:
            return state;
    }
};

const byId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_ENTRIES_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.id]: o,
            }), { ...state });
        default:
            return state;
    }
};

const byUserId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_ENTRIES_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.userId]: unique([
                    ...(memo[o.userId] || []),
                    o.id,
                ]),
            }), { ...state });
        default:
            return state;
    }
};

const byRpgSessionId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_ENTRIES_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.rpgSessionId]: unique([
                    ...(memo[o.rpgSessionId] || []),
                    o.id,
                ]),
            }), { ...state });
        default:
            return state;
    }
};

export default combineReducers({
    byId,
    byUserId,
    byRpgSessionId,
    list,
});

export const getByUserId = ({ rpgEntries: { byUserId } }) => byUserId;
export const getByRpgSessionId = ({ rpgSessionId, state: { rpgEntries: { byId, byRpgSessionId } } }) =>
    (byRpgSessionId[rpgSessionId] || []).map(id => byId[id]);
