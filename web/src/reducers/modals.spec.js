import faker from "faker";

import * as reducer from "./modals";
import * as types   from "reducers/types";

describe("modals.nickname", () => {
    let state;
    beforeEach(() => {
        localStorage.clear();
        state = { isOpen: false, nickname: "", errors: [] };
    });

    test("INITIAL", () => {
        const type = Symbol("INITIAL");
        const action = { type };
        const expected = { isOpen: false, nickname: "", errors: [] };
        const result = reducer.nickname(undefined, action);
        expect(result).toEqual(expected);
    });

    test("CLOSE_NICKNAME_MODAL", () => {
        const type = types.CLOSE_NICKNAME_MODAL;
        const action = { type };
        const expected = { ...state, isOpen: false };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("OPEN_NICKNAME_MODAL", () => {
        const type = types.OPEN_NICKNAME_MODAL;
        const action = { type };
        const expected = { ...state, isOpen: true };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("NICKNAME_MODAL_INPUT", () => {
        const nickname = faker.internet.userName();
        const type = types.NICKNAME_MODAL_INPUT;
        const action = { type, payload: { nickname } };
        const expected = { ...state, nickname };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("CURRENT_USER_RECEIVED", () => {
        const id = faker.random.number(1000);
        const nickname = faker.internet.userName();
        const type = types.CURRENT_USER_RECEIVED;
        const action = { type, payload: { nickname, id } };
        const expected = { ...state, nickname };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("NICKNAME_ALREADY_TAKEN without errors", () => {
        const type = types.NICKNAME_ALREADY_TAKEN;
        const action = { type };
        const expected = { ...state, errors: ["alreadyTaken"] };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("NICKNAME_ALREADY_TAKEN with errors", () => {
        state = { ...state, errors: ["foo", "bar"] };
        const type = types.NICKNAME_ALREADY_TAKEN;
        const action = { type };
        const expected = { ...state, errors: ["alreadyTaken", "bar", "foo"] };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("NICKNAME_ALREADY_TAKEN with 'alreadyTaken' error", () => {
        state = { ...state, errors: ["alreadyTaken"] };
        const type = types.NICKNAME_ALREADY_TAKEN;
        const action = { type };
        const expected = { ...state, errors: ["alreadyTaken"] };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });

    test("NICKNAME_ALREADY_TAKEN with 'alreadyTaken' and other errors", () => {
        state = { ...state, errors: ["foo", "alreadyTaken", "bar"] };
        const type = types.NICKNAME_ALREADY_TAKEN;
        const action = { type };
        const expected = { ...state, errors: ["alreadyTaken", "bar", "foo"] };
        const result = reducer.nickname(state, action);
        expect(result).toEqual(expected);
    });
});

describe("isNicknameModalOpen", () => {
    const getter = reducer.isNicknameModalOpen;

    test("modal is closed", () => {
        const isOpen = false;
        const state = { modals: { nickname: { isOpen } } };
        expect(getter(state)).toEqual(isOpen);
    });

    test("modal is open", () => {
        const isOpen = true;
        const state = { modals: { nickname: { isOpen } } };
        expect(getter(state)).toEqual(isOpen);
    });
});

describe("getNicknameModalErrors", () => {
    const getter = reducer.getNicknameModalErrors;

    test("modal have errors", () => {
        const errors = ["foo", "bar"];
        const state = { modals: { nickname: { errors } } };
        expect(getter(state)).toEqual(errors);
    });

    test("modal does not have errors", () => {
        const errors = [];
        const state = { modals: { nickname: { errors } } };
        expect(getter(state)).toEqual(errors);
    });
});
