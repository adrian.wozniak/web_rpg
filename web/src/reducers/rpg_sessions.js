import { combineReducers } from "redux";

import * as types from "./types";

const list = (state = [], { type, payload }) => {
    switch (type) {
        case types.RPG_SESSIONS_RECEIVED:
            return [...payload];
        case types.RPG_SESSION_RECEIVED:
            return [
                ...state.filter(o => o.id !== payload.id),
                payload,
            ];
        default:
            return state;
    }
};

const byId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_SESSIONS_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.id]: o,
            }), {});
        case types.RPG_SESSION_RECEIVED:
            return {
                ...state,
                [payload.id]: payload,
            };
        default:
            return state;
    }
};

const byUserId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_SESSIONS_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.userId]: [
                    ...(memo[o.userId] || []),
                    o.id,
                ],
            }), {});
        case types.RPG_SESSION_RECEIVED:
            return {
                ...state,
                [payload.userId]: [
                    ...(state[payload.userId] || []),
                    payload.id,
                ],
            };
        default:
            return state;
    }
};

export default combineReducers({
    byId,
    byUserId,
    list,
});

export const getAllRpgSessions = ({ rpgSessions: { list } }) => list;

export const getRpgSessionsAuthorsIds = ({ rpgSessions: { byUserId } }) =>
    Array.from(Object.keys(byUserId).map(s => parseInt(s))).sort((a, b) => {
        if (a > b) return 1;
        else if (b < a) return -1;
        return 0;
    });

export const getByUserId = ({ rpgSessions: { byUserId } }) => byUserId;
