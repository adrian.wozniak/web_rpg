import * as objects from "spec/objects";
import * as reducer from "./rpg_characters";
import * as types   from "reducers/types";
import { sortById } from "../utils/sort";

describe("rpg_characters.list", () => {
    let state;
    beforeEach(() => {
        localStorage.clear();
        state = [];
    });

    test("INITIAL", () => {
        const type = Symbol("INITIAL");
        const action = { type };
        const expected = [];
        const result = reducer.list(undefined, action);
        expect(result).toEqual(expected);
    });

    test("RPG_CHARACTERS_RECEIVED with 3 rpg characters", () => {
        const payload = objects.getRpgCharacters(3);
        const expected = [...payload].sort(sortById);
        const type = types.RPG_CHARACTERS_RECEIVED;
        const action = { type, payload: payload };
        const result = reducer.list(state, action);
        expect(result).toEqual(expected);
    });

    test("RPG_CHARACTERS_RECEIVED with 1 old unique, 1 duplication and 1 new", () => {
        const uniqueCharacter = objects.getRpgCharacter({ id: 4 });
        const newCharacter = objects.getRpgCharacter({ id: 44 });
        const duplicationCharacter = objects.getRpgCharacter({ id: 444 });

        const payload = [newCharacter, duplicationCharacter];
        const state = [uniqueCharacter, duplicationCharacter];
        const expected = [newCharacter, uniqueCharacter, duplicationCharacter]
            .sort(sortById);
        const type = types.RPG_CHARACTERS_RECEIVED;
        const action = { type, payload: payload };
        const result = reducer.list(state, action);
        expect(result).toEqual(expected);
    });
});

describe("rpg_characters.byId", () => {
    let state;
    beforeEach(() => {
        localStorage.clear();
        state = {};
    });

    test("INITIAL", () => {
        const type = Symbol("INITIAL");
        const action = { type };
        const expected = {};
        const result = reducer.byId(undefined, action);
        expect(result).toEqual(expected);
    });

    test("RPG_CHARACTERS_RECEIVED with 3 rpg characters", () => {
        const payload = objects.getRpgCharacters(3);
        const expected = payload.reduce((m, o) => ({
            ...m,
            [o.id]: o
        }), {});
        const type = types.RPG_CHARACTERS_RECEIVED;
        const action = { type, payload: payload };
        const result = reducer.byId(state, action);
        expect(result).toEqual(expected);
    });

    test("RPG_CHARACTERS_RECEIVED with 1 old unique, 1 duplication and 1 new", () => {
        const uniqueCharacter = objects.getRpgCharacter({ id: 4 });
        const newCharacter = objects.getRpgCharacter({ id: 44 });
        const duplicationCharacter = objects.getRpgCharacter({ id: 444 });

        const payload = [newCharacter, duplicationCharacter];
        const state = {
            [uniqueCharacter.id]:      uniqueCharacter,
            [duplicationCharacter.id]: duplicationCharacter
        };
        const expected = {
            [newCharacter.id]:         newCharacter,
            [uniqueCharacter.id]:      uniqueCharacter,
            [duplicationCharacter.id]: duplicationCharacter
        };
        const type = types.RPG_CHARACTERS_RECEIVED;
        const action = { type, payload: payload };
        const result = reducer.byId(state, action);
        expect(result).toEqual(expected);
    });
});

describe("rpg_characters.byUserId", () => {
    let state;
    beforeEach(() => {
        localStorage.clear();
        state = {};
    });

    test("INITIAL", () => {
        const type = Symbol("INITIAL");
        const action = { type };
        const expected = {};
        const result = reducer.byUserId(undefined, action);
        expect(result).toEqual(expected);
    });

    test("RPG_CHARACTERS_RECEIVED with 3 rpg characters", () => {
        const payload = objects.getRpgCharacters(3);
        const expected = payload.reduce((m, o) => ({
            ...m,
            [o.userId]: [...(m[o.userId] || []), o.id]
        }), {});
        const type = types.RPG_CHARACTERS_RECEIVED;
        const action = { type, payload: payload };
        const result = reducer.byUserId(state, action);
        expect(result).toEqual(expected);
    });
});

test("rpg_characters.getByUserId", () => {
    const getter = reducer.getByUserId;
    const { userId, id } = objects.getRpgCharacter();
    const byUserId = { [userId]: [id] };
    const state = { rpgCharacters: { byUserId } };
    expect(getter(state)).toEqual(byUserId);
});

test("rpg_characters.getUserRpgCharacters", () => {
    const getter = reducer.getUserRpgCharacters;
    const rpgCharacter = objects.getRpgCharacter();
    const byUserId = { [rpgCharacter.userId]: [rpgCharacter.id] };
    const byId = { [rpgCharacter.id]: rpgCharacter };
    const state = { rpgCharacters: { byUserId, byId } };
    expect(getter({ state, userId: rpgCharacter.userId })).toEqual([rpgCharacter]);
});
