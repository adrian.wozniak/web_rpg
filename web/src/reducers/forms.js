import { combineReducers } from "redux";
import V                   from "functional-validators";

import * as types from "./types";

const emailValidator = V.and(V.required(), V.email());

const initSignUp = () => ({
    email:       "",
    emailErrors: [],
    errors:      []
});

const initSignIn = () => ({
    email:       "",
    emailErrors: [],
    errors:      []
});

export const signUp = (state = initSignUp(), { type, payload }) => {
    switch (type) {
        case types.SIGN_UP_INPUT: {
            const { email } = payload;
            const validation = emailValidator(email);
            return ({
                ...state,
                emailErrors: Array.isArray(validation) ? validation : [],
                email
            });
        }
        case types.DATABASE_UNAVAILABLE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.databaseUnavailable"
                ]
            };
        case types.DELIVERY_FAILURE:
            return {
                ...state,
                errors: [
                    ...(new Set([
                        ...state.errors,
                        "signUp.errors.deliveryFailure"
                    ]))
                ]
            };
        case types.SAVE_EMAIL_FAILURE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.saveEmailFailure"
                ]
            };
        case types.USER_NOT_FOUND:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.userNotFound"
                ]
            };
        case types.EMAIL_ALREADY_TAKEN:
            return {
                ...state,
                errors: [
                            ...state.errors,
                            "signUp.errors.emailAlreadyTaken"
                        ].sort()
            };
        default:
            return state;
    }
};

export const signIn = (state = initSignIn(), { type, payload }) => {
    switch (type) {
        case types.SIGN_IN_INPUT: {
            const { email } = payload;
            const validation = emailValidator(email);
            return ({
                ...state,
                emailErrors: Array.isArray(validation) ? validation : [],
                email
            });
        }
        case types.DATABASE_UNAVAILABLE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.databaseUnavailable"
                ]
            };
        case types.DELIVERY_FAILURE:
            return {
                ...state,
                errors: [
                    ...(new Set([
                        ...state.errors,
                        "signUp.errors.deliveryFailure"
                    ]))
                ]
            };
        case types.SAVE_EMAIL_FAILURE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.saveEmailFailure"
                ]
            };
        case types.USER_NOT_FOUND:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.userNotFound"
                ]
            };
        case types.EMAIL_ALREADY_TAKEN:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "signUp.errors.emailAlreadyTaken"
                ]
            };
        default:
            return state;
    }
};

const intRpgEntryForm = () => ({
    rpgCharacterId: null,
    userId:         null,
    rpgSessionId:   null,
    input:          ""
});

export const rpgEntry = (state = intRpgEntryForm(), { type, payload }) => {
    switch (type) {
        case types.RPG_ENTRY_FORM_RPG_CHARACTER_ID_CHANGED: {
            const { rpgCharacterId } = payload;
            return { ...state, rpgCharacterId };
        }
        case types.RPG_ENTRY_FORM_USER_ID_CHANGED: {
            const { userId } = payload;
            return { ...state, userId };
        }
        case types.RPG_ENTRY_FORM_RPG_SESSION_ID_CHANGED: {
            const { rpgSessionId } = payload;
            return { ...state, rpgSessionId };
        }
        case types.RPG_ENTRY_FORM_INPUT_CHANGED: {
            const { input } = payload;
            return { ...state, input };
        }
        default:
            return state;
    }
};

export default combineReducers({ signIn, signUp, rpgEntry });
