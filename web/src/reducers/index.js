import { combineReducers } from "redux";
import { reducers }        from "react-markdown-editor";

import pages         from "./pages";
import users         from "./users";
import tokens        from "./tokens";
import forms         from "./forms";
import modals        from "./modals";
import rpgSessions   from "./rpg_sessions";
import rpgCharacters from "./rpg_characters";
import rpgEntries    from "./rpg_entries";

export default combineReducers({
    pages,
    users,
    tokens,
    forms,
    modals,
    rpgSessions,
    rpgCharacters,
    rpgEntries,
    rpgEntryEditor: reducers
});
