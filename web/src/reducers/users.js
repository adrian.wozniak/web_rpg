import { combineReducers } from "redux";

import * as types from "./types";

const initState = () => null;

const current = (state = initState(), { type, payload }) => {
    switch (type) {
        case types.CURRENT_USER_RECEIVED:
            return payload;
        default:
            return state;
    }
};

const byId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.CURRENT_USER_RECEIVED:
            return {
                ...state,
                [payload.id]: payload,
            };
        case types.USERS_RECEIVED:
            return payload.reduce((memo, user) => ({
                ...memo,
                [user.id]: user,
            }), state);
        default:
            return state;
    }
};

export default combineReducers({ current, byId });

export const getCurrentUserNickname = ({ users: { current } }) =>
    current ? current.nickname : null;

export const getCurrentUserId = ({ users: { current } }) =>
    current ? current.id : null;
