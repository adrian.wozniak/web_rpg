import { combineReducers } from "redux";

import * as types   from "./types";

import { sortById } from "../utils/sort";

const joinWithPayload = ({ state, payload }) => {
    const ids = payload.reduce((m, c) => ({ ...m, [c.id]: 1 }), {});
    return [
        ...state.filter(c => !ids[c.id]),
        ...payload,
    ].sort(sortById);
};

export const list = (state = [], { type, payload }) => {
    switch (type) {
        case types.RPG_CHARACTERS_RECEIVED:
            return joinWithPayload({ state, payload });
        default:
            return state;
    }
};

export const byId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_CHARACTERS_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.id]: o,
            }), { ...state });
        default:
            return state;
    }
};

export const byUserId = (state = {}, { type, payload }) => {
    switch (type) {
        case types.RPG_CHARACTERS_RECEIVED:
            return payload.reduce((memo, o) => ({
                ...memo,
                [o.userId]: [
                    ...(memo[o.userId] || []),
                    o.id,
                ],
            }), { ...state });
        default:
            return state;
    }
};

export default combineReducers({
    byId,
    byUserId,
    list,
});

export const getByUserId = ({ rpgCharacters: { byUserId } }) => byUserId;

export const getUserRpgCharacters = ({ state: { rpgCharacters }, userId }) =>
    (rpgCharacters.byUserId[userId] || []).map(id => (
        rpgCharacters.byId[id]
    ));
