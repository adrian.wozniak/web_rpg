import * as types from "./types";

export const ACCESS_TOKEN_KEY = "access-token";

const initState = () => ({
    accessToken: localStorage.getItem(ACCESS_TOKEN_KEY) || null,
    errors:      [],
});

export const tokens = (state = initState(), { type, payload }) => {
    switch (type) {
        case types.TOKEN_RECEIVED:
            // console.log("Incoming token %o", payload.token);
            // console.log("Stored token %o", localStorage.getItem(ACCESS_TOKEN_KEY));
            localStorage.setItem(ACCESS_TOKEN_KEY, payload.token);
            return {
                ...state,
                ...payload,
                accessToken: payload.token,
            };
        case types.DATABASE_UNAVAILABLE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "tokens.errors.databaseUnavailable",
                ]
            };
        case types.CREATE_TOKEN_FAILURE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "tokens.errors.createTokenFailure",
                ]
            };
        case types.TOKEN_NOT_FOUND:
            console.error(`token not found uuid: ${localStorage.getItem(ACCESS_TOKEN_KEY)}`);
            localStorage.removeItem(ACCESS_TOKEN_KEY);
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "tokens.errors.tokenNotFound",
                ]
            };
        case types.PARSE_TEMPLATE_FAILURE:
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "tokens.errors.parseTemplateFailure",
                ]
            };
        case types.INVALID_UUID_FORMAT:
            console.error(`invalid uuid: ${localStorage.getItem(ACCESS_TOKEN_KEY)}`);
            localStorage.removeItem(ACCESS_TOKEN_KEY);
            return {
                ...state,
                errors: [
                    ...state.errors,
                    "tokens.errors.invalidUuidFormat",
                ]
            };
        default:
            return state;
    }
};

export default tokens;

export const getStoredToken = () => localStorage.getItem(ACCESS_TOKEN_KEY);
