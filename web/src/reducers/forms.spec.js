import faker from "faker";

import * as actions from "actions";
import * as forms   from "./forms";
import * as types   from "reducers/types";

describe("forms signUp reducer", () => {
    test("INITIAL", () => {
        const expected = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(undefined, { type: Symbol("INITIAL") });
        expect(result).toEqual(expected);
    });

    test("SIGN_UP_INPUT with valid email", () => {
        const email = faker.internet.email().toLocaleLowerCase();
        const expected = { email, errors: [], emailErrors: [] };
        const result = forms.signUp(undefined, actions.signUpInput(email));
        expect(result).toEqual(expected);
    });

    test("SIGN_UP_INPUT with invalid email", () => {
        const email = faker.internet.userName().toLocaleLowerCase();
        const expected = { email, errors: [], emailErrors: ["invalid email"] };
        const result = forms.signUp(undefined, actions.signUpInput(email));
        expect(result).toEqual(expected);
    });

    test("DATABASE_UNAVAILABLE", () => {
        const action = { type: types.DATABASE_UNAVAILABLE };
        const error = "signUp.errors.databaseUnavailable";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(state, action);
        expect(result).toEqual(expected);
    });

    test("DELIVERY_FAILURE", () => {
        const action = { type: types.DELIVERY_FAILURE };
        const error = "signUp.errors.deliveryFailure";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(state, action);
        expect(result).toEqual(expected);
    });

    test("SAVE_EMAIL_FAILURE", () => {
        const action = { type: types.SAVE_EMAIL_FAILURE };
        const error = "signUp.errors.saveEmailFailure";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(state, action);
        expect(result).toEqual(expected);
    });

    test("USER_NOT_FOUND", () => {
        const action = { type: types.USER_NOT_FOUND };
        const error = "signUp.errors.userNotFound";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(state, action);
        expect(result).toEqual(expected);
    });

    test("EMAIL_ALREADY_TAKEN", () => {
        const action = { type: types.EMAIL_ALREADY_TAKEN };
        const error = "signUp.errors.emailAlreadyTaken";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signUp(state, action);
        expect(result).toEqual(expected);
    });
});

describe("forms signIn reducer", () => {
    test("INITIAL", () => {
        const expected = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(undefined, { type: Symbol("INITIAL") });
        expect(result).toEqual(expected);
    });

    test("SIGN_IN_INPUT with valid email", () => {
        const email = faker.internet.email().toLocaleLowerCase();
        const expected = { email, errors: [], emailErrors: [] };
        const result = forms.signIn(undefined, actions.signInInput(email));
        expect(result).toEqual(expected);
    });

    test("SIGN_IN_INPUT with invalid email", () => {
        const email = faker.internet.userName().toLocaleLowerCase();
        const expected = { email, errors: [], emailErrors: ["invalid email"] };
        const result = forms.signIn(undefined, actions.signInInput(email));
        expect(result).toEqual(expected);
    });

    test("DATABASE_UNAVAILABLE", () => {
        const action = { type: types.DATABASE_UNAVAILABLE };
        const error = "signUp.errors.databaseUnavailable";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(state, action);
        expect(result).toEqual(expected);
    });

    test("DELIVERY_FAILURE", () => {
        const action = { type: types.DELIVERY_FAILURE };
        const error = "signUp.errors.deliveryFailure";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(state, action);
        expect(result).toEqual(expected);
    });

    test("SAVE_EMAIL_FAILURE", () => {
        const action = { type: types.SAVE_EMAIL_FAILURE };
        const error = "signUp.errors.saveEmailFailure";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(state, action);
        expect(result).toEqual(expected);
    });

    test("USER_NOT_FOUND", () => {
        const action = { type: types.USER_NOT_FOUND };
        const error = "signUp.errors.userNotFound";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(state, action);
        expect(result).toEqual(expected);
    });

    test("EMAIL_ALREADY_TAKEN", () => {
        const action = { type: types.EMAIL_ALREADY_TAKEN };
        const error = "signUp.errors.emailAlreadyTaken";
        const expected = { email: "", errors: [error], emailErrors: [] };
        const state = { email: "", errors: [], emailErrors: [] };
        const result = forms.signIn(state, action);
        expect(result).toEqual(expected);
    });
});
