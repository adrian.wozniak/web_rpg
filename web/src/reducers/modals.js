import { combineReducers } from "redux";

import * as types from "./types";

const initState = () => ({
    isOpen:   false,
    nickname: "",
    errors:   [],
});

export const nickname = (state = initState(), { type, payload }) => {
    switch (type) {
        case types.CLOSE_NICKNAME_MODAL:
            return { ...state, isOpen: false };
        case types.OPEN_NICKNAME_MODAL:
            return { ...state, isOpen: true };
        case types.NICKNAME_MODAL_INPUT: {
            const { nickname } = payload;
            return { ...state, nickname };
        }
        case types.CURRENT_USER_RECEIVED: {
            const { nickname } = payload;
            return {
                ...state,
                nickname,
                isOpen: nickname === "" || nickname === "User",
            };
        }
        case types.NICKNAME_ALREADY_TAKEN:
            return {
                ...state,
                errors: [
                    ...state.errors.filter(s => s !== "alreadyTaken"),
                    "alreadyTaken"
                ].sort()
            };
        default:
            return state;
    }
};

export default combineReducers({
    nickname,
});

export const isNicknameModalOpen = ({ modals: { nickname: { isOpen } } }) =>
    isOpen;

export const getNicknameModalErrors = ({ modals: { nickname: { errors } } }) =>
    errors;
