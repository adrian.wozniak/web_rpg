import faker from "faker";

import * as reducer from "./tokens";
import * as types   from "reducers/types";

describe("tokens", () => {
    let state;
    beforeEach(() => {
        localStorage.clear();
        state = { accessToken: null, errors: [] };
    });

    test("INITIAL", () => {
        const type = Symbol("INITIAL");
        const action = { type };
        const expected = { accessToken: null, errors: [] };
        const result = reducer.tokens(undefined, action);
        expect(result).toEqual(expected);
    });

    test("TOKEN_RECEIVED", () => {
        const token = faker.random.uuid();
        const type = types.TOKEN_RECEIVED;
        const action = { type, payload: { token } };
        const expected = { accessToken: token, token, errors: [] };
        const result = reducer.tokens(state, action);
        expect(result).toEqual(expected);
    });

    test("DATABASE_UNAVAILABLE", () => {
        const type = types.DATABASE_UNAVAILABLE;
        const action = { type };
        const error = "tokens.errors.databaseUnavailable";
        const expected = { accessToken: null, errors: [error] };
        const result = reducer.tokens(state, action);
        expect(result).toEqual(expected);
    });

    test("CREATE_TOKEN_FAILURE", () => {
        const type = types.CREATE_TOKEN_FAILURE;
        const error = "tokens.errors.createTokenFailure";
        const expected = { ...state, errors: [error] };
        const payload = { type };
        const result = reducer.tokens(state, payload);
        expect(result).toEqual(expected);
    });

    test("TOKEN_NOT_FOUND", () => {
        const type = types.TOKEN_NOT_FOUND;
        const error = "tokens.errors.tokenNotFound";
        const expected = { ...state, errors: [error] };
        const payload = { type };
        const result = reducer.tokens(state, payload);
        expect(result).toEqual(expected);
    });

    test("PARSE_TEMPLATE_FAILURE", () => {
        const type = types.PARSE_TEMPLATE_FAILURE;
        const error = "tokens.errors.parseTemplateFailure";
        const expected = { ...state, errors: [error] };
        const payload = { type };
        const result = reducer.tokens(state, payload);
        expect(result).toEqual(expected);
    });

    test("INVALID_UUID_FORMAT", () => {
        const type = types.INVALID_UUID_FORMAT;
        const error = "tokens.errors.invalidUuidFormat";
        const expected = { ...state, errors: [error] };
        const payload = { type };
        const result = reducer.tokens(state, payload);
        expect(result).toEqual(expected);
    });
});
