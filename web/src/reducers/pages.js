import { combineReducers } from "redux";

import * as types from "./types";

const LANDING_PAGE_PAGE_KEY = "landing-page_page-key";

const landingPageInitState = (page = parseInt(localStorage.getItem(LANDING_PAGE_PAGE_KEY))) => ({
    page:  Number.isNaN(page) ? 1 : page,
    limit: 100,
});

export const landingPage = (state = landingPageInitState(), { type, payload }) => {
    switch (type) {
        case types.LANDING_PAGE_CHANGE_PAGE: {
            const { page } = payload;
            return { ...state, page };
        }
        default:
            return state;
    }
};

export const rpgSessionPage = (state = {}, { type }) => {
    switch (type) {
        default:
            return state;
    }
};

export default combineReducers({
    landingPage,
    rpgSessionPage,
});
