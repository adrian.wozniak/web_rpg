import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import { signUpInput, signUpSubmit } from "actions";

import * as styles from "./styles.modules.css";

const SignUp = ({ onSignUpSubmit, onSignUpInput, signUp, t }) => (
    <form onSubmit={onSignUpSubmit} className={styles.form}>
        <fieldset className={styles.fieldset}>
            <legend className={styles.legend}>
                {t("legend")}
            </legend>
            <input
                className={styles.input}
                type='email'
                value={signUp.email}
                onChange={onSignUpInput}
                placeholder={t("email")}
            />
            {
                Array.isArray(signUp.emailErrors)
                    ? signUp.emailErrors.map((msg, idx) => (
                        <p className={styles.error} key={`email-error-${idx}`}>
                            {msg}
                        </p>
                    ))
                    : null
            }
            {
                signUp.errors.map((error, idx) => (
                    <p className={styles.error} key={`error-${idx}`}>
                        {t(error.replace(/signUp\./, ""))}
                    </p>
                ))
            }
            <input
                className={styles.submit}
                type='submit'
                value={t("submit")}
            />
        </fieldset>
    </form>
);

SignUp.propTypes = {
    onSignUpSubmit: PropTypes.func,
    onSignUpInput:  PropTypes.func,
    signUp:         PropTypes.shape({
        email:       PropTypes.string,
        emailErrors: PropTypes.arrayOf(PropTypes.string),
        errors:      PropTypes.arrayOf(PropTypes.string)
    }),
    t:              PropTypes.func
};

const mapStateToProps = ({ forms: { signUp } }) => ({
    signUp
});
const mapDispatchToProps = (dispatch) => ({
    "onSignUpSubmit":
        (event) => {
            event.preventDefault();
            dispatch(signUpSubmit({}));
        },
    "onSignUpInput":
        ({ target: { value: email } }) =>
            dispatch(signUpInput(email))
});

export default withNamespaces("signUp")(connect(mapStateToProps, mapDispatchToProps)(SignUp));
