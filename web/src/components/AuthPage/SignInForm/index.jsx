import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import { signInInput, signInSubmit } from "actions";

import * as styles from "./styles.modules.css";

const SignIn = ({ onSignInSubmit, onSignInInput, signIn, t }) => (
    <form onSubmit={onSignInSubmit} className={styles.form}>
        <fieldset className={styles.fieldset}>
            <legend className={styles.legend}>
                {t("legend")}
            </legend>
            <input
                className={styles.input}
                type='email'
                value={signIn.email}
                onChange={onSignInInput}
                placeholder={t("email")}
            />
            {
                Array.isArray(signIn.errors)
                    ? signIn.errors.map((msg, idx) => (
                        <p className={styles.error} key={`error-${idx}`}>
                            {msg}
                        </p>
                    ))
                    : null
            }
            <input
                className={styles.submit}
                type='submit'
                value={t("submit")}
            />
        </fieldset>
    </form>
);
SignIn.propTypes = {
    onSignInSubmit: PropTypes.func,
    onSignInInput:  PropTypes.func,
    signIn:         PropTypes.shape({
        email:       PropTypes.string,
        emailErrors: PropTypes.arrayOf(PropTypes.string),
        errors:      PropTypes.arrayOf(PropTypes.string)
    }),
    t:              PropTypes.func
};

const mapStateToProps = ({ forms: { signIn } }) => ({
    signIn
});
const mapDispatchToProps = (dispatch) => ({
    "onSignInSubmit": (event) => {
        event.preventDefault();
        dispatch(signInSubmit({}));
    },
    "onSignInInput": ({ target: { value: email } }) => dispatch(signInInput(email))
});

export default withNamespaces("signIn")(connect(mapStateToProps, mapDispatchToProps)(SignIn));
