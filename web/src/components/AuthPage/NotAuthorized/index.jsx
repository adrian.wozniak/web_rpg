import React       from "react";

import SignUpForm from "components/AuthPage/SignUpForm";
import SignInForm from "components/AuthPage/SignInForm";

import * as styles from "./styles.modules.css";

const NotAuthorized = () => (
    <section className={styles.notAuthorized}>
        <SignUpForm/>
        <SignInForm/>
    </section>
);

export default NotAuthorized;
