import React          from "react";
import { connect }    from "react-redux";
import * as PropTypes from "prop-types";

import * as styles from "./styles.modules.css";

import NavBar      from "components/AuthorizedParts/NavBar";
import Inner       from "components/AuthorizedParts/Inner";
import RpgSessions from "components/LandingPage/RpgSessions";

import * as actions from "actions";

class Landing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { token, requestCurrentUser } = this.props;
        requestCurrentUser(token);
    }

    static getDerivedStateFromProps(nextProps, state) {
        const { currentUser, openNicknameModal } = nextProps;

        if (currentUser && (currentUser.nickname === "" || currentUser.nickname === "User")) {
            openNicknameModal();
        }

        return state;
    }

    render() {
        const { page, limit } = this.props;
        return (
            <section className={styles.authorized}>
                <NavBar/>
                <Inner>
                    <RpgSessions
                        page={page}
                        limit={limit}
                    />
                </Inner>
            </section>
        );
    }

    static get propTypes() {
        return {
            token:              PropTypes.string,
            requestCurrentUser: PropTypes.func,
            currentUser:        PropTypes.shape({
                nickname: PropTypes.string
            }),
            openNicknameModal:  PropTypes.func,
            page:               PropTypes.number,
            limit:              PropTypes.number
        };
    }
}

const mapStateToProps = ({ tokens: { accessToken }, users: { current }, pages: { landingPage } }) => ({
    token:       accessToken,
    currentUser: current,
    page:        landingPage.page,
    limit:       landingPage.limit
});

const mapDispatchToProps = (dispatch) => ({
    requestCurrentUser: token => dispatch(actions.requestCurrentUser(token)),
    openNicknameModal:  () => dispatch(actions.openNicknameModal({}))
});

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
