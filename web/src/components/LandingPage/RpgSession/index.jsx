import React              from "react";
import { connect }        from "react-redux";
import { Link }           from "react-router-dom";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import * as styles from "./styles.modules.css";

const RpgSession = ({ rpgSession, author, stopPropagation }) => (
    <li
        className={styles.item}
        onClick={stopPropagation}
    >
        <Link
            to={`/rpg-session/${rpgSession.id}`}
        >
            <span className={styles.name}>
                {rpgSession.name}
            </span>
            <span className={styles.author}>
                ({author ? author.nickname : ""})
            </span>
        </Link>
    </li>
);

RpgSession.propTypes = {
    author:          PropTypes.shape({
        nickname: PropTypes.string.isRequired,
    }),
    rpgSession:      PropTypes.shape({
        name: PropTypes.string.isRequired,
    }),
    stopPropagation: PropTypes.func,
};

const mapStateToProps = ({ users }, { rpgSession }) => ({
    author: users.byId[rpgSession.userId],
});
const mapDispatchToProps = () => ({
    stopPropagation: event => {
        event.preventDefault();
        event.stopPropagation();
    }
});

export default withNamespaces("rpgSession")(connect(mapStateToProps, mapDispatchToProps)(RpgSession));
