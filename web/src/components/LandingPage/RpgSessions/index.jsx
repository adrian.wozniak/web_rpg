import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import * as styles from "./styles.modules.css";

import RpgSession from "components/LandingPage/RpgSession";

import { getAllRpgSessions, getByUserId, getRpgSessionsAuthorsIds } from "reducers/rpg_sessions";
import * as actions                                                 from "actions";

class RpgSessions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            rpgSessions:       props.rpgSessions,
            byUserId:          props.byUserId,
            requestUsers:      props.requestUsers,
            page:              props.page,
            limit:             props.limit,
        };
    }

    static getDerivedStateFromProps(nextProps, state) {
        const { authorIds, page, limit, byUserId, rpgSessions } = nextProps;
        if (nextProps.byUserId !== state.byUserId || nextProps.page !== state.page) {
            const { requestUsers } = state;
            requestUsers({
                authorIds,
                page:  0,
                limit: 100,
            });
        }
        return {
            ...state,
            byUserId,
            page,
            limit,
            rpgSessions,
        };
    }

    componentDidMount() {
        const { requestRpgSessions, page, limit } = this.props;
        requestRpgSessions({ page, limit });
    }

    render() {
        const { rpgSessions } = this.props;
        return (
            <ul className={styles.list}>
                {
                    rpgSessions.map(rpgSession => (
                        <RpgSession key={`rpg-session-${rpgSession.id}`} rpgSession={rpgSession}/>
                    ))
                }
            </ul>
        );
    }

    static get propTypes() {
        return {
            requestRpgSessions: PropTypes.func,
            requestUsers:       PropTypes.func,
            rpgSessions:        PropTypes.array,
            authorIds:          PropTypes.arrayOf(PropTypes.number),
            byUserId:           PropTypes.object,
            page:               PropTypes.number.isRequired,
            limit:              PropTypes.number.isRequired,
        };
    }
}

const mapStateToProps = (state) => ({
    rpgSessions: getAllRpgSessions(state),
    authorIds:   getRpgSessionsAuthorsIds(state),
    byUserId:    getByUserId(state),
});
const mapDispatchToProps = (dispatch) => ({
    requestRpgSessions:
        ({ limit, page }) =>
            dispatch(actions.requestRpgSessions({ limit, page })),
    requestUsers:
        ({ page, limit, authorIds }) =>
            dispatch(actions.requestUsers({ page, limit, authorIds })),
});

export default withNamespaces("rpgSession")(connect(mapStateToProps, mapDispatchToProps)(RpgSessions));
