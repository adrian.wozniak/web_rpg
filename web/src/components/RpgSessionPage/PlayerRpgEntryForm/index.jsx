import React from "react";
import { connect } from "react-redux";
import * as PropTypes from "prop-types";
import classNames from "classnames";
import MarkdownEditor from "react-markdown-editor";

import store from "../../../store";
import * as styles from "./styles.modules.css";
import * as actions from "actions";

const PlayerRpgEntryForm = ({ onInputChange, t, onSubmit }) => (
    <form
        className={classNames(
            styles.rpgEntryForm,
            styles.form
        )}
        onSubmit={onSubmit}
    >
        <MarkdownEditor
            className={classNames(
                styles.input
            )}
            initialContent=""
            iconsSet="font-awesome"
            onContentChange={onInputChange}
            name="rpgEntryEditor"
            store={store}
        />
        <input type="submit" value={t("submit")} />
    </form>
);

PlayerRpgEntryForm.propTypes = {
    isNarrator:    PropTypes.bool,
    rpgSessionId:  PropTypes.number,
    rpgSession:    PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string
    }),
    rpgCharacters: PropTypes.arrayOf(PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string
    })),
    rpgEntryForm: PropTypes.shape({
        rpgCharacterId: PropTypes.number,
        userId:         PropTypes.number,
        rpgSessionId:   PropTypes.number,
        input:          PropTypes.string,
    }),
    onSave:        PropTypes.func,
    onSubmit:      PropTypes.func,
    onInputChange: PropTypes.func,
    t:             PropTypes.func,
};

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch, { rpgEntryForm, onSave }) => ({
    onSubmit:      () => onSave(rpgEntryForm),
    onInputChange:
    input => dispatch(actions.rpgEntryFormInputChanged({
        input
    })),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlayerRpgEntryForm);
