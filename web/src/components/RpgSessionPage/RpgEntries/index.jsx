import React          from "react";
import { connect }    from "react-redux";
import * as PropTypes from "prop-types";

import RpgEntry     from "components/RpgSessionPage/RpgEntry";
import RpgEntryForm from "components/RpgSessionPage/RpgEntryForm";

import { getByRpgSessionId } from "reducers/rpg_entries";
import * as styles           from "./styles.modules.css";

const RpgEntries = ({ rpgEntries, rpgSessionId }) => (
    <ul className={styles.list}>
        {
            rpgEntries.map(rpgEntry => (
                <RpgEntry
                    key={`RpgEntry-${rpgEntry.id}`}
                    rpgEntry={rpgEntry}
                    rpgSessionId={rpgSessionId}
                />
            ))
        }
        <li className={styles.item}>
            <RpgEntryForm
                rpgSessionId={rpgSessionId}
            />
        </li>
    </ul>
);

RpgEntries.propTypes = {
    rpgEntries:   PropTypes.arrayOf(PropTypes.shape({
        input:          PropTypes.string,
        id:             PropTypes.number,
        rpgSessionId:   PropTypes.number,
        rpgCharacterId: PropTypes.number,
    })),
    rpgSessionId: PropTypes.number,
};

const mapStateToProps = (state, { rpgSessionId }) => ({
    rpgEntries: getByRpgSessionId({ rpgSessionId, state }),
});

export default connect(mapStateToProps)(RpgEntries);
