import React          from "react";
import { connect }    from "react-redux";
import * as PropTypes from "prop-types";

import * as styles from "./styles.modules.css";

const Popup = ({ rpgCharacter }) => (
    <ul className={styles.popup}>
        {
            rpgCharacter.statistics.map(stat => (
                <li
                    key={stat.name}
                    className={styles.stat}
                >
                    <span className={styles.statName}>
                        {stat.name}
                    </span>
                    <span className={styles.statValue}>
                        {stat.value}
                    </span>
                </li>
            ))
        }
    </ul>
);

Popup.propTypes = {
    rpgCharacter: PropTypes.shape({
        statistics: PropTypes.arrayOf(PropTypes.shape({
            name:  PropTypes.string,
            value: PropTypes.number,
        })),
    }),
};

class RpgCharacterPopup extends React.Component {
    state = { isPopupActive: false };
    onClick = () => this.setState({ isPopupActive: !this.state.isPopupActive });

    render() {
        const { rpgCharacter } = this.props;
        const { isPopupActive } = this.state;
        if (!rpgCharacter)
            return null;

        return (
            <div
                className={styles.rpgCharacterPopup}
                onClick={this.onClick}
            >
                <div className={styles.name}>
                    <span>
                        {rpgCharacter.name}
                    </span>
                </div>
                {
                    isPopupActive
                        ? (
                            <Popup
                                rpgCharacter={rpgCharacter}
                            />)
                        : null
                }
            </div>
        );
    }
}

RpgCharacterPopup.propTypes = {
    rpgCharacter: PropTypes.shape({
        statistics: PropTypes.arrayOf(PropTypes.shape({
            name:  PropTypes.string,
            value: PropTypes.number,
        })),
    }),
    isPopupActive: PropTypes.bool,
};

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(RpgCharacterPopup);
