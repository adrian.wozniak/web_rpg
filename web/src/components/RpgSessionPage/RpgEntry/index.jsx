import React          from "react";
import { connect }    from "react-redux";
import Markdown       from "react-markdown";
import * as PropTypes from "prop-types";
import classNames     from "classnames";

import * as styles from "./styles.modules.css";

import RpgCharacterPopup from "components/RpgSessionPage/RpgCharacterPopup";

const RpgEntry = ({ rpgEntry, rpgCharacter, rpgSession }) => (
    <li
        className={classNames(
            styles.rpgEntry,
            rpgSession.userId === rpgEntry.userId ? styles.owner : null,
        )}
    >
        <RpgCharacterPopup
            rpgCharacter={rpgCharacter}
            isPopupActive={true}
        />
        <div className={styles.entryWrapper}>
            {
                rpgCharacter && rpgCharacter.imageUrl && rpgCharacter.imageUrl.length
                    ? (
                        <img
                            src={rpgCharacter.imageUrl}
                            alt={rpgCharacter.name}
                            className={styles.image}
                        />
                    )
                    : null
            }
            <Markdown
                source={rpgEntry.input}
            />
        </div>
    </li>
);

RpgEntry.propTypes = {
    rpgSessionId: PropTypes.number,
    rpgEntry:     PropTypes.shape({
        input:          PropTypes.string,
        id:             PropTypes.number,
        rpgSessionId:   PropTypes.number,
        rpgCharacterId: PropTypes.number,
    }),
    rpgCharacter: PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string,
        imageUrl:     PropTypes.string,
    }),
    rpgSession:   PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string,
    }),
};

const mapStateToProps = (
    { rpgCharacters, rpgSessions },
    { rpgEntry }
) => ({
    rpgCharacter: rpgCharacters.byId[rpgEntry.rpgCharacterId],
    rpgSession:   rpgSessions.byId[rpgEntry.rpgSessionId],
});

export default connect(mapStateToProps)(RpgEntry);
