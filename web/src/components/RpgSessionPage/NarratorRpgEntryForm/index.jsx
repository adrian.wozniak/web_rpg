import React from "react";
import { connect } from "react-redux";
import * as PropTypes from "prop-types";
import classNames from "classnames";
import MarkdownEditor from "react-markdown-editor";

import store from "../../../store";
import * as styles from "./styles.modules.css";

import NarratorCharacterSelect from "components/RpgSessionPage/NarratorCharacterSelect";

import * as actions from "actions";

const NarratorRpgEntryForm = ({ t, onSubmit, onInputChange, onRpgCharacterIdChange }) => (
    <form onSubmit={onSubmit} >
        <div
            className={classNames(
                styles.rpgEntryForm,
                styles.form,
                styles.narratorForm
            )}
        >
            <MarkdownEditor
                className={classNames(
                    styles.input,
                    styles.narratorInput
                )}
                initialContent=""
                iconsSet="font-awesome"
                onContentChange={onInputChange}
                name="rpgEntryEditor"
                store={store}
            />
            <div className={styles.characterSelect}>
                <NarratorCharacterSelect
                    onChange={onRpgCharacterIdChange}
                />
            </div>
        </div>
        <div className={classNames(styles.submitRow)}>
            <input
                type="submit"
                value={t("submit")}
                className={classNames(styles.submitButton)}
            />
        </div>
    </form>
);

NarratorRpgEntryForm.propTypes = {
    isNarrator:        PropTypes.bool,
    rpgSessionId:      PropTypes.number,
    rpgSession:        PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string
    }),
    rpgCharacters:     PropTypes.arrayOf(PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string
    })),
    rpgEntryForm: PropTypes.shape({
        rpgCharacterId: PropTypes.number,
        userId:         PropTypes.number,
        rpgSessionId:   PropTypes.number,
        input:          PropTypes.string,
    }),
    onSave:                 PropTypes.func,
    onSubmit:               PropTypes.func,
    onInputChange:          PropTypes.func,
    onRpgCharacterIdChange: PropTypes.func,
    t:                      PropTypes.func,
};

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch, { rpgEntryForm, onSave }) => ({
    onSubmit:               () => onSave(rpgEntryForm),
    onRpgCharacterIdChange: ({ value: rpgCharacter }) =>
        dispatch(actions.rpgEntryFormRpgCharacterIdChanged({
            rpgCharacterId: rpgCharacter.id,
        })),
    onInputChange:
    (input) => dispatch(actions.rpgEntryFormInputChanged({ input })),
});

export default connect(mapStateToProps, mapDispatchToProps)(NarratorRpgEntryForm);
