import React          from "react";
import { connect }    from "react-redux";
import * as PropTypes from "prop-types";

import NavBar     from "components/AuthorizedParts/NavBar";
import Inner      from "components/AuthorizedParts/Inner";
import RpgEntries from "components/RpgSessionPage/RpgEntries";

import * as styles from "./styles.modules.css";

import * as actions         from "actions";
import { getCurrentUserId } from "reducers/users";

class RpgSession extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            requestUsersRelatedRpgSessions: props.requestUsersRelatedRpgSessions,
        };
    }

    static getDerivedStateFromProps(nextProps, state) {
        const { userId } = nextProps;
        if (state.userId !== userId) {
            const { requestUsersRelatedRpgSessions } = state;
            requestUsersRelatedRpgSessions({ userIds: [userId] });
        }
        return {
            ...state,
            userId,
        };
    }

    componentDidMount() {
        const {
            token,
            rpgSessionId,
            requestCurrentUser,
            requestRpgSession,
            requestRpgEntries,
            requestRpgCharacters,
        } = this.props;
        requestCurrentUser(token);
        requestRpgSession({ id: rpgSessionId });
        requestRpgEntries({ rpgSessionId });
        requestRpgCharacters({ rpgSessionId });
    }

    render() {
        const { rpgSession, rpgSessionId } = this.props;
        return (
            <section className={styles.rpgSession}>
                <NavBar
                />
                <Inner>
                    <h3>
                        {rpgSession ? rpgSession.name : "Rpg Session"}
                    </h3>
                    <RpgEntries
                        rpgSessionId={rpgSessionId}
                    />
                </Inner>
            </section>
        );
    }
}

RpgSession.propTypes = {
    token:                          PropTypes.string,
    userId:                         PropTypes.number,
    rpgSessionId:                   PropTypes.number,
    requestRpgSession:              PropTypes.func,
    requestCurrentUser:             PropTypes.func,
    requestRpgEntries:              PropTypes.func,
    requestRpgCharacters:           PropTypes.func,
    requestUsersRelatedRpgSessions: PropTypes.func,
    rpgSession:                     PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string,
    }),
};

const mapStateToProps = (
    { tokens: { accessToken }, rpgSessions: { byId: rpgSessionById }, users },
    { match: { params: { id } } }
) => ({
    token:        accessToken,
    userId:       getCurrentUserId({ users }),
    rpgSessionId: parseInt(id),
    rpgSession:   rpgSessionById[parseInt(id)],
});

const mapDispatchToProps = (dispatch) => ({
    requestCurrentUser:
        (token) =>
            dispatch(actions.requestCurrentUser(token)),
    requestRpgSession:
        ({ id }) =>
            dispatch(actions.requestRpgSession({ id })),
    requestRpgEntries:
        ({ rpgSessionId }) =>
            dispatch(actions.requestRpgEntries({
                page:          0,
                limit:         100,
                rpgSessionIds: [rpgSessionId],
            })),
    requestRpgCharacters:
        ({ rpgSessionId }) =>
            dispatch(actions.requestRpgCharacters({
                page:          0,
                limit:         100,
                rpgSessionIds: [rpgSessionId],
            })),
    requestUsersRelatedRpgSessions:
        ({ userIds }) =>
            dispatch(actions.requestUsersRelatedRpgSessions({
                userIds,
                page:  0,
                limit: 100,
            }))
});

export default connect(mapStateToProps, mapDispatchToProps)(RpgSession);
