import React          from "react";
import { connect }    from "react-redux";
import Select         from "react-select";
import * as PropTypes from "prop-types";
import classNames     from "classnames";

import * as styles from "./styles.modules.css";

import { getUserRpgCharacters } from "reducers/rpg_characters";
import { getCurrentUserId }     from "reducers/users";

const NarratorCharacterOption = ({ innerRef, innerProps, value }) => (
    <div
        ref={innerRef}
        {...innerProps}
        className={styles.narratorCharacterOption}
    >
        <span className={styles.imageWrapper}>
            <img
                src={value.imageUrl}
                alt={value.name}
                className={styles.image}
            />
        </span>
        <span className={styles.name}>
            {value.name}
        </span>
    </div>
);

NarratorCharacterOption.propTypes = {
    rpgCharacter: PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string,
        imageUrl:     PropTypes.string,
    }),
    rpgSession:   PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string,
    }),
    onChange:     PropTypes.func,
};

const NarratorCharacterSelect = ({ rpgCharacters, onChange }) => (
    <Select
        className={classNames(styles.narratorCharacterSelect)}
        components={{ Option: NarratorCharacterOption }}
        options={rpgCharacters}
        isMulti={false}
        onChange={onChange}
    />
);

NarratorCharacterSelect.propTypes = {
    rpgCharacters: PropTypes.arrayOf(PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string,
        imageUrl:     PropTypes.string,
    })),
    onChange:      PropTypes.func,
};

const mapStateToProps = ({ rpgCharacters, users }) => ({
    rpgCharacters: getUserRpgCharacters({
        state:  { rpgCharacters },
        userId: getCurrentUserId({ users })
    }).map(c => ({ label: c.name, value: c })),
});
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NarratorCharacterSelect);
