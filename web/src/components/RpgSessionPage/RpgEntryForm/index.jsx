import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import NarratorRpgEntryForm from "components/RpgSessionPage/NarratorRpgEntryForm";
import PlayerRpgEntryForm from "components/RpgSessionPage/PlayerRpgEntryForm";

import * as actions             from "actions";
import { getUserRpgCharacters } from "reducers/rpg_characters";
import { getCurrentUserId }     from "reducers/users";

class RpgEntryForm extends React.Component {
    state = {};

    static getDerivedStateFromProps(props, state) {
        const {
            userId,
            rpgSessionId,
            rpgEntryFormUserIdChanged,
            rpgEntryFormRpgSessionIdChanged
        } = props;

        if (props.userId !== state.userId) {
            rpgEntryFormUserIdChanged({ userId });
            rpgEntryFormRpgSessionIdChanged({ rpgSessionId });
        }

        const newState = {
            ...state,
            userId,
            rpgSessionId,
            rpgEntryFormUserIdChanged,
            rpgEntryFormRpgSessionIdChanged
        };

        return newState;
    }

    render() {
        const {
            rpgEntryForm,
            isNarrator,
            rpgSessionId,
            rpgSession,
            rpgCharacters,
            saveEntry,
            rpgEntryFormRpgSessionIdChanged,
            t,
        } = this.props;
        return (
            isNarrator
                ? <NarratorRpgEntryForm
                    t={t}
                    rpgEntryForm={rpgEntryForm}
                    rpgSessionId={rpgSessionId}
                    rpgSession={rpgSession}
                    rpgCharacters={rpgCharacters}
                    onSave={saveEntry}
                />
                : <PlayerRpgEntryForm
                    t={t}
                    rpgEntryForm={rpgEntryForm}
                    rpgSessionId={rpgSessionId}
                    rpgSession={rpgSession}
                    rpgCharacters={rpgCharacters}
                    onSave={saveEntry}
                    onCharacterChange={rpgEntryFormRpgSessionIdChanged}
                />
        );
    }
}

RpgEntryForm.propTypes = {
    isNarrator:                        PropTypes.bool,
    rpgSessionId:                      PropTypes.number,
    userId:                            PropTypes.number,
    rpgSession:                        PropTypes.shape({
        id:        PropTypes.number,
        userId:    PropTypes.number,
        name:      PropTypes.string,
        createdAt: PropTypes.string,
        updatedAt: PropTypes.string
    }),
    rpgCharacters:                     PropTypes.arrayOf(PropTypes.shape({
        id:           PropTypes.number,
        rpgSessionId: PropTypes.number,
        userId:       PropTypes.number,
        name:         PropTypes.string
    })),
    saveEntry:                          PropTypes.func,
    rpgEntryFormRpgCharacterIdChanged: PropTypes.func,
    rpgEntryFormUserIdChanged:         PropTypes.func,
    rpgEntryFormRpgSessionIdChanged:   PropTypes.func,
    t:                                 PropTypes.func,
    rpgEntryForm:                      PropTypes.shape({
        rpgCharacterId: PropTypes.number,
        userId:         PropTypes.number,
        rpgSessionId:   PropTypes.number,
        input:          PropTypes.string,
    }),
};

const mapStateToProps = ({ rpgSessions, rpgCharacters, users, forms }, { rpgSessionId }) => ({
    rpgSession:    rpgSessions.byId[rpgSessionId],
    userId:        getCurrentUserId({ users }),
    rpgCharacters: getUserRpgCharacters({
        state:  { rpgCharacters },
        userId: getCurrentUserId({ users })
    }),
    isNarrator:    (rpgSessions.byId[rpgSessionId] || {}).userId === getCurrentUserId({ users }),
    rpgEntryForm: forms.rpgEntry,
});
const mapDispatchToProps = (dispatch, { rpgSessionId }) => ({
    rpgEntryFormRpgCharacterIdChanged: ({ rpgCharacterId }) =>
        dispatch(actions.rpgEntryFormRpgCharacterIdChanged({
            rpgCharacterId,
        })),
    rpgEntryFormUserIdChanged:
    ({ userId }) => dispatch(actions.rpgEntryFormUserIdChanged({
        userId
    })),
    rpgEntryFormRpgSessionIdChanged:
    ({ rpgSessionId }) => dispatch(actions.rpgEntryFormRpgSessionIdChanged({
        rpgSessionId
    })),
    saveEntry:
    ({ userId, rpgCharacterId, input }) => dispatch(actions.requestCreateRpgEntry({ 
        userId, 
        rpgCharacterId, 
        rpgSessionId, 
        input
    })),
});

export default withNamespaces("rpgEntryForm")(connect(mapStateToProps, mapDispatchToProps)(RpgEntryForm));
