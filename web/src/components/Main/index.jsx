import React                    from "react";
import { Provider }             from "react-redux";
import { Route, BrowserRouter } from "react-router-dom";

import store from "../../store";

import Landing        from "components/LandingPage/Landing";
import Authorize      from "components/AuthorizePage/Authorize";
import RpgSession     from "components/RpgSessionPage/RpgSession";
import NotAuthorized  from "components/AuthPage/NotAuthorized";
import ProtectedRoute from "components/Main/ProtectedRoute";
import NicknameModal  from "components/Modals/NicknameModal";

const Modals = () => (
    <section>
        <NicknameModal/>
    </section>
);

const Main = () => (
    <Provider store={store}>
        <BrowserRouter>
            <article>
                <Modals/>
                <ProtectedRoute
                    path={"/"}
                    component={Landing}
                    exact={true}
                />
                <ProtectedRoute
                    path={"/rpg-session/:id"}
                    component={RpgSession}
                    exact={true}
                />
                <Route
                    path={"/authorize"}
                    component={Authorize}
                    exact={true}
                />
                <Route
                    path={"/auth"}
                    component={NotAuthorized}
                    exact={true}
                />
            </article>
        </BrowserRouter>
    </Provider>
);

export default Main;
