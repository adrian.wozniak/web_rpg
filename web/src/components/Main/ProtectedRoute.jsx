import React               from "react";
import { Route, Redirect } from "react-router-dom";
import * as PropTypes      from "prop-types";

import { getStoredToken } from "reducers/tokens";

const ProtectedRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            getStoredToken() ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/auth",
                        state:    { from: props.location }
                    }}
                />
            )
        }
    />
);

ProtectedRoute.propTypes = {
    component: PropTypes.func,
    location:  PropTypes.object,
};

export default ProtectedRoute;
