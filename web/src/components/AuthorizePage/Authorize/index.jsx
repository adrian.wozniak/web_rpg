import React              from "react";
import * as PropTypes     from "prop-types";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import { decode }         from "query-params";
import { Redirect }       from "react-router-dom";

import loaderUrl from "images/image-loader.gif";

import * as styles from "./styles.modules.css";

import * as actions from "actions";

class Authorize extends React.Component {
    componentDidMount() {
        const { checkToken, params: { token } } = this.props;
        checkToken(token);
    }

    render() {
        const { t, tokens } = this.props;
        if (tokens.accessToken)
            return <Redirect to={"/"}/>;

        return (
            <section
                className={styles.loading}
            >
                <img
                    src={loaderUrl}
                    alt={t("loading")}
                />
            </section>
        );
    }

    static get propTypes() {
        return {
            checkToken: PropTypes.func,
            t:          PropTypes.func,
            params:     PropTypes.shape({
                token: PropTypes.string
            }),
            tokens:     PropTypes.shape({
                accessToken: PropTypes.string
            })
        };
    }
}

const mapStateToProps = (state) => ({
    tokens: state.tokens,
    params: decode(document.location.search.replace(/^\?/, ""))
});
const mapDispatchToProps = dispatch => ({
    "checkToken": token => dispatch(actions.checkToken(token))
});

export default withNamespaces("authorize")(connect(mapStateToProps, mapDispatchToProps)(Authorize));
