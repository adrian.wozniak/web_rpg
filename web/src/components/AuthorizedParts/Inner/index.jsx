import React          from "react";
import * as PropTypes from "prop-types";

import * as styles from "./styles.modules.css";

const Inner = ({ children }) => (
    <div className={styles.inner}>{children}</div>
);

Inner.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
    ]),
};

export default Inner;
