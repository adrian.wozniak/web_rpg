import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import * as styles from "./styles.modules.css";

import { NavLink } from "react-router-dom";

const NavBar = ({ currentUser, isHomeActive, t }) =>
    currentUser
        ? (
            <menu className={styles.navBar}>
                <ul className={styles.list}>
                    <li className={styles.item}>
                        <NavLink
                            to={"/"}
                            isActive={isHomeActive}
                            activeClassName={styles.active}
                        >
                            <h5 className={styles.textWrapper}>
                                {t("home")}
                            </h5>
                        </NavLink>
                    </li>
                    <li className={styles.item}>
                        <h5 className={styles.textWrapper}>
                            {currentUser.nickname}
                        </h5>
                    </li>
                </ul>
            </menu>
        )
        : null;

NavBar.propTypes = {
    currentUser:  PropTypes.shape({
        nickname: PropTypes.string
    }),
    isHomeActive: PropTypes.func,
    t:            PropTypes.func,
};

const mapStateToProps = (state) => ({
    currentUser: state.users.current,
});
const mapDispatchToProps = () => ({
    isHomeActive: () => location.pathname === "/" || location.pathname === "",
});

export default withNamespaces("navbar")(connect(mapStateToProps, mapDispatchToProps)(NavBar));
