import React              from "react";
import { connect }        from "react-redux";
import { withNamespaces } from "react-i18next";
import * as PropTypes     from "prop-types";

import * as styles from "./styles.modules.css";

import { getCurrentUserNickname }                      from "reducers/users";
import { getNicknameModalErrors, isNicknameModalOpen } from "reducers/modals";
import { nicknameModalInput, nicknameModalSubmit }     from "actions";

const NicknameModal = ({ t, onNicknameChange, onSubmit, nickname, isOpen, errors }) =>
    isOpen
        ? (
            <nickname-modal class={styles.modal}>
                <h3 className={styles.header}>
                    {t("pickNickname")}
                </h3>
                <form
                    className={styles.form}
                    onSubmit={onSubmit}
                >
                    <input
                        type={"text"}
                        onChange={onNicknameChange}
                        defaultValue={nickname}
                        className={styles.input}
                    />
                    <input
                        type={"submit"}
                        value={t("submit")}
                        className={styles.submit}
                    />
                </form>
                {
                    errors.map((error) => (
                        <p key={error} className={styles.error}>
                            {t(error)}
                        </p>
                    ))
                }
            </nickname-modal>
        )
        : null;

NicknameModal.propTypes = {
    t:                PropTypes.func,
    onNicknameChange: PropTypes.func,
    onSubmit:         PropTypes.func,
    nickname:         PropTypes.string,
    isOpen:           PropTypes.bool,
    errors:           PropTypes.arrayOf(PropTypes.string),
};

const mapStateToProps = (state) => ({
    nickname: getCurrentUserNickname(state),
    isOpen:   isNicknameModalOpen(state),
    errors:   getNicknameModalErrors(state),
});

const mapDispatchToProps = (dispatch) => ({
    "onNicknameChange": (event) => dispatch(nicknameModalInput(event.target.value)),
    "onSubmit":         (event) => (event.preventDefault(), dispatch(nicknameModalSubmit())),
});

export default withNamespaces("nicknameModal")(connect(mapStateToProps, mapDispatchToProps)(NicknameModal));

if (window.customElements) {
    class NicknameModalElement extends HTMLElement {
    }

    window.customElements.define("nickname-modal", NicknameModalElement);
}
