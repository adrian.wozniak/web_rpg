import * as types from "./types";

const sortInt = (a, b) => a > b ? 1 : a < b ? -1 : 0;

test("all incoming messages must own values", () => {
    const values = Object.values(types).sort(sortInt);
    const unique = values.filter((v, index, self) => self.indexOf(v) === index).sort(sortInt);
    expect(values).toEqual(unique);
});
