const toString = ({}).toString;
const isString = (obj) => Reflect.apply(toString, obj, []) === "[object String]";
const isNumber = (obj) => Reflect.apply(toString, obj, []) === "[object Number]";
const isFunction = (obj) => Reflect.apply(toString, obj, []) === "[object Function]";
// const isObject = obj => Reflect.apply(toString, obj, []) === "[object Object]";
// const isArray = obj => Array.isArray(obj);

export const REPLACE = Symbol("REPLACE");

const resultInjector = (target, name, value) => {
    if (isString(name)) {
        target = { ...target, [name]: value };
    } else if (isNumber(name)) {
        target = [...target];
        target[name] = value;
    }
    return target;
};

const defaultByteEraser = ({ array, size, bytesEraser, initialLength, target }) => {
    if (isFunction(bytesEraser)) {
        // console.log("erase bytes with eraser %o %o", array.length, array);
        bytesEraser(array, target);
        // console.log("after erased bytes with eraser %o %o", array.length, array);
    } else if (array.length === initialLength) {
        // console.log("simple erase %o %o", size, array);
        array.splice(0, size);
    }
};

export const build = (typedArray, { skipOp = false, target = {} } = {}) => {
    // console.groupCollapsed("build target");
    // console.log("  build decoded object with opts: %o %o", typedArray, { skipOp, target });
    const array = Array.isArray(typedArray)
        ? typedArray
        : Array.from(typedArray);

    const main = ({
        "then":    (drainer) => {
            // console.log(" --- drain progress %o remaining %o %o", target, array.length, array);
            target = drainer(target, array);
            return main;
        },
        "collect": () => {
            // console.groupEnd();
            return target;
        },
    });

    return skipOp ? main : main.then(drainOp);
};

export const buildArray = (typedArray, opts = {}) =>
    build(typedArray, { ...opts, target: [] });

export const drain = ({ name, size, sliceProcessor, bytesEraser, inject = resultInjector, intercept }) => (target, array) => {
    // console.groupCollapsed(`draining ${name.toString()}`);
    if (isFunction(size))
        size = size(array);

    const initialLength = array.length;
    const slice = array.slice(0, size);
    let value = isFunction(sliceProcessor) ? sliceProcessor(slice, target, array) : slice;
    if (isFunction(intercept)) value = intercept(value);
    target = inject(target, name, value);
    defaultByteEraser({ target, initialLength, array, size, bytesEraser });
    // console.groupEnd();
    return target;
};

export const drainString = (name, opts = {}) =>
    drain({
        ...opts,
        name,
        size:           a => a[0] + 1,
        sliceProcessor: a => a.slice(1, a.length)
            .map(n => String.fromCharCode(n))
            .join("")
    });

export const drainNumber = (name, opts = {}) =>
    drain({
        ...opts,
        name,
        size:           1,
        sliceProcessor: a => a[0],
    });

export const drainUuid = (name, opts = {}) =>
    drain({
        ...opts,
        name,
        size:           4,
        sliceProcessor: a => intoUuid(
            Array
                .from(new Uint8Array(Int32Array.from(a).buffer))
                .map(b => Number(b).toString(16).padStart(2, "0"))
        )
    });

export const drainArrayWith = (name, drainer, { intercept } = {}) =>
    drain({
        name,
        size:           a => a.length,
        sliceProcessor: (_slice, _target, slice) => {
            // console.groupCollapsed(name.toString());
            const length = slice.splice(0, 1)[0];
            // console.log("drain array %o length %o", name, length);
            let target = [];
            for (let index = 0; index < length; index++) {
                // console.groupCollapsed(`drain index ${index}`);
                target = drainer(index, { intercept })(target, slice);
                // console.log("  ---- drained entity %o remaining bytes %o %o", target, slice, slice.length);
                // console.groupEnd();
            }
            // console.groupEnd();
            return target;
        }
    });

export const drainArrayOf = (typeDecoder, name = Symbol("Array")) =>
    drain({
        name,
        size:           a => a.length,
        sliceProcessor: slice => {
            // console.groupCollapsed("drain array of objects");
            const length = slice.splice(0, 1)[0];
            // console.log("drain array length %o", length);
            return new Array(length).fill({}).map(() => {
                return typeDecoder(slice, { skipOp: true });
                // console.log("drain entity %o", res);
                // return res;
            });
            // console.groupEnd();
            // return result;
        },
        inject:         (target, _, value) => [...target, ...value],
    });

const drainOp = drain({ name: Symbol("op"), size: 1 });

const intoUuid = (a) =>
    [
        a.slice(0, 4).join(""),
        a.slice(4, 6).join(""),
        a.slice(6, 8).join(""),
        a.slice(8, 10).join(""),
        a.slice(10, 16).join(""),
    ].join("-");
