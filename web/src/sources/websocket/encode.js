import * as types from "sources/websocket/types";

export const ping = () =>
    Int32Array.from([types.PING]);

export const signIn = email =>
    Int32Array.from([
        types.SIGN_IN,
        email.normalize("NFD").length,
        ...email.normalize("NFD").split("").map(c => c.charCodeAt(0))
    ]);

export const signUp = email =>
    Int32Array.from([
        types.SIGN_UP,
        email.normalize("NFD").length,
        ...email.normalize("NFD").split("").map(c => c.charCodeAt(0))
    ]);

export const authorize = (token) => Int32Array.from([
    types.AUTHORIZE,
    token.length,
    ...token.split("").map(c => c.charCodeAt(0))
]);

export const currentUser = (token) => Int32Array.from([
    types.CURRENT_USER,
    token.length,
    ...token.split("").map(c => c.charCodeAt(0))
]);

export const changeNickname = nickname => Int32Array.from([
    types.CHANGE_NICKNAME,
    nickname.length,
    ...nickname.split("").map(c => c.charCodeAt(0))
]);

export const rpgSession = ({ id }) => Int32Array.from([
    types.SINGLE_RPG_SESSION,
    id
]);

export const rpgSessionsList = ({ page, limit }) => Int32Array.from([
    types.RPG_SESSIONS_LIST,
    page,
    limit
]);

export const usersList = ({ page, limit, authorIds }) => Int32Array.from([
    types.USERS_LIST,
    page,
    limit,
    authorIds.length,
    ...authorIds
]);

export const rpgCharactersList = ({ page, limit, rpgSessionIds }) => Int32Array.from([
    types.RPG_CHARACTERS_LIST,
    page,
    limit,
    rpgSessionIds.length,
    ...rpgSessionIds
]);

export const usersRelatedRpgCharactersList = ({ page, limit, userIds }) => Int32Array.from([
    types.USERS_RELATED_RPG_CHARACTERS_LIST,
    page,
    limit,
    userIds.length,
    ...userIds
]);

export const rpgEntriesList = ({ page, limit, rpgSessionIds }) => Int32Array.from([
    types.RPG_ENTRIES_LIST,
    page,
    limit,
    rpgSessionIds.length,
    ...rpgSessionIds
]);

export const sendRpgSessionsRpgCharactersList = ({ page, limit, rpgSessionIds }) => Int32Array.from([
    types.RPG_SESSIONS_RPG_CHARACTERS_LIST,
    page,
    limit,
    rpgSessionIds.length,
    ...rpgSessionIds
]);

export const sendCreateRpgEntry = ({ userId, rpgCharacterId, rpgSessionId, input }) => Int32Array.from([
    types.CREATE_RPG_SESSION_ENTRY,
    userId,
    rpgCharacterId,
    rpgSessionId,
    input.length,
    ...input.split("").map(c => c.charCodeAt(0))
]);
