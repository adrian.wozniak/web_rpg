import ws          from "./index";
import * as encode from "sources/websocket/encode";

export const sendSignUp = ({ email }) =>
    ws.send(encode.signUp(email));

export const sendSignIn = ({ email }) =>
    ws.send(encode.signIn(email));

export const sendAuthorize = ({ token }) =>
    ws.send(encode.authorize(token));

export const sendCurrentUser = ({ token }) =>
    ws.send(encode.currentUser(token));

export const sendChangeNickname = ({ nickname }) =>
    ws.send(encode.changeNickname(nickname));

export const sendUsersList = ({ page, limit, authorIds }) =>
    ws.send(encode.usersList({ page, limit, authorIds }));

export const sendSingleRpgSession = ({ id }) =>
    ws.send(encode.rpgSession({ id }));

export const sendRpgSessionsList = ({ page, limit }) =>
    ws.send(encode.rpgSessionsList({ page, limit }));

export const sendRpgCharactersList = ({ page, limit, rpgSessionIds }) =>
    ws.send(encode.rpgCharactersList({ page, limit, rpgSessionIds }));

export const sendUsersRelatedRpgCharactersList = ({ page, limit, userIds }) =>
    ws.send(encode.usersRelatedRpgCharactersList({ page, limit, userIds }));

export const sendRpgEntriesList = ({ page, limit, rpgSessionIds }) =>
    ws.send(encode.rpgEntriesList({ page, limit, rpgSessionIds }));

export const sendRpgSessionsRpgCharactersList = ({ page, limit, rpgSessionIds }) =>
    ws.send(encode.sendRpgSessionsRpgCharactersList({ page, limit, rpgSessionIds }));

export const sendCreateRpgEntry = ({ userId, rpgCharacterId, rpgSessionId, input }) =>
    ws.send(encode.sendCreateRpgEntry({ userId, rpgCharacterId, rpgSessionId, input }));
