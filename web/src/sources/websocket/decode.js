import {
    build,
    buildArray,
    drainArrayOf,
    drainArrayWith,
    drainNumber,
    drainString,
    drainUuid
} from "sources/websocket/drainer";

export const decodeUser = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainString("email"))
        .then(drainString("nickname"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeArrayOfUser = (array, opts = {}) =>
    buildArray(array, { ...opts })
        .then(drainArrayOf(decodeUser))
        .collect();

export const decodeToken = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainNumber("userId"))
        .then(drainUuid("token"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeRpgSession = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainNumber("userId"))
        .then(drainString("name"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeArrayOfRpgSession = (array, opts = {}) =>
    buildArray(array, { ...opts })
        .then(drainArrayOf(decodeRpgSession))
        .collect();

export const decodeRpgEntry = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainNumber("userId"))
        .then(drainNumber("rpgCharacterId"))
        .then(drainNumber("rpgSessionId"))
        .then(drainString("input"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeArrayOfRpgEntry = (array, opts = {}) =>
    buildArray(array, { ...opts })
        .then(drainArrayOf(decodeRpgEntry))
        .collect();

export const decodeRpgCharacter = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainNumber("userId"))
        .then(drainString("name"))
        .then(drainString("imageUrl"))
        .then(drainArrayWith("statistics", drainString, {
            intercept: stat => ({
                name:  stat.split(": ")[0],
                value: parseFloat(stat.split(": ")[1])
            })
        }))
        .then(drainString("statType"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeArrayOfRpgCharacter = (array, opts = {}) =>
    buildArray(array, { ...opts })
        .then(drainArrayOf(decodeRpgCharacter))
        .collect();

export const decodeRpgSessionsRpgCharacter = (array, opts = {}) =>
    build(array, { ...opts })
        .then(drainNumber("id"))
        .then(drainNumber("rpgCharacterId"))
        .then(drainNumber("rpgSessionId"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

export const decodeArrayOfRpgSessionsRpgCharacter = (array, opts = {}) =>
    buildArray(array, { ...opts })
        .then(drainArrayOf(decodeRpgSessionsRpgCharacter))
        .collect();
