import * as decode  from "sources/websocket/decode";
import * as arrays  from "spec/arrays";
import * as objects from "spec/objects";

test("decode user should be possible", () => {
    const user = objects.getUser();
    const normalized = arrays.normalizeFields(user);
    const array = arrays.getCurrentUserArray(normalized);
    const result = decode.decodeUser(Int32Array.from(array));

    expect(result).toEqual(user);
});

test("decode token should be possible", () => {
    const token = objects.getToken();
    const normalized = arrays.normalizeFields(token);
    const tokenArray = arrays.getTokenArray(normalized);
    const result = decode.decodeToken(Int32Array.from(tokenArray));

    expect(result).toEqual(token);
});

test("decode 3 rpg sessions should return an array", () => {
    const rpgSessions = objects.getRpgSessions(3);
    const normalized = rpgSessions.map(o => arrays.normalizeFields(o));
    const array = arrays.getRpgSessionsList(...normalized);
    const result = decode.decodeArrayOfRpgSession(Int32Array.from(array));

    expect(result).toBeInstanceOf(Array);
});

test("decode 3 rpg sessions should be possible", () => {
    const rpgSessions = objects.getRpgSessions(3);
    const normalized = rpgSessions.map(o => arrays.normalizeFields(o));
    const array = arrays.getRpgSessionsList(...normalized);
    const result = decode.decodeArrayOfRpgSession(Int32Array.from(array));

    expect(result).toEqual(rpgSessions);
});

test("decode 2 users should return an array", () => {
    const users = objects.getUsers(2);
    const normalized = users.map(u => arrays.normalizeFields(u));
    const array = arrays.getUsersArray(...normalized);
    const result = decode.decodeArrayOfUser(Int32Array.from(array));

    expect(result).toBeInstanceOf(Array);
});

test("decode 2 users should be possible", () => {
    const users = objects.getUsers(2);
    const normalized = users.map(u => arrays.normalizeFields(u));
    const array = arrays.getUsersArray(...normalized);
    const result = decode.decodeArrayOfUser(Int32Array.from(array));

    expect(result).toEqual(users);
});

test("decode 3 rpg entries should be possible", () => {
    const expected = objects.getRpgEntries(1);

    const array = arrays.getRpgEntriesList(
        ...expected.map(entry => ({
            id:             entry.id,
            userId:         entry.userId,
            rpgCharacterId: entry.rpgCharacterId,
            rpgSessionId:   entry.rpgSessionId,
            input:          arrays.asArray(entry.input),
            createdAt:      arrays.asArray(entry.createdAt),
            updatedAt:      arrays.asArray(entry.updatedAt),
        }))
    );
    const decoded = decode.decodeArrayOfRpgEntry(Int32Array.from(array));

    expect(decoded).toEqual(expected);
});

test("decode 3 rpg characters should be possible", () => {
    const characters = objects.getRpgCharacters(1);
    const normalized = characters.map(o => arrays.normalizeFields(o));
    const array = arrays.getRpgCharactersList(...normalized);
    const result = decode.decodeArrayOfRpgCharacter(Int32Array.from(array));
    const expected = characters.map(character => ({
        ...character,
        statistics: character.statistics.map(stat => ({
            name:  stat.replace(/:.+$/, ""),
            value: parseInt(stat.replace(/^.+:/, "")),
        })),
    }));
    expect(result).toEqual(expected);
});

test("decode 3 rpg sessions rpg characters should be possible", () => {
    const rpgSessionsRpgCharacters = objects.getRpgSessionsRpgCharacters(3);
    const normalized = rpgSessionsRpgCharacters.map(o => arrays.normalizeFields(o));
    const array = arrays.getRpgSessionsRpgCharactersList(...normalized);
    const result = decode.decodeArrayOfRpgSessionsRpgCharacter(array);
    expect(result).toEqual(rpgSessionsRpgCharacters);
});
