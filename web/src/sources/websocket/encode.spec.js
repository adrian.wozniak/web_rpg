import faker from "faker";

import * as encode from "sources/websocket/encode";
import * as types  from "sources/websocket/types";
import * as arrays from "spec/arrays";

describe("ping", () => {
    test("is must return Int32Array", () => {
        expect(encode.ping()).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.ping());
        const expected = [types.PING];
        expect(res).toEqual(expected);
    });
});

describe("signIn", () => {
    const email = faker.internet.email();
    const encodedEmail = arrays.asArray(email.normalize("NFD"));

    test("is must return Int32Array", () => {
        expect(encode.signIn(email)).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.signIn(email));
        const expected = [types.SIGN_IN, ...encodedEmail];
        expect(res).toEqual(expected);
    });
});

describe("signUp", () => {
    const email = faker.internet.email();
    const encodedEmail = arrays.asArray(email.normalize("NFD"));

    test("is must return Int32Array", () => {
        expect(encode.signUp(email)).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.signUp(email));
        const expected = [types.SIGN_UP, ...encodedEmail];
        expect(res).toEqual(expected);
    });
});

describe("authorize", () => {
    const token = faker.random.uuid();
    const encodedToken = arrays.asArray(token);

    test("is must return Int32Array", () => {
        expect(encode.authorize(token)).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.authorize(token));
        const expected = [types.AUTHORIZE, ...encodedToken];
        expect(res).toEqual(expected);
    });
});

describe("currentUser", () => {
    const token = faker.random.uuid();
    const encodedToken = arrays.asArray(token);

    test("is must return Int32Array", () => {
        expect(encode.currentUser(token)).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.currentUser(token));
        const expected = [types.CURRENT_USER, ...encodedToken];
        expect(res).toEqual(expected);
    });
});

describe("changeNickname", () => {
    const nickname = faker.internet.userName();
    const encodedNickname = arrays.asArray(nickname);

    test("is must return Int32Array", () => {
        expect(encode.changeNickname(nickname)).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.changeNickname(nickname));
        const expected = [types.CHANGE_NICKNAME, ...encodedNickname];
        expect(res).toEqual(expected);
    });
});

describe("rpgSession", () => {
    const id = faker.random.number({ min: 1, max: 1000 });

    test("is must return Int32Array", () => {
        expect(encode.rpgSession({ id })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.rpgSession({ id }));
        const expected = [types.SINGLE_RPG_SESSION, id];
        expect(res).toEqual(expected);
    });
});

describe("rpgSessionsList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });

    test("is must return Int32Array", () => {
        expect(encode.rpgSessionsList({ page, limit })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.rpgSessionsList({ page, limit }));
        const expected = [types.RPG_SESSIONS_LIST, page, limit];
        expect(res).toEqual(expected);
    });
});

describe("usersList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });
    const authorIds = arrays.generateArrayOf(faker.random.number({
        min: 1,
        max: 10
    }), () => faker.random.number({ min: 1, max: 1000 }));

    test("is must return Int32Array", () => {
        expect(encode.usersList({ page, limit, authorIds })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.usersList({ page, limit, authorIds }));
        const expected = [types.USERS_LIST, page, limit, authorIds.length, ...authorIds];
        expect(res).toEqual(expected);
    });
});

describe("rpgCharactersList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });
    const rpgSessionIds = arrays.generateArrayOf(faker.random.number({
        min: 1,
        max: 10
    }), () => faker.random.number({ min: 1, max: 1000 }));

    test("is must return Int32Array", () => {
        expect(encode.rpgCharactersList({ page, limit, rpgSessionIds })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.rpgCharactersList({ page, limit, rpgSessionIds }));
        const expected = [types.RPG_CHARACTERS_LIST, page, limit, rpgSessionIds.length, ...rpgSessionIds];
        expect(res).toEqual(expected);
    });
});

describe("usersRelatedRpgCharactersList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });
    const userIds = arrays.generateArrayOf(faker.random.number({
        min: 1,
        max: 10
    }), () => faker.random.number({ min: 1, max: 1000 }));

    test("is must return Int32Array", () => {
        expect(encode.usersRelatedRpgCharactersList({ userIds, page, limit })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.usersRelatedRpgCharactersList({ userIds, page, limit }));
        const expected = [types.USERS_RELATED_RPG_CHARACTERS_LIST, page, limit, userIds.length, ...userIds];
        expect(res).toEqual(expected);
    });
});

describe("rpgEntriesList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });
    const rpgSessionIds = arrays.generateArrayOf(faker.random.number({
        min: 1,
        max: 10
    }), () => faker.random.number({ min: 1, max: 1000 }));

    test("is must return Int32Array", () => {
        expect(encode.rpgEntriesList({ page, limit, rpgSessionIds })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.rpgEntriesList({ page, limit, rpgSessionIds }));
        const expected = [types.RPG_ENTRIES_LIST, page, limit, rpgSessionIds.length, ...rpgSessionIds];
        expect(res).toEqual(expected);
    });
});

describe("sendRpgSessionsRpgCharactersList", () => {
    const page = faker.random.number({ min: 1, max: 1000 });
    const limit = faker.random.number({ min: 1, max: 1000 });
    const rpgSessionIds = arrays.generateArrayOf(faker.random.number({
        min: 1,
        max: 10
    }), () => faker.random.number({ min: 1, max: 1000 }));

    test("is must return Int32Array", () => {
        expect(encode.sendRpgSessionsRpgCharactersList({ page, limit, rpgSessionIds })).toBeInstanceOf(Int32Array);
    });

    test("must contains valid bytes", () => {
        const res = Array.from(encode.sendRpgSessionsRpgCharactersList({ page, limit, rpgSessionIds }));
        const expected = [types.RPG_SESSIONS_RPG_CHARACTERS_LIST, page, limit, rpgSessionIds.length, ...rpgSessionIds];
        expect(res).toEqual(expected);
    });
});
