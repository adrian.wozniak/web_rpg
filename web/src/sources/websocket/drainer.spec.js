import { build, drainString, drainNumber, drainUuid } from "./drainer";
import * as arrays                                    from "spec/arrays";
import * as objects                                   from "spec/objects";

test("decode user should be possible", () => {
    const user = objects.getUser();
    const normalized = arrays.normalizeFields(user);
    const array = arrays.getCurrentUserArray(normalized);
    const result = build(Int32Array.from(array))
        .then(drainNumber("id"))
        .then(drainString("email"))
        .then(drainString("nickname"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

    expect(result).toEqual(user);
});

test("decode token should be possible", () => {
    const token = objects.getToken();
    const normalized = arrays.normalizeFields(token);
    const array = arrays.getTokenArray(normalized);
    const result = build(Int32Array.from(array))
        .then(drainNumber("id"))
        .then(drainNumber("userId"))
        .then(drainUuid("token"))
        .then(drainString("createdAt"))
        .then(drainString("updatedAt"))
        .collect();

    expect(result).toEqual(token);
});
