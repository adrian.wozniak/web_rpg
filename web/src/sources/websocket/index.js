import * as wsTypes from "sources/websocket/types";
import { ping }     from "sources/websocket/encode";
import * as actions from "sources/websocket/actions";
import * as decode  from "sources/websocket/decode";

import store from "../../store";

import * as storeTypes    from "reducers/types";
import { getStoredToken } from "reducers/tokens";

const valueToName = n => {
    for (let fieldName in wsTypes) {
        if (wsTypes[fieldName] === n)
            return fieldName;
    }
    return Symbol(`Unknown message value ${n}`);
};

const socketUrl = () => {
    const location = new URL(process.env.BACKEND_HOST);
    const protocol = location.protocol === "https" ? "wss" : "ws";
    const { host } = location;
    console.log(`Web socket address is: ${protocol}://${host}/ws/`);
    return `${protocol}://${host}/ws/`;
};

let wsPromise = null;
let healthInterval = null;

export const NO_PING_KEY = "NO_WS_PING";
export const PING_EVERY_KEY = "PING_WS_EVERY";
export const PING_EVERY =
    localStorage.getItem(PING_EVERY_KEY)
        ? parseInt(localStorage.getItem(PING_EVERY_KEY))
        : 1000;

const sendPing = () =>
    !localStorage.getItem(NO_PING_KEY) ? WS.send(ping().buffer) : null;

const handleBinaryMessage = (message) => {
    try {
        const reader = new FileReader();
        reader.onload = ({ target: { result } }) => {
            const array = new Int32Array(result);
            const op = array[0];
            if (op !== wsTypes.PING && op !== wsTypes.PONG) {
                console.log("Received op %i %o", op, valueToName(op));
                console.log("   %o", array);
            }
            switch (op) {
                case wsTypes.PING:
                case wsTypes.PONG: {
                    setTimeout(() => sendPing(wsPromise), PING_EVERY);
                    break;
                }
                case wsTypes.OP_TOKEN:
                    return store.dispatch({
                        type:    storeTypes.TOKEN_RECEIVED,
                        payload: decode.decodeToken(array),
                    });
                case wsTypes.OP_VALID_TOKEN:
                    return store.dispatch({
                        type:    storeTypes.TOKEN_RECEIVED,
                        payload: decode.decodeToken(array),
                    });
                case wsTypes.OP_USER:
                    return store.dispatch({
                        type:    storeTypes.USER_RECEIVED,
                        payload: decode.decodeUser(array),
                    });
                case wsTypes.OP_CURRENT_USER:
                    return store.dispatch({
                        type:    storeTypes.CURRENT_USER_RECEIVED,
                        payload: decode.decodeUser(array),
                    });
                case wsTypes.OP_USERS_LIST:
                    return store.dispatch({
                        type:    storeTypes.USERS_RECEIVED,
                        payload: decode.decodeArrayOfUser(array),
                    });
                case wsTypes.OP_RPG_SESSION:
                    return store.dispatch({
                        type:    storeTypes.RPG_SESSION_RECEIVED,
                        payload: decode.decodeRpgSession(array),
                    });
                case wsTypes.OP_RPG_SESSIONS_LIST:
                    return store.dispatch({
                        type:    storeTypes.RPG_SESSIONS_RECEIVED,
                        payload: decode.decodeArrayOfRpgSession(array),
                    });
                case wsTypes.OP_RPG_CHARACTERS_LIST:
                    return store.dispatch({
                        type:    storeTypes.RPG_CHARACTERS_RECEIVED,
                        payload: decode.decodeArrayOfRpgCharacter(array),
                    });
                case wsTypes.OP_RPG_ENTRIES_LIST:
                    return store.dispatch({
                        type:    storeTypes.RPG_ENTRIES_RECEIVED,
                        payload: decode.decodeArrayOfRpgEntry(array),
                    });

                //    ERRORS
                case wsTypes.DELIVERY_FAILURE:
                    return store.dispatch({
                        type: storeTypes.DELIVERY_FAILURE,
                    });
                case wsTypes.SAVE_EMAIL_FAILURE:
                    return store.dispatch({
                        type: storeTypes.SAVE_EMAIL_FAILURE,
                    });
                case wsTypes.USER_NOT_FOUND:
                    return store.dispatch({
                        type: storeTypes.USER_NOT_FOUND,
                    });
                case wsTypes.EMAIL_ALREADY_TAKEN:
                    return store.dispatch({
                        type: storeTypes.EMAIL_ALREADY_TAKEN,
                    });
                case wsTypes.CREATE_TOKEN_FAILURE:
                    return store.dispatch({
                        type: storeTypes.CREATE_TOKEN_FAILURE,
                    });
                case wsTypes.TOKEN_NOT_FOUND:
                    return store.dispatch({
                        type: storeTypes.TOKEN_NOT_FOUND,
                    });
                case wsTypes.PARSE_TEMPLATE_FAILURE:
                    return store.dispatch({
                        type: storeTypes.PARSE_TEMPLATE_FAILURE,
                    });
                case wsTypes.INVALID_UUID_FORMAT:
                    return store.dispatch({
                        type: storeTypes.INVALID_UUID_FORMAT,
                    });
                case wsTypes.NICKNAME_ALREADY_TAKEN:
                    return store.dispatch({
                        type: storeTypes.NICKNAME_ALREADY_TAKEN,
                    });

                case wsTypes.NO_RPG_SESSIONS_FOUND:
                    return store.dispatch({
                        type: storeTypes.NO_RPG_SESSIONS_FOUND,
                    });
                case wsTypes.NO_USERS_FOUND:
                    return store.dispatch({
                        type: storeTypes.NO_USERS_FOUND,
                    });

                default:
                    console.log(array[0], array);
            }
        };
        reader.readAsArrayBuffer(message);
    } catch (_) {
        console.error("Invalid number of bytes in response");
        console.error(" Some parts of encode to u8 vector are wrong and length is not divable by 4");
    }
};

const handleStringMessage = (message) => {
    console.log("String ws message: %o", message);
};

const handleMessage = (message) => {
    if (message instanceof Blob) {
        return handleBinaryMessage(message);
    } else {
        return handleStringMessage(message);
    }
};

const createSocket = () => wsPromise = new Promise(resolve => {
    console.info("Creating Web Socket...");
    if (healthInterval)
        clearInterval(healthInterval);

    const webSocket = new WebSocket(socketUrl());
    const waitForOpen = () => requestAnimationFrame(() => {
        console.log("Waiting for ws to open...\nCurrent ws state: %o", webSocket.readyState);
        if (webSocket.readyState === webSocket.OPEN) {
            console.log("Web socket connected!");
            sendPing(webSocket);
            if (getStoredToken())
                actions.sendAuthorize({ token: getStoredToken() });
            wsPromise = Promise.resolve(webSocket);
            resolve(webSocket);
        } else waitForOpen();
    });
    webSocket.addEventListener("open", () => {
        webSocket.addEventListener("message", (event) => {
            handleMessage(event.data);
        });
        webSocket.addEventListener("close", () => {
            console.error("web socket closing or closed");
            createSocket();
        });
        webSocket.onerror = (event) => {
            console.error("web socket error");
            if (webSocket.readyState === webSocket.CLOSED || webSocket.readyState === webSocket.CLOSING) {
                console.error("web socket error closing or closed");
                createSocket();
            }
            console.error(event);
        };
        waitForOpen();
    });
    healthInterval = setInterval(() => {
        // console.log("Current ws state: %o", ws.readyState);
        switch (webSocket.readyState) {
            case webSocket.CLOSING:
            case webSocket.CLOSED: {
                console.error("Web socket is closing or already closed :(");
                createSocket();
                wsPromise = Promise.reject(new Error("Web socket is closing or already closed :("));
                break;
            }
        }
    }, 1000);
});

class WS {
    static async send(msg) {
        if (process.env.NODE_ENV === "development" && msg[0] > 1) {
            console.log("sending message %o %o", valueToName(msg[0]), msg);
        }

        if (!wsPromise) {
            await createSocket();
            const ws = await wsPromise;
            ws.send(msg);
            return msg;
        }

        try {
            const ws = await wsPromise;
            ws.send(msg);
            return msg;
        } catch (_) {
            await createSocket();
            const ws = await wsPromise;
            ws.send(msg);
            return msg;
        }
    }
}

export default WS;
window.WS = WS;
