/* output messages */
export const PING = 1;
export const PONG = 2;
export const SIGN_IN = 3;
export const SIGN_UP = 4;
export const AUTHORIZE = 5;
export const CURRENT_USER = 6;
export const CHANGE_NICKNAME = 7;
export const RPG_SESSIONS_LIST = 8;
export const USERS_LIST = 9;
export const RPG_CHARACTERS_LIST = 10;
export const RPG_ENTRIES_LIST = 11;
export const SINGLE_RPG_SESSION = 12;
export const USERS_RELATED_RPG_CHARACTERS_LIST = 13;
export const RPG_SESSIONS_RPG_CHARACTERS_LIST = 14;
export const CREATE_RPG_SESSION_ENTRY = 15;


/* web socket errors */
export const DELIVERY_FAILURE = 201;
export const SAVE_EMAIL_FAILURE = 202;
export const USER_NOT_FOUND = 203;
export const EMAIL_ALREADY_TAKEN = 204;
export const CREATE_TOKEN_FAILURE = 205;
export const TOKEN_NOT_FOUND = 206;
export const PARSE_TEMPLATE_FAILURE = 207;
export const INVALID_UUID_FORMAT = 208;
export const NICKNAME_ALREADY_TAKEN = 209;
export const NO_RPG_SESSIONS_FOUND = 210;
export const NO_USERS_FOUND = 211;


/* incoming messages */
export const OP_USER = 400;
export const OP_TOKEN = 401;
export const OP_CURRENT_USER = 402;
export const OP_VALID_TOKEN = 403;
export const OP_RPG_SESSIONS_LIST = 404;
export const OP_USERS_LIST = 405;
export const OP_RPG_CHARACTERS_LIST = 406;
export const OP_RPG_ENTRIES_LIST = 407;
export const OP_RPG_SESSION = 408;
export const RPG_SESSIONS_RPG_CHARACTERS = 409;
