import * as types from "../reducers/types";

export const openNicknameModal = payload => ({
    type: types.OPEN_NICKNAME_MODAL,
    payload,
});

export const nicknameModalInput = nickname => ({
    type:    types.NICKNAME_MODAL_INPUT,
    payload: { nickname },
});

export const nicknameModalSubmit = payload => ({
    type: types.NICKNAME_MODAL_SUBMIT,
    payload
});
