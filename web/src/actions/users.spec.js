import faker from "faker";

import * as actions from "actions";
import * as types   from "reducers/types";
import * as arrays  from "spec/arrays";

describe("requestUsers", () => {
    test("without params", () => {
        const payload = undefined;
        const result = actions.requestUsers(payload);
        const expected = {
            type:    types.USERS_REQUEST,
            payload: { page: 0, limit: 100, authorIds: [] }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const page = faker.random.number(1000);
        const limit = faker.random.number(1000);
        const authorIds = arrays.generateArrayOf(
            faker.random.number(100),
            () => faker.random.number(1000)
        );
        const payload = { page, limit, authorIds };
        const result = actions.requestUsers(payload);
        const expected = {
            type:    types.USERS_REQUEST,
            payload: { page, limit, authorIds }
        };
        expect(result).toEqual(expected);
    });
});
