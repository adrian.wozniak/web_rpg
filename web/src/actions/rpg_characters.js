import * as types from "../reducers/types";

export const requestRpgCharacters = ({ page = 0, limit = 100, rpgSessionIds = [] } = {}) => ({
    type:    types.RPG_CHARACTERS_REQUEST,
    payload: { page, limit, rpgSessionIds }
});
