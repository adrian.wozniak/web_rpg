import faker from "faker";

import * as actions from "actions";
import * as types   from "reducers/types";

describe("checkToken", () => {
    test("without params", () => {
        const result = actions.checkToken(undefined);
        const expected = {
            type:    types.CHECK_TOKEN,
            payload: { token: undefined }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const uuid = faker.random.uuid();
        const result = actions.checkToken(uuid);
        const expected = {
            type:    types.CHECK_TOKEN,
            payload: { token: uuid }
        };
        expect(result).toEqual(expected);
    });
});

describe("requestCurrentUser", () => {
    test("without params", () => {
        const result = actions.requestCurrentUser(undefined);
        const expected = {
            type:    types.CURRENT_USER_REQUEST,
            payload: { token: undefined }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const uuid = faker.random.uuid();
        const result = actions.requestCurrentUser(uuid);
        const expected = {
            type:    types.CURRENT_USER_REQUEST,
            payload: { token: uuid }
        };
        expect(result).toEqual(expected);
    });
});

describe("signInSubmit", () => {
    test("without params", () => {
        const result = actions.signInSubmit(undefined);
        const expected = {
            type:    types.SIGN_IN_SUBMIT,
            payload: undefined
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const result = actions.signInSubmit("");
        const expected = {
            type:    types.SIGN_IN_SUBMIT,
            payload: ""
        };
        expect(result).toEqual(expected);
    });
});

describe("signInInput", () => {
    test("without params", () => {
        const email = undefined;
        const result = actions.signInInput(email);
        const expected = {
            type:    types.SIGN_IN_INPUT,
            payload: { email }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const email = faker.internet.email();
        const result = actions.signInInput(email);
        const expected = {
            type:    types.SIGN_IN_INPUT,
            payload: { email }
        };
        expect(result).toEqual(expected);
    });
});

describe("signUpSubmit", () => {
    test("without params", () => {
        const result = actions.signUpSubmit(undefined);
        const expected = {
            type:    types.SIGN_UP_SUBMIT,
            payload: undefined
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const result = actions.signUpSubmit("");
        const expected = {
            type:    types.SIGN_UP_SUBMIT,
            payload: ""
        };
        expect(result).toEqual(expected);
    });
});

describe("signUpInput", () => {
    test("without params", () => {
        const email = undefined;
        const result = actions.signUpInput(email);
        const expected = {
            type:    types.SIGN_UP_INPUT,
            payload: { email }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const email = faker.internet.email();
        const result = actions.signUpInput(email);
        const expected = {
            type:    types.SIGN_UP_INPUT,
            payload: { email }
        };
        expect(result).toEqual(expected);
    });
});
