import faker from "faker";

import * as actions from "actions";
import * as types   from "reducers/types";
import * as arrays  from "spec/arrays";

describe("requestRpgCharacters", () => {
    test("without params", () => {
        const payload = undefined;
        const result = actions.requestRpgCharacters(payload);
        const expected = {
            type:    types.RPG_CHARACTERS_REQUEST,
            payload: { page: 0, limit: 100, rpgSessionIds: [] }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const page = faker.random.number(1000);
        const limit = faker.random.number(1000);
        const rpgSessionIds = arrays.generateArrayOf(
            faker.random.number(100),
            () => faker.random.number(1000)
        );
        const payload = { page, limit, rpgSessionIds };
        const result = actions.requestRpgCharacters(payload);
        const expected = {
            type:    types.RPG_CHARACTERS_REQUEST,
            payload: { page, limit, rpgSessionIds }
        };
        expect(result).toEqual(expected);
    });
});
