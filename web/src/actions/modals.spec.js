import faker from "faker";

import * as actions from "actions";
import * as types   from "reducers/types";

describe("openNicknameModal", () => {
    test("without params", () => {
        const param = undefined;
        const result = actions.openNicknameModal(param);
        const expected = {
            type:    types.OPEN_NICKNAME_MODAL,
            payload: param
        };
        expect(result).toEqual(expected);
    });

    test("with param", () => {
        const param = faker.internet.userName();
        const result = actions.openNicknameModal(param);
        const expected = {
            type:    types.OPEN_NICKNAME_MODAL,
            payload: param
        };
        expect(result).toEqual(expected);
    });
});

describe("nicknameModalInput", () => {
    test("without params", () => {
        const nickname = undefined;
        const result = actions.nicknameModalInput(nickname);
        const expected = {
            type:    types.NICKNAME_MODAL_INPUT,
            payload: { nickname }
        };
        expect(result).toEqual(expected);
    });

    test("with param", () => {
        const nickname = faker.internet.userName();
        const result = actions.nicknameModalInput(nickname);
        const expected = {
            type:    types.NICKNAME_MODAL_INPUT,
            payload: { nickname }
        };
        expect(result).toEqual(expected);
    });
});

describe("nicknameModalSubmit", () => {
    test("without params", () => {
        const param = undefined;
        const result = actions.nicknameModalSubmit(param);
        const expected = {
            type:    types.NICKNAME_MODAL_SUBMIT,
            payload: param
        };
        expect(result).toEqual(expected);
    });

    test("with param", () => {
        const param = faker.internet.userName();
        const result = actions.nicknameModalSubmit(param);
        const expected = {
            type:    types.NICKNAME_MODAL_SUBMIT,
            payload: param
        };
        expect(result).toEqual(expected);
    });
});
