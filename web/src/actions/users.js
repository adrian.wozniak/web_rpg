import * as types from "../reducers/types";

export const requestUsers = ({ authorIds = [], page = 0, limit = 100 } = {}) => ({
    type:    types.USERS_REQUEST,
    payload: { authorIds, page, limit }
});
