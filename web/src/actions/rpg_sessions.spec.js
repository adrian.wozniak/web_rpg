import faker from "faker";

import * as arrays  from "spec/arrays";
import * as actions from "actions";
import * as types   from "reducers/types";

describe("requestRpgSession", () => {
    test("without params", () => {
        const payload = undefined;
        expect(() => actions.requestRpgSession(payload)).toThrow();
    });

    test("with params", () => {
        const id = faker.random.number(1000);
        const payload = { id };
        const result = actions.requestRpgSession(payload);
        const expected = {
            type:    types.RPG_SESSION_REQUEST,
            payload: { id }
        };
        expect(result).toEqual(expected);
    });
});

describe("requestRpgSessions", () => {
    test("without params", () => {
        const payload = undefined;
        const result = actions.requestRpgSessions(payload);
        const expected = {
            type:    types.RPG_SESSIONS_REQUEST,
            payload: { page: 0, limit: 100 }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const page = faker.random.number(1000);
        const limit = faker.random.number(1000);
        const payload = { page, limit };
        const result = actions.requestRpgSessions(payload);
        const expected = {
            type:    types.RPG_SESSIONS_REQUEST,
            payload: { page, limit }
        };
        expect(result).toEqual(expected);
    });
});

describe("requestUsersRelatedRpgSessions", () => {
    test("without params", () => {
        const payload = undefined;
        const result = actions.requestUsersRelatedRpgSessions(payload);
        const expected = {
            type:    types.USERS_RELATED_RPG_CHARACTERS_LIST_REQUEST,
            payload: { page: 0, limit: 100, userIds: [] }
        };
        expect(result).toEqual(expected);
    });

    test("with params", () => {
        const page = faker.random.number(1000);
        const limit = faker.random.number(1000);
        const userIds = arrays.generateArrayOf(
            faker.random.number(100),
            () => faker.random.number(1000)
        );
        const payload = { page, limit, userIds };
        const result = actions.requestUsersRelatedRpgSessions(payload);
        const expected = {
            type:    types.USERS_RELATED_RPG_CHARACTERS_LIST_REQUEST,
            payload: { page, limit, userIds }
        };
        expect(result).toEqual(expected);
    });
});
