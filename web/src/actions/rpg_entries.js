import * as types from "../reducers/types";

export const requestRpgEntries = ({ page = 0, limit = 100, rpgSessionIds = [] } = {}) => ({
    type:    types.RPG_ENTRIES_REQUEST,
    payload: { page, limit, rpgSessionIds }
});

export const requestCreateRpgEntry = ({ userId, rpgCharacterId, rpgSessionId, input } = {}) => ({
    type:    types.CREATE_RPG_SESSION_ENTRY_REQUEST,
    payload: { userId, rpgCharacterId, rpgSessionId, input }
});

export const rpgEntryFormRpgCharacterIdChanged = ({ rpgCharacterId }) => ({
    type:    types.RPG_ENTRY_FORM_RPG_CHARACTER_ID_CHANGED,
    payload: { rpgCharacterId }
});
export const rpgEntryFormUserIdChanged = ({ userId }) => ({
    type:    types.RPG_ENTRY_FORM_USER_ID_CHANGED,
    payload: { userId }
});
export const rpgEntryFormRpgSessionIdChanged = ({ rpgSessionId }) => ({
    type:    types.RPG_ENTRY_FORM_RPG_SESSION_ID_CHANGED,
    payload: { rpgSessionId }
});
export const rpgEntryFormInputChanged = ({ input }) => ({
    type:    types.RPG_ENTRY_FORM_INPUT_CHANGED,
    payload: { input }
});
