import * as types from "reducers/types";

export const requestRpgSessionsRpgCharacters = ({ rpgSessionIds }) => ({
    type:    types.RPG_SESSIONS_RPG_CHARACTERS_LIST_REQUEST,
    payload: { rpgSessionIds }
});
