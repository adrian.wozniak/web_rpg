import * as types from "../reducers/types";

export const checkToken = token => ({
    type:    types.CHECK_TOKEN,
    payload: { token }
});

export const requestCurrentUser = token => ({
    type:    types.CURRENT_USER_REQUEST,
    payload: { token }
});

export const signInSubmit = payload => ({
    type: types.SIGN_IN_SUBMIT,
    payload
});

export const signInInput = email => ({
    type:    types.SIGN_IN_INPUT,
    payload: { email }
});

export const signUpSubmit = payload => ({
    type: types.SIGN_UP_SUBMIT,
    payload
});

export const signUpInput = email => ({
    type:    types.SIGN_UP_INPUT,
    payload: { email }
});
