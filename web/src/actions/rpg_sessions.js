import * as types from "../reducers/types";

export const requestRpgSession = ({ id }) => ({
    type:    types.RPG_SESSION_REQUEST,
    payload: { id }
});

export const requestRpgSessions = ({ page = 0, limit = 100 } = {}) => ({
    type:    types.RPG_SESSIONS_REQUEST,
    payload: { page, limit }
});

export const requestUsersRelatedRpgSessions = ({ page = 0, limit = 100, userIds = [] } = {}) => ({
    type:    types.USERS_RELATED_RPG_CHARACTERS_LIST_REQUEST,
    payload: { page, limit, userIds }
});
