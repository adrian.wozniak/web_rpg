import * as sort from "./sort";

test("sortNumbers", () => {
    const expected = [3, 4, 7, 7, 15, 29];
    const array = [3, 15, 4, 7, 29, 7];
    const result = array.sort(sort.sortNumbers);
    expect(result).toEqual(expected);
});

test("sortById", () => {
    const expected = [
        { id: 3 },
        { id: 4 },
        { id: 7 },
        { id: 7 },
        { id: 15 },
        { id: 29 }
    ];
    const array = [
        { id: 3 },
        { id: 15 },
        { id: 7 },
        { id: 4 },
        { id: 29 },
        { id: 7 }
    ];
    const result = array.sort(sort.sortById);
    expect(result).toEqual(expected);
});
