import React     from "react";
import ReactDom  from "react-dom";
import PropTypes from "prop-types";
import "@webcomponents/webcomponentsjs";

Object.defineProperty(React, "PropTypes", { value: PropTypes });

import "./app.css";
import "./translations";
// import WS from "./sources/websocket";
// WS.send('hello');

import Main from "components/Main";

import "./rust";
//
// ReactDom.render(
//     <Main/>,
//     document.body.appendChild(document.createElement("main"))
// );
