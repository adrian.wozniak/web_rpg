import { call } from "redux-saga/effects";

import * as wsActions   from "sources/websocket/actions";

export const requestUsers = function* ({ payload: { authorIds, page, limit } }) {
    try {
        yield call(wsActions.sendUsersList, { authorIds, page, limit });
    } catch (error) {
        console.error("current user request failed");
        console.error("  params %o", { page, limit, authorIds });
    }
};
