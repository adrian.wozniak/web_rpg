import { call } from "redux-saga/effects";

import * as wsActions from "sources/websocket/actions";

export const requestSingleRpgSession = function* ({ payload: { id } }) {
    try {
        yield call(wsActions.sendSingleRpgSession, { id });
    } catch (error) {
        console.error("rpg session request failed");
        console.error("  params %o", { id });
    }
};

export const requestRpgSessions = function* ({ payload: { page, limit } }) {
    try {
        yield call(wsActions.sendRpgSessionsList, { page, limit });
    } catch (error) {
        console.error("rpg sessions list request failed");
        console.error("  params %o", { page, limit });
    }
};
