import { takeEvery } from "redux-saga/effects";

import * as types from "reducers/types";

import * as accountSaga                  from "sagas/account";
import * as usersSaga                    from "sagas/users";
import * as rpgSessionsSaga              from "sagas/rpg_sessions";
import * as rpgCharactersSaga            from "sagas/rpg_characters";
import * as rpgEntriesSaga               from "sagas/rpg_entries";
import * as rpgSessionsRpgCharactersSaga from "sagas/rpg_sessions_rpg_characters";

export default function* () {
    yield takeEvery(types.SIGN_UP_SUBMIT, accountSaga.signUpSaga);
    yield takeEvery(types.SIGN_IN_SUBMIT, accountSaga.signInSaga);
    yield takeEvery(types.CHECK_TOKEN, accountSaga.authorizeSaga);
    yield takeEvery(types.CURRENT_USER_REQUEST, accountSaga.requestCurrentUserSaga);
    yield takeEvery(types.NICKNAME_MODAL_SUBMIT, accountSaga.requestChangeNicknameSaga);

    yield takeEvery(types.USERS_REQUEST, usersSaga.requestUsers);

    yield takeEvery(types.RPG_SESSION_REQUEST, rpgSessionsSaga.requestSingleRpgSession);
    yield takeEvery(types.RPG_SESSIONS_REQUEST, rpgSessionsSaga.requestRpgSessions);

    yield takeEvery(types.RPG_ENTRIES_REQUEST, rpgEntriesSaga.sendRpgEntriesList);
    yield takeEvery(types.CREATE_RPG_SESSION_ENTRY_REQUEST, rpgEntriesSaga.sendCreateRpgEntry);

    yield takeEvery(types.RPG_CHARACTERS_REQUEST, rpgCharactersSaga.requestRpgCharacters);
    yield takeEvery(types.USERS_RELATED_RPG_CHARACTERS_LIST_REQUEST, rpgCharactersSaga.requestUsersRelatedRpgCharacters);

    yield takeEvery(types.RPG_SESSIONS_RPG_CHARACTERS_LIST_REQUEST, rpgSessionsRpgCharactersSaga.sendRpgSessionsRpgCharacters);
}
