import { call } from "redux-saga/effects";

import * as wsActions from "sources/websocket/actions";

export const sendRpgSessionsRpgCharacters = function* ({ payload: { page, limit, rpgSessionIds } }) {
    try {
        yield call(wsActions.sendRpgSessionsRpgCharactersList, { page, limit, rpgSessionIds });
    } catch (error) {
        console.error("rpg sessions rpg characters request failed");
        console.error("  params %o", { page, limit, rpgSessionIds });
    }
};
