import { call } from "redux-saga/effects";

import * as wsActions from "sources/websocket/actions";

export const sendRpgEntriesList = function* ({ payload: { page, limit, rpgSessionIds } }) {
    try {
        yield call(wsActions.sendRpgEntriesList, { page, limit, rpgSessionIds });
    } catch (error) {
        console.error("rpg entries request failed");
        console.error("  params %o", { page, limit, rpgSessionIds });
    }
};

export const sendCreateRpgEntry = function* ({ payload: { userId, rpgCharacterId, rpgSessionId, input } }) {
    try {
        yield call(wsActions.sendCreateRpgEntry, { userId, rpgCharacterId, rpgSessionId, input });
    } catch (error) {
        console.error("create rpg entry request failed");
        console.error("  params %o", { userId, rpgCharacterId, rpgSessionId, input });
    }
};
