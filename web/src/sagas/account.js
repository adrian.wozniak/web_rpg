import { call, select } from "redux-saga/effects";

import * as wsActions from "sources/websocket/actions";

export const signUpSaga = function* () {
    try {
        const state = yield select();
        yield call(wsActions.sendSignUp, { email: state.forms.signUp.email });
    } catch (error) {
        console.error("sign up request failed");
    }
};

export const signInSaga = function* () {
    try {
        const state = yield select();
        yield call(wsActions.sendSignIn, { email: state.forms.signIn.email });
    } catch (error) {
        console.error("sign in request failed");
    }
};

export const authorizeSaga = function* ({ payload: { token } }) {
    try {
        yield call(wsActions.sendAuthorize, { token });
    } catch (error) {
        console.error("authorize request failed");
        console.error("  token: %o", token);
    }
};

export const requestCurrentUserSaga = function* ({ payload: { token } }) {
    try {
        yield call(wsActions.sendCurrentUser, { token });
    } catch (error) {
        console.error("current user request failed");
    }
};

export const requestChangeNicknameSaga = function* () {
    try {
        const { modals: { nickname: { nickname } } } = yield select();
        if (nickname && nickname.length) {
            yield call(wsActions.sendChangeNickname, { nickname });
        }
    } catch (error) {
        console.error("current user request failed");
    }
};
