import { call } from "redux-saga/effects";

import * as wsActions from "sources/websocket/actions";

export const requestRpgCharacters = function* ({ payload: { page, limit, rpgSessionIds } }) {
    try {
        yield call(wsActions.sendRpgCharactersList, { page, limit, rpgSessionIds });
    } catch (error) {
        console.error("rpg characters request failed");
        console.error("  params %o", { page, limit, rpgSessionIds });
    }
};

export const requestUsersRelatedRpgCharacters = function* ({ payload: { page, limit, userIds } }) {
    try {
        yield call(wsActions.sendUsersRelatedRpgCharactersList, { page, limit, userIds });
    } catch (error) {
        console.error("user related rpg characters request failed");
        console.error("  params %o", { page, limit, userIds });
    }
};
