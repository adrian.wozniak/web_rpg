import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware             from "redux-saga";

import reducer from "./reducers";
import saga    from "./sagas";

let sagaMiddleware;
if (window["__SAGA_MONITOR_EXTENSION__"]) {
    const monitor = window["__SAGA_MONITOR_EXTENSION__"];
    sagaMiddleware = createSagaMiddleware({ sagaMonitor: monitor });
} else {
    sagaMiddleware = createSagaMiddleware();
}
let middleware = [sagaMiddleware];

if (process.env.NODE_ENV === "development") {
    middleware.push(require("redux-freeze"));
}

middleware = applyMiddleware(...middleware);

if (process.env.NODE_ENV === "development") {
    middleware = require("redux-devtools-extension").composeWithDevTools(middleware);
}

const store = createStore(reducer, middleware);
sagaMiddleware.run(saga);
export default store;
