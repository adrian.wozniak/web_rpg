import i18next                from "i18next";
import Cache                  from "i18next-localstorage-cache";
import LanguageDetector       from "i18next-browser-languagedetector";
import postProcessor          from "i18next-sprintf-postprocessor";
import { reactI18nextModule } from "react-i18next";

import pl from "./pl";
import en from "./en";

i18next
    .use(Cache)
    .use(LanguageDetector)
    .use(postProcessor)
    .use(reactI18nextModule)
    .init({
        debug:       process.env.NODE_ENV === "development",
        fallbackLng: "en",
        resources:   { pl, en },
        react:       {
            wait: true
        }
    });
