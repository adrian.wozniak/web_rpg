export default {
    signUp:        {
        email:  "E-Mail",
        legend: "Rejestracja",
        submit: "Wyślij",
        errors: {
            databaseUnavailable: "Problem z połączniem z bazą danych",
            deliveryFailure:     "Problem z dostarczeniem e-mail'a",
            saveEmailFailure:    "Problem z zapisaniem adresu e-mail",
            userNotFound:        "Problem ze znalezieniem użytkownika dla danego adresu e-mail'a",
            emailAlreadyTaken:   "Adres e-mail został już wykorzystany",
        },
    },
    signIn:        {
        email:  "E-Mail",
        legend: "Logowanie",
        submit: "Zaloguj",
        errors: {
            databaseUnavailable:  "Database unavailable",
            createTokenFailure:   "Create token failure",
            tokenNotFound:        "Token not found",
            parseTemplateFailure: "Parse template failure",
            invalidUuidFormat:    "Invalid uuid format",
        },
    },
    authorize:     {
        loading: "Autoryzuję...",
        errors:  {
            databaseUnavailable:  "Problem z połączniem z bazą danych",
            createTokenFailure:   "Problem z utworzeniem tokenu dostępu",
            tokenNotFound:        "Nieprawidłowy token",
            parseTemplateFailure: "Problem z wygenerowaniem mail",
            invalidUuidFormat:    "Token nie ma właściwiej struktury",
        },
    },
    nicknameModal: {
        pickNickname: "Wybierz swój nick",
        submit:       "Zapisz",
        alreadyTaken: "Nickname jest zajęty",
    },
    navbar:        {
        home: "Główna strona"
    },
    rpgEntryForm: {
        submit: "Zapisz wpis",
    },
};
