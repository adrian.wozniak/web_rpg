export default {
    signUp:        {
        email:  "E-Mail",
        legend: "Sign Up",
        submit: "Submit",
        errors: {
            databaseUnavailable: "Database cannot be reached",
            deliveryFailure:     "Cannot deliver email",
            saveEmailFailure:    "Cannot save e-mail address",
            userNotFound:        "Cannot find user with given e-mail",
            emailAlreadyTaken:   "E-mail address had been already taken",
        },
    },
    signIn:        {
        email:  "E-Mail",
        legend: "Sign In",
        submit: "Log In",
        errors: {
            databaseUnavailable:  "Database unavailable",
            createTokenFailure:   "Create token failure",
            tokenNotFound:        "Token not found",
            parseTemplateFailure: "Parse template failure",
            invalidUuidFormat:    "Invalid uuid format",
        },
    },
    authorize:     {
        loading: "Authorizing...",
        errors:  {
            databaseUnavailable:  "Database unavailable",
            createTokenFailure:   "Create token failure",
            tokenNotFound:        "Token not found",
            parseTemplateFailure: "Parse template failure",
            invalidUuidFormat:    "Invalid uuid format",
        },
    },
    nicknameModal: {
        pickNickname: "Choose your nickname",
        submit:       "Save",
        alreadyTaken: "Nickname is already taken",
    },
    navbar:        {
        home: "Home",
    },
    rpgEntryForm: {
        submit: "Save entry",
    },
};
