import faker from "faker";

export const getRpgStatistics = () =>
    ([
        "Strength",
        "Dexterity",
        "Charisma",
        "Intelligence",
        "Wisdom",
        "Constitution"
    ]);

export const getRandomStatistic = (stats = getRpgStatistics()) =>
    faker.random.arrayElement(stats);
