import faker      from "faker";
import { format } from "date-fns";

import * as rpgSystems    from "./rpg_systems";
import * as rpgStatistics from "./rpg_statistics";

export const getRpgEntries = (n = 1) =>
    new Array(n).fill({}).map(() => getRpgEntry());

export const getRpgSessions = (n = 1) =>
    new Array(n).fill({}).map(() => getRpgSession());

export const getUsers = (n = 1) =>
    new Array(n).fill({}).map(() => getUser());

export const getRpgCharacters = (n = 1) =>
    new Array(n).fill({}).map(() => getRpgCharacter());

export const getRpgSessionsRpgCharacters = (n = 1) =>
    new Array(n).fill({}).map(() => getRpgSessionsRpgCharacter());

export const getRpgEntry = () =>
    ({
        id:             faker.random.number(100),
        userId:         faker.random.number(100),
        rpgCharacterId: faker.random.number(100),
        rpgSessionId:   faker.random.number(100),
        input:          faker.lorem.lines(10),
        createdAt:      format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt:      format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });

export const getRpgSession = () =>
    ({
        id:        faker.random.number(100),
        userId:    faker.random.number(100),
        name:      faker.company.catchPhrase(),
        createdAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });

export const getToken = () =>
    ({
        id:        faker.random.number(100),
        userId:    faker.random.number(100),
        token:     faker.random.uuid(),
        createdAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });

export const getUser = () =>
    ({
        id:        faker.random.number(100),
        email:     faker.internet.email(),
        nickname:  faker.internet.userName(),
        createdAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt: format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });

export const getRpgCharacter = ({ id, userId } = {}) =>
    ({
        id:         id || faker.random.number(100),
        userId:     userId || faker.random.number(100),
        name:       [faker.name.firstName(), faker.name.lastName()].join(" "),
        imageUrl:   faker.internet.url(),
        statistics: new Array(faker.random.number({ min: 2, max: 10 })).fill({}).map(() =>
            `${rpgStatistics.getRandomStatistic()}: ${faker.random.number({ min: 1, max: 10 })}`
        ),
        statType:   rpgSystems.getRandomRpgSystem(),
        createdAt:  format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt:  format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });

export const getRpgSessionsRpgCharacter = () =>
    ({
        id:             faker.random.number(100),
        rpgCharacterId: faker.random.number(100),
        rpgSessionId:   faker.random.number(100),
        createdAt:      format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        ),
        updatedAt:      format(
            faker.date.between("2007-01-01", "2020-10-10"),
            "MM/DD/YYYY hh:mm:ss"
        )
    });
