import * as types from "sources/websocket/types";

export const getRpgEntry =
    ({ id, userId, rpgCharacterId, rpgSessionId, input, createdAt, updatedAt }) =>
        ([
            id,
            userId,
            rpgCharacterId,
            rpgSessionId,
            ...input,
            ...createdAt,
            ...updatedAt
        ]);

export const getRpgEntriesList = (...entries) => [
    types.OP_RPG_ENTRIES_LIST,
    entries.length, // length
    ...entries.reduce((memo, entry) => [
        ...memo,
        ...getRpgEntry(entry),
    ], []),
];
