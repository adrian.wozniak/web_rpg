/*
2, // id
3, // user id
1827285955, 1313275345, -578749796, 1538269910, // uuid
19, // created at length
50, 48, 49, 56, 45, 49, 50, 45, 49, 48, 32, 49, 50, 58, 50, 54, 58, 48, 54, // 2018-12-10 12:26:06
19, // updated at length
50, 48, 49, 56, 45, 49, 50, 45, 49, 48, 32, 49, 50, 58, 50, 54, 58, 48, 54 // 2018-12-10 12:26:06
*/
export const getToken = ({ id, userId, token, createdAt, updatedAt }) =>
    ([
        id,
        userId,
        ...token,
        ...createdAt,
        ...updatedAt,
    ]);

export const getTokenArray = (token) => [
    61, //op
    ...getToken(token),
];
