import * as types from "sources/websocket/types";

export const getRpgSession =
    ({ id, userId, name, createdAt, updatedAt }) => [
        id, // session id
        userId, // user id
        ...name,
        ...createdAt,
        ...updatedAt,
    ];

export const getRpgSessionsList = (...rpgSessions) => [
    types.OP_RPG_SESSIONS_LIST, // op
    rpgSessions.length, // array length
    ...rpgSessions.reduce((m, o) => [...m, ...getRpgSession(o)], []),
];
