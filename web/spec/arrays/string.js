export const asArray = (str) => [
    str.length,
    ...str.split("").map(c => c.charCodeAt(0)),
];
