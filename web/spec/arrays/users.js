import * as types from "sources/websocket/types";

export const getUser = ({ id, email, nickname, createdAt, updatedAt }) =>
    ([
        id,
        ...email,
        ...nickname,
        ...createdAt,
        ...updatedAt,
    ]);

export const getUsersArray = (...users) => [
    405, // op
    users.length, // array length
    ...users.reduce((m, u) => [...m, ...getUser(u)], []),
];

export const getCurrentUserArray = (user) =>
    ([
        types.OP_CURRENT_USER, // op
        ...getUser(user),
    ]);
