import * as types from "sources/websocket/types";

export const getRpgCharacter =
    ({ id, userId, name, imageUrl, statistics, statType, createdAt, updatedAt }) =>
        ([
            id,
            userId,
            ...name,
            ...imageUrl,
            ...statistics,
            ...statType,
            ...createdAt,
            ...updatedAt
        ]);

export const getRpgCharactersList = (...entries) => [
    types.OP_RPG_CHARACTERS_LIST,
    entries.length, // length
    ...entries.reduce((memo, entry) => [
        ...memo,
        ...getRpgCharacter(entry),
    ], []),
];
