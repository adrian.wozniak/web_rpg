import * as types from "sources/websocket/types";

export const getRpgSessionsRpgCharacter =
    ({ id, rpgSessionId, rpgCharacterId, createdAt, updatedAt }) => [
        id,
        rpgCharacterId,
        rpgSessionId,
        ...createdAt,
        ...updatedAt
    ];

export const getRpgSessionsRpgCharactersList = (...a) => [
    types.OP_RPG_SESSIONS_LIST, // op
    a.length, // array length
    ...a.reduce((m, o) => [...m, ...getRpgSessionsRpgCharacter(o)], [])
];
