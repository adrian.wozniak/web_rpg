export const generateArrayOf = (len, fillWith) =>
    new Array(len).fill(0).map(fillWith);
