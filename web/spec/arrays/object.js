import { parse } from "uuid-parse";

import * as string from "./string";

const prepareUuid = (a) =>
    ([
        [0, 1, 2, 3].map(i => (a[i] << (i * 8))).reduce((m, n) => m + n, 0),
        [0, 1, 2, 3].map(i => (a[i + 4] << (i * 8))).reduce((m, n) => m + n, 0),
        [0, 1, 2, 3].map(i => (a[i + 8] << (i * 8))).reduce((m, n) => m + n, 0),
        [0, 1, 2, 3].map(i => (a[i + 12] << (i * 8))).reduce((m, n) => m + n, 0)
    ]);

const parseUuid = s =>
    prepareUuid(parse(s));

const asMapperKey = obj =>
    Reflect
        .apply(({}).toString, obj, [])
        .replace(/^\[object /, "")
        .replace(/\]$/, "")
        .toLowerCase();

const isUuid = s =>
    s.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);

const maps = ({
    "string": s => isUuid(s) ? parseUuid(s) : string.asArray(s),
    "number": n => n,
    "array":  a => [
        a.length,
        ...a.reduce((memo, e) => {
            const type = asMapperKey(e);
            const serializer = maps[type];
            const res = serializer(e);
            return Array.isArray(res)
                ? [...memo, ...res]
                : [...memo, res];
        }, [])
    ]
});

export const normalizeFields = (object) =>
    Object.keys(object).reduce((memo, key) => ({
        ...memo,
        [key]: maps[asMapperKey(object[key])](object[key])
    }), {});
