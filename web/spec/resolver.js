const { resolve, join } = require("path");
const { existsSync } = require("fs");
const { resolve: { alias, extensions } } = require(resolve(__dirname, "..", "webpack.config.js"));

const projectRoot = resolve(__dirname, "..");

module.exports = p => {
    for (let name in alias) {
        if (p.startsWith(name))
            return alias[name];
    }
    if (existsSync(p)) {
        return p;
    }
    for (let ext of extensions) {
        if (existsSync(join(projectRoot, "node_modules", `${p}${ext}`))) {
            return join(projectRoot, "node_modules", `${p}${ext}`);
        }
    }
    return join(projectRoot, "node_modules", p);
};
